<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->increments('id');
            $table->json('details');
            $table->unsignedInteger('form_id');
            $table->unsignedInteger('creator_id');
            $table->unsignedInteger('customer_id');
            $table->unsignedInteger('staff_id')->nullable();
            $table->unsignedInteger('department_id')->nullable();
            $table->unsignedInteger('createdbranch_id')->nullable();
            $table->unsignedInteger('collectbranch_id')->nullable();
            $table->integer('sequence_turn');
            $table->string('urgency');
            $table->string('status');
            $table->string('onholdreason')->nullable();;
            $table->dateTime('collection');
            $table->string('remark')->nullable();
            $table->string('storage')->nullable();
            $table->float('price')->nullable();
            $table->float('discount')->nullable();
            $table->float('paid')->nullable();
            $table->boolean('full_paid')->nullable();
            $table->string('collectorname')->nullable();
            $table->string('collectornric')->nullable();
            $table->dateTime('collectortime')->nullable();
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('form_id')->references('id')->on('forms');
            $table->foreign('creator_id')->references('id')->on('users');
            $table->foreign('customer_id')->references('id')->on('users');
            $table->foreign('staff_id')->references('id')->on('users');
            $table->foreign('department_id')->references('id')->on('departments');
            $table->foreign('createdbranch_id')->references('id')->on('branches');
            $table->foreign('collectbranch_id')->references('id')->on('branches');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}
