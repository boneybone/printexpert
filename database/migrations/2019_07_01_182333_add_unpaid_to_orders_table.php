<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddUnpaidToOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('orders', function (Blueprint $table) {
            $table->boolean('unpaid_collection')->nullable();
            $table->unsignedInteger('unpaid_approver_id')->nullable();

            $table->foreign('unpaid_approver_id')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('orders', function (Blueprint $table) {
            $table->dropColumn('unpaid_collection');
            $table->dropForeign(['unpaid_approver_id']);
            $table->dropColumn('unpaid_approver_id');
        });
    }
}
