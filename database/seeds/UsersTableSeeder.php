<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'title' => 'Mr',
            'name' => 'Admin',
            'nric' => '800118045261',
            'contact' => '0189897574',
            'email' => 'admin@printexpert.com',
            'password' => bcrypt('qqqq1111'),
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);
        DB::table('users')->insert([
            'title' => 'Mr',
            'name' => 'Marketing Manager',
            'nric' => '901108045161',
            'department_id' => 1,
            'contact' => '0129090897',
            'email' => 'marketingmanager@printexpert.com',
            'password' => bcrypt('qqqq1111'),
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);
        DB::table('users')->insert([
            'title' => 'Mr',
            'name' => 'Marketing Staff',
            'nric' => '891010107883',
            'department_id' => 1,
            'contact' => '0198787133',
            'email' => 'marketingstaff@printexpert.com',
            'password' => bcrypt('qqqq1111'),
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);
        DB::table('users')->insert([
            'title' => 'Madam',
            'name' => 'Design Manager',
            'nric' => '901010107883',
            'department_id' => 2,
            'contact' => '0198787233',
            'email' => 'designmanager@printexpert.com',
            'password' => bcrypt('qqqq1111'),
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);
        DB::table('users')->insert([
            'title' => 'Sir',
            'name' => 'Design Staff',
            'nric' => '941010107883',
            'department_id' => 2,
            'contact' => '0198707633',
            'email' => 'designstaff@printexpert.com',
            'password' => bcrypt('qqqq1111'),
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);
        DB::table('users')->insert([
            'title' => 'Madam',
            'name' => 'Printing Manager',
            'nric' => '901010107833',
            'department_id' => 3,
            'contact' => '0198757633',
            'email' => 'printingmanager@printexpert.com',
            'password' => bcrypt('qqqq1111'),
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);
        DB::table('users')->insert([
            'title' => 'Sir',
            'name' => 'Printing Staff',
            'nric' => '941011107823',
            'department_id' => 3,
            'contact' => '0128787633',
            'email' => 'printingstaff@printexpert.com',
            'password' => bcrypt('qqqq1111'),
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);
        DB::table('users')->insert([
            'title' => 'Madam',
            'name' => 'Production Manager',
            'nric' => '901210107833',
            'department_id' => 4,
            'contact' => '0178787633',
            'email' => 'productionmanager@printexpert.com',
            'password' => bcrypt('qqqq1111'),
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);
        DB::table('users')->insert([
            'title' => 'Sir',
            'name' => 'Production Staff',
            'nric' => '941110107823',
            'department_id' => 4,
            'contact' => '0198767633',
            'email' => 'productionstaff@printexpert.com',
            'password' => bcrypt('qqqq1111'),
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);
        DB::table('users')->insert([
            'title' => 'Madam',
            'name' => 'Collection Manager',
            'nric' => '900110107833',
            'department_id' => 5,
            'contact' => '0198757633',
            'email' => 'collectionmanager@printexpert.com',
            'password' => bcrypt('qqqq1111'),
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);
        DB::table('users')->insert([
            'title' => 'Sir',
            'name' => 'Collection Staff',
            'nric' => '941010107823',
            'department_id' => 5,
            'contact' => '0198487633',
            'email' => 'collectionstaff@printexpert.com',
            'password' => bcrypt('qqqq1111'),
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);
        DB::table('users')->insert([
            'title' => 'Madam',
            'name' => 'Apparel Manager',
            'nric' => '920110107833',
            'department_id' => 6,
            'contact' => '0198387633',
            'email' => 'apparelmanager@printexpert.com',
            'password' => bcrypt('qqqq1111'),
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);
        DB::table('users')->insert([
            'title' => 'Sir',
            'name' => 'Apparel Staff',
            'nric' => '951010107823',
            'department_id' => 6,
            'contact' => '0198287633',
            'email' => 'apparelstaff@printexpert.com',
            'password' => bcrypt('qqqq1111'),
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);
        DB::table('users')->insert([
            'title' => 'Madam',
            'name' => 'Offset Manager',
            'nric' => '800110107833',
            'department_id' => 7,
            'contact' => '0198187633',
            'email' => 'offsetmanager@printexpert.com',
            'password' => bcrypt('qqqq1111'),
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);
        DB::table('users')->insert([
            'title' => 'Sir',
            'name' => 'Offset Staff',
            'nric' => '841010107823',
            'department_id' => 7,
            'contact' => '0198787632',
            'email' => 'offsetstaff@printexpert.com',
            'password' => bcrypt('qqqq1111'),
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);
        DB::table('users')->insert([
            'title' => 'Madam',
            'name' => 'Branch Manager',
            'nric' => '900110107033',
            'department_id' => 8,
            'contact' => '0198787623',
            'email' => 'branchmanager@printexpert.com',
            'password' => bcrypt('qqqq1111'),
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);
        DB::table('users')->insert([
            'title' => 'Sir',
            'name' => 'Branch Staff',
            'nric' => '941010107123',
            'department_id' => 8,
            'contact' => '0198787233',
            'email' => 'branchstaff@printexpert.com',
            'password' => bcrypt('qqqq1111'),
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);
        DB::table('users')->insert([
            'title' => 'Miss',
            'name' => 'Fazura',
            'nric' => '940102035066',
            'contact' => '0128684232',
            'email' => 'customer@gmail.com',
            'password' => bcrypt('qqqq1111'),
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);

    }
}
