<?php

use Illuminate\Database\Seeder;

class BranchesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('branches')->insert([
            'name' => 'Seksyen 2 Shah Alam HQ',
            'address1' => '20, Jalan Bunga Tanjung 2/16',
            'address2' => 'Seksyen 2',
            'postcode' => '40000',
            'city' => 'Shah Alam',
            'state' => 'Selangor',
        ]);
        DB::table('branches')->insert([
            'name' => 'Pantai Bharu',
            'address1' => 'A-1-8, Pantai Business Center',
            'address2' => 'Jalan Pantai Baharu',
            'postcode' => '59200',
            'city' => 'Kuala Lumpur',
            'state' => 'Wilayah Persekutuan Kuala Lumpur',
        ]);
        DB::table('branches')->insert([
            'name' => 'Puncak Alam',
            'address1' => 'No. 22 & 24 Puncak Bestari',
            'address2' => 'Jalan Niaga Bestari 8',
            'postcode' => '42300',
            'city' => 'Bandar Puncak Alam',
            'state' => 'Selangor',
        ]);
        DB::table('branches')->insert([
            'name' => 'Dengkil',
            'address1' => 'No.10, Aras 2 Anjung Dengkil',
            'address2' => 'Universiti Teknologi MARA, Cawangan Selangor, Kampus Dengkil',
            'postcode' => '43800',
            'city' => 'Dengkil',
            'state' => 'Selangor',
        ]);
        DB::table('branches')->insert([
            'name' => 'Seksyen 7 Shah Alam',
            'address1' => 'No. VG-16, Blok M',
            'address2' => 'Jalan Plumbum V7/V, Seksyen 7',
            'postcode' => '40000',
            'city' => 'Shah Alam',
            'state' => 'Selangor',
        ]);
        DB::table('branches')->insert([
            'name' => 'Taiping',
            'address1' => 'Lot 21, 23, Susur ILBP 1, Jalan Istana Larut',
            'address2' => 'Istana Larut Business Park',
            'postcode' => '34000 ',
            'city' => 'Taiping',
            'state' => 'Perak',
        ]);

    }
}