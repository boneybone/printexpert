<?php

use Illuminate\Database\Seeder;
use App\Permission;
use App\User;
use Spatie\Permission\Models\Role;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // Seed the default permissions
        $permissions = Permission::defaultPermissions();

        foreach ($permissions as $perms) {
            Permission::firstOrCreate(['name' => $perms]);
        }

        $role = Role::create(['name' => 'Super Admin']);
        $role->givePermissionTo($permissions);

        $role = Role::create(['name' => 'Manager']);

        $role = Role::create(['name' => 'Staff']);

        $role = Role::create(['name' => 'Customer']);

        $this->call(DepartmentsTableSeeder::class);
        $this->call(UsersTableSeeder::class);
        $this->call(BranchesTableSeeder::class);

        //Assign User for Testing
        $i=0;
        User::find(++$i)->assignRole('Super Admin');
        User::find(++$i)->assignRole('Manager');
        User::find(++$i)->assignRole('Staff');
        User::find(++$i)->assignRole('Manager');
        User::find(++$i)->assignRole('Staff');
        User::find(++$i)->assignRole('Manager');
        User::find(++$i)->assignRole('Staff');
        User::find(++$i)->assignRole('Manager');
        User::find(++$i)->assignRole('Staff');
        User::find(++$i)->assignRole('Manager');
        User::find(++$i)->assignRole('Staff');
        User::find(++$i)->assignRole('Manager');
        User::find(++$i)->assignRole('Staff');
        User::find(++$i)->assignRole('Manager');
        User::find(++$i)->assignRole('Staff');
        User::find(++$i)->assignRole('Manager');
        User::find(++$i)->assignRole('Staff');
        User::find(++$i)->assignRole('Customer');

    }
}
