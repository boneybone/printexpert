<?php

use Illuminate\Database\Seeder;

class DepartmentsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('departments')->insert(['name' => 'Marketing',]);
        DB::table('departments')->insert(['name' => 'Design',]);
        DB::table('departments')->insert(['name' => 'Printing',]);
        DB::table('departments')->insert(['name' => 'Production',]);
        DB::table('departments')->insert(['name' => 'Collection',]);
        DB::table('departments')->insert(['name' => 'Gift & Apparel',]);
        DB::table('departments')->insert(['name' => 'Offset',]);
        DB::table('departments')->insert(['name' => 'Branch',]);
    }
}
