@extends('layouts.master')

@section('navbar')
<div id="main-wrapper">
    <!-- ============================================================== -->
    <!-- Topbar header - style you can find in pages.scss -->
    <!-- ============================================================== -->
    <header class="topbar">
        <nav class="navbar top-navbar navbar-expand-md navbar-dark">
            <div class="navbar-header">
                <!-- This is for the sidebar toggle which is visible on mobile only -->
                <a class="nav-toggler waves-effect waves-light d-block d-md-none" href="javascript:void(0)"><i
                        class="ti-menu ti-close"></i></a>
                <a class="navbar-brand d-block d-md-none" href="{{route('home')}}">
                    <!-- Logo text -->
                    <span class="logo-text">
                        <!-- dark Logo text -->
                        <img src="{{ asset('assets/images/logos/logo.png')}}" alt="homepage" class="dark-logo" />
                        <!-- Light Logo text -->
                        <img src="{{ asset('assets/images/logos/logo.png')}}" style="width:200px;height:40px;"
                            class="light-logo" alt="homepage" />
                    </span>
                </a>
                <div class="d-none d-md-block text-center">
                    <a class="sidebartoggler waves-effect waves-light d-flex align-items-center side-start"
                        href="javascript:void(0)" data-sidebartype="mini-sidebar">
                        <i class="mdi mdi-menu"></i>
                        <span class="navigation-text ml-3"> Navigation</span>
                    </a>
                </div>
                <!-- ============================================================== -->
                <!-- End Logo -->
                <!-- ============================================================== -->
                <!-- ============================================================== -->
                <!-- Toggle which is visible on mobile only -->
                <!-- ============================================================== -->
                <a class="topbartoggler d-block d-md-none waves-effect waves-light" href="javascript:void(0)"
                    data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent"
                    aria-expanded="false" aria-label="Toggle navigation"><i class="ti-more"></i></a>
            </div>
            <!-- ============================================================== -->
            <!-- End Logo -->
            <!-- ============================================================== -->
            <div class="navbar-collapse collapse" id="navbarSupportedContent">
                <!-- ============================================================== -->
                <!-- toggle and nav items -->
                <!-- ============================================================== -->
                <ul class="navbar-nav float-left mr-auto">
                    <!-- ============================================================== -->
                    <!-- Logo -->
                    <!-- ============================================================== -->
                    <li class="nav-item border-right">
                        <a class="nav-link navbar-brand d-none d-md-block" href="{{route('home')}}">
                            <!-- Logo text -->
                            <span class="logo-text">
                                <!-- dark Logo text -->
                                <img src="{{ asset('assets/images/logos/logo.png')}}" alt="homepage" class="dark-logo" />
                                <!-- Light Logo text -->
                                <img src="{{ asset('assets/images/logos/logo.png')}}" style="width:auto;height:30px;" class="light-logo" alt="homepage" />
                            </span>
                        </a>
                    </li>
                    {{-- <!-- ============================================================== -->
                    <!-- Notification -->
                    <!-- ============================================================== -->
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle waves-effect waves-dark" href="" data-toggle="dropdown"
                            aria-haspopup="true" aria-expanded="false"> <i class="mdi mdi-check-circle font-18"></i>
                            <div class="notify">
                                <span class="heartbit"></span>
                                <span class="point"></span>
                            </div>
                        </a>
                        <div class="dropdown-menu dropdown-menu-left mailbox animated bounceInDown">
                            <span class="with-arrow"><span class="bg-primary"></span></span>
                            <ul class="list-style-none">
                                <li>
                                    <div class="drop-title border-bottom">You have 3 new Tasks</div>
                                </li>
                                <li>
                                    <div class="message-center notifications">
                                        <!-- Message -->
                                        <a href="javascript:void(0)" class="message-item">
                                            <span class="btn btn-danger btn-circle"><i class="fa fa-link"></i></span>
                                            <span class="mail-contnet">
                                                <h5 class="message-title">Luanch Admin</h5> <span class="mail-desc">Just
                                                    see the my new admin!</span> <span class="time">9:30 AM</span>
                                            </span>
                                        </a>
                                        <!-- Message -->
                                        <a href="javascript:void(0)" class="message-item">
                                            <span class="btn btn-success btn-circle"><i class="ti-calendar"></i></span>
                                            <span class="mail-contnet">
                                                <h5 class="message-title">Event today</h5> <span class="mail-desc">Just
                                                    a reminder that you have event</span> <span class="time">9:10
                                                    AM</span>
                                            </span>
                                        </a>
                                        <!-- Message -->
                                        <a href="javascript:void(0)" class="message-item">
                                            <span class="btn btn-info btn-circle"><i class="ti-settings"></i></span>
                                            <span class="mail-contnet">
                                                <h5 class="message-title">Settings</h5> <span class="mail-desc">You can
                                                    customize this template as you want</span> <span class="time">9:08
                                                    AM</span>
                                            </span>
                                        </a>
                                        <!-- Message -->
                                        <a href="javascript:void(0)" class="message-item">
                                            <span class="btn btn-primary btn-circle"><i class="ti-user"></i></span>
                                            <span class="mail-contnet">
                                                <h5 class="message-title">Pavan kumar</h5> <span class="mail-desc">Just
                                                    see the my admin!</span> <span class="time">9:02 AM</span>
                                            </span>
                                        </a>
                                    </div>
                                </li>
                                <li>
                                    <a class="nav-link text-center mb-1 text-dark" href="javascript:void(0);">
                                        <strong>See
                                            all Tasks</strong> <i class="fa fa-angle-right"></i> </a>
                                </li>
                            </ul>
                        </div>
                    </li>
                    <!-- ============================================================== -->
                    <!-- End Notification -->
                    <!-- ============================================================== --> --}}
                </ul>
                <!-- ============================================================== -->
                <!-- Right side toggle and nav items -->
                <!-- ============================================================== -->
                <ul class="navbar-nav float-right">
                    {{-- <!-- ============================================================== -->
                    <!-- Search -->
                    <!-- ============================================================== -->
                    <li class="nav-item search-box">
                        <form class="app-search d-none d-lg-block">
                            <input type="text" class="form-control" placeholder="Search...">
                            <a href="" class="active"><i class="fa fa-search"></i></a>
                        </form>
                    </li> --}}
                    <!-- ============================================================== -->
                    <!-- User profile and search -->
                    <!-- ============================================================== -->
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle waves-effect waves-dark pro-pic" href=""
                            data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    <img src="{!! Auth::user()->photo() !!}" width="31" class="rounded-circle" alt="user">
                            <span class="ml-2 user-text font-medium">{{ Auth::user()->name }}</span><span
                                class="fas fa-angle-down ml-2 user-text"></span>
                        </a>
                        <div class="dropdown-menu dropdown-menu-right user-dd animated flipInY">
                            <div class="d-flex no-block align-items-center p-3 mb-2 border-bottom">
                                <div class="">
                                        <img src="{!! Auth::user()->photo() !!}" width="80" class="rounded" alt="user">
                                </div>
                                <div class="ml-2">
                                    <h4 class="mb-0">{{ Auth::user()->name }}</h4>
                                    <p class=" mb-0 text-muted">{{ Auth::user()->email }}</p>
                                </div>
                            </div>
                            <a class="dropdown-item" href="{{route('user.edit', Auth::user()->id)}}"><i
                                    class="ti-user mr-1 ml-1"></i> My
                                Profile</a>
                            <div class="dropdown-divider"></div>
                            <a class="dropdown-item" href="{{ route('logout') }}" onclick="event.preventDefault();
                                              document.getElementById('logout-form').submit();"><i
                                    class="fa fa-power-off mr-1 ml-1"></i> Logout</a>
                        </div>
                    </li>
                    <!-- ============================================================== -->
                    <!-- User profile and search -->
                    <!-- ============================================================== -->
                </ul>
            </div>
        </nav>
    </header>
    <!-- ============================================================== -->
    <!-- End Topbar header -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- Left Sidebar - style you can find in sidebar.scss  -->
    <!-- ============================================================== -->
    <aside class="left-sidebar">
        <!-- Sidebar scroll-->
        <div class="scroll-sidebar">
            <!-- Sidebar navigation-->
            <nav class="sidebar-nav">
                <ul id="sidebarnav">
                    <li class="sidebar-item">
                        <a class="sidebar-link has-arrow waves-effect waves-dark profile-dd" href="javascript:void(0)"
                            aria-expanded="false">
                                <img src="{!! Auth::user()->photo() !!}" width="30" class="rounded-circle ml-2" alt="user">
                            <span class="hide-menu">{{ Auth::user()->name }} </span>
                        </a>
                        <ul aria-expanded="false" class="collapse  first-level">
                            <li class="sidebar-item">
                                <a href="{{route('user.edit', Auth::user()->id)}}" class="sidebar-link">
                                    <i class="ti-user"></i>
                                    <span class="hide-menu"> My Profile</span>
                                </a>
                            </li>
                            <li class="sidebar-item">
                                <a href="{{ route('logout') }}" onclick="event.preventDefault();
                                                  document.getElementById('logout-form').submit();"
                                    class="sidebar-link">
                                    <i class="fas fa-power-off"></i>
                                    <span class="hide-menu"> Logout </span>
                                </a>
                            </li>
                        </ul>
                    </li>
                    <li class="sidebar-item">
                        <a class="sidebar-link waves-effect waves-dark sidebar-link" href="{{route('dashboard.index')}}"
                            aria-expanded="false">
                            <i class="mdi mdi-av-timer"></i>
                            <span class="hide-menu">Dashboard</span>
                        </a>
                    </li>
                    @if(Auth::user()->getRoleNames()->first() != 'Super Admin' && (Auth::user()->department->name != 'Marketing' && Auth::user()->department->name != 'Collection'))
                    <li class="sidebar-item">
                        <a class="sidebar-link has-arrow waves-effect waves-dark sidebar-link" href="javascript:void(0)"
                            aria-expanded="false">
                            <i class="far fa-clipboard"></i>
                            <span class="hide-menu"> {{ Auth::user()->department->name }} Task</span>
                        </a>

                        <ul aria-expanded="false" class="collapse  first-level">
                                @can('Assign Order')
                                <li class="sidebar-item">
                                    <a class="sidebar-link" href="{{route('dashboard.manager')}}">
                                        <i class="mdi mdi-exclamation"></i>
                                        <span class="hide-menu"> New Order</span>
                                    </a>
                                </li>
                                @endcan
                                <li class="sidebar-item">
                                    <a class="sidebar-link" href="{{route('dashboard.staff')}}">
                                        <i class="mdi mdi-view-list"></i>
                                        <span class="hide-menu"> Assigned Order</span>
                                    </a>
                                </li>
                        </ul>
                    </li>
                    @endif
                    @can('View User')
                    <li class="sidebar-item">
                        <a class="sidebar-link waves-effect waves-dark sidebar-link" href="{{route('user.index')}}"
                            aria-expanded="false">
                            <i class="mdi mdi-account-multiple"></i>
                            <span class="hide-menu">Users</span>
                        </a>
                    </li>
                    @endcan
                    @role('Super Admin')
                    <li class="sidebar-item">
                        <a class="sidebar-link waves-effect waves-dark sidebar-link" href="{{route('role.index')}}"
                            aria-expanded="false">
                            <i class="mdi mdi-account-settings-variant"></i>
                            <span class="hide-menu">Roles</span>
                        </a>
                    </li>
                    @endrole
                    @can('View Department')
                    <li class="sidebar-item">
                        <a class="sidebar-link waves-effect waves-dark sidebar-link"
                            href="{{route('department.index')}}" aria-expanded="false">
                            <i class="mdi mdi-briefcase"></i>
                            <span class="hide-menu">Departments</span>
                        </a>
                    </li>
                    @endcan
                    @can('View Branch')
                    <li class="sidebar-item">
                        <a class="sidebar-link waves-effect waves-dark sidebar-link" href="{{route('branch.index')}}"
                            aria-expanded="false">
                            <i class="mdi mdi-map-marker"></i>
                            <span class="hide-menu">Branches</span>
                        </a>
                    </li>
                    @endcan
                    @can('View Form')
                    <li class="sidebar-item">
                        <a class="sidebar-link waves-effect waves-dark sidebar-link" href="{{route('form.index')}}"
                            aria-expanded="false">
                            <i class="mdi mdi-clipboard-text"></i>
                            <span class="hide-menu">Forms</span>
                        </a>
                    </li>
                    @endcan
                    {{-- @can('View Status')
                    <li class="sidebar-item">
                        <a class="sidebar-link waves-effect waves-dark sidebar-link" href="{{route('status.index')}}"
                            aria-expanded="false">
                            <i class="mdi mdi-playlist-check"></i>
                            <span class="hide-menu">Status</span>
                        </a>
                    </li>
                    @endcan --}}
                    <li class="sidebar-item">
                            <a class="sidebar-link waves-effect waves-dark sidebar-link" href="{{route('order.index')}}"
                                aria-expanded="false">
                                <i class="mdi mdi-view-list"></i>
                                <span class="hide-menu">Order List</span>
                            </a>
                    </li>
                    @if(Auth::user()->getRoleNames()->first() == 'Super Admin' || (Auth::user()->getRoleNames()->first() == 'Manager' && (Auth::user()->department->name == 'Marketing')))
                    <li class="sidebar-item">
                        <a class="sidebar-link waves-effect waves-dark sidebar-link" href="{{route('dashboard.payment')}}">
                            <i class="far fa-money-bill-alt"></i>
                            <span class="hide-menu">Payment List</span>
                        </a>
                    </li>
                    <li class="sidebar-item">
                            <a class="sidebar-link waves-effect waves-dark sidebar-link" href="{{route('order.selectForm')}}">
                                <i class="mdi mdi-plus"></i>
                                <span class="hide-menu">Create Order</span>
                            </a>
                    </li>
                    @endif
                </ul>
            </nav>
            <!-- End Sidebar navigation -->
        </div>
        <!-- End Sidebar scroll-->
    </aside>
    <!-- ============================================================== -->
    <!-- End Left Sidebar - style you can find in sidebar.scss  -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- Page wrapper  -->
    <!-- ============================================================== -->
    <div class="page-wrapper">
        <!-- ============================================================== -->
        <!-- Bread crumb and right sidebar toggle -->
        <!-- ============================================================== -->
        <div class="page-breadcrumb bg-light">
            <div class="row my-2">
                <div class="col-lg-3 col-md-4 col-xs-12 align-self-center">
                    <h5 class="font-medium text-uppercase mb-0">@yield('breadcrumb')</h5>
                </div>
                <div class="col-lg-9 col-md-8 col-xs-12 align-self-center">
                    <nav aria-label="breadcrumb" class=" float-md-right float-left">
                        <ol class="breadcrumb mb-0 justify-content-end p-0 bg-light">
                            <li class="breadcrumb-item"><a href="{{route('home')}}">Home</a></li>
                            <li class="breadcrumb-item active" aria-current="page">@yield('breadcrumb')</li>
                        </ol>
                    </nav>
                </div>
            </div>
        </div>
        <!-- ============================================================== -->
        <!-- End Bread crumb and right sidebar toggle -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- Container fluid  -->
        <!-- ============================================================== -->
        <div class="page-content container-fluid">
            @if(session()->has('message'))
            <div class="col-sm-12">
            <div class="alert alert-success"> <i class="ti-check"></i>
                {{ session()->get('message') }}
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span
                        aria-hidden="true">×</span>
                </button>
            </div>
            </div>
            @elseif(session()->has('error'))
            <div class="col-sm-12">
            <div class="alert alert-danger"> <i class="ti-alert"></i>
                {{ session()->get('error') }}
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span
                        aria-hidden="true">×</span>
                </button>
            </div>
            </div>
            @endif
                @yield('content')
        </div>
        <!-- ============================================================== -->
        <!-- End Container fluid  -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- footer -->
        <!-- ============================================================== -->
        <footer class="footer text-center">
            2019 © Print Expert
        </footer>
        <!-- ============================================================== -->
        <!-- End footer -->
        <!-- ============================================================== -->
    </div>
    <!-- ============================================================== -->
    <!-- End Page wrapper  -->
    <!-- ============================================================== -->
</div>

<form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
    @csrf
</form>
@endsection
