@extends('layouts.navbar')
@section('breadcrumb', 'Order List')
@section('content')

<div class="card">
    <div class="d-flex no-block align-items-center col-lg-12 py-3" style="border-bottom: 1px solid #eaeaea;">
        <h4 class="card-title mb-0 ml-2">TRACK ORDER</h4>
    </div>
    <div class="card-body">
        <div class="form-group">
            <input type="search" class="form-control" placeholder="Search order by Order ID, Created By, Phone Number"
                id="searchOrder">
        </div>
        <div class="table-responsive" id="searchOrderTable">
        </div>
    </div>
</div>

<div class="card">
    <div class="d-flex no-block align-items-center col-lg-12 py-3" style="border-bottom: 1px solid #eaeaea;">
        <h4 class="card-title mb-0 ml-2">ORDER LIST</h4>
    </div>
    <div class="card-body">
        <div class="table-responsive">
            @if(Auth::user()->getRoleNames()->first() == 'Manager' && (Auth::user()->department->name == 'Marketing'))
                @include('order.indexdatamarketingmanager')
            @else
                @include('order.indexdata')
            @endif
        </div>
    </div>
</div>

@endsection

@section('script')
<script>
    $(document).ready(function () {

        $('#orderTable thead tr').clone(true).appendTo('#orderTable thead');
        $('#orderTable thead tr:eq(1) th').each(function (i) {

            var title = $(this).text().toLowerCase().replace(/\b[a-z]/g, function (txtVal) {
                return txtVal.toUpperCase();
            });
            $(this).html('<input type="text" class="form-control form-control-sm" placeholder="Search ' + title + '" />');

            $('input', this).on('keyup change', function () {
                if($('#orderTable').DataTable().column(i).search() !== this.value)
                    $('#orderTable').DataTable().column(i).search(this.value).draw();
            });
        });

        $('#orderTable').DataTable({
            orderCellsTop: true,
            fixedHeader: true
        });

    });

    $('#searchOrder').on('keyup', function () {
        $value = $(this).val();

        if($value != '')
        {
            $.ajax({
                type: 'get',
                url: '/order/search',
                data: {
                    'search': $value
                },
                success: function (searchResult) {
                    $('#searchOrderTable').html(searchResult);
                    $('#idSearchTable').DataTable();
                    $('#contactSearchTable').DataTable();
                    $('#creatorSearchTable').DataTable();
                }
            });
        }
        else
            $('#searchOrderTable').html(null);

    });

    $(document).on("click", "button.holdOrder", function () {
        orderID = $(this).attr("data-id");
        swal({
            title: 'Enter your reason',
            input: 'text',
            showCancelButton: true,
            inputValidator: (value) => {
                if (value == '') {
                    return 'You need to write something!'
                }
            }
        }).then((result) => {
            if (result.value) {
                $.ajax({
                    type: 'PUT',
                    url: '/order/updateStatus',
                    data: {
                        onholdreason: result.value,
                        id: orderID
                    },
                    success: function (data) {
                        swal('Task On Hold!', data.message, 'success').then((result) => {location.reload();});
                    }
                });
            }
        })
    });

    $(document).on("click", "input.unpaidCollection", function () {
        orderID = $(this).attr("data-id");

        $.ajax({
            type: 'PUT',
            url: '/order/updateUnpaid',
            data: {
                id: orderID
            },
            success: function (data) {
                swal(data.type, data.message, 'success').then((result) => {location.reload();});
            }
        });

    });

    $(document).on("click", "button.resumeOrder", function () {
        orderID = $(this).attr("data-id");
        swal({
            title: 'Are you sure?',
            text: 'Resume this task?',
            type: 'question',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes',
            cancelButtonText: 'No'
        }).then((result) => {
            if (result.value) {
                $.ajax({
                    type: 'PUT',
                    url: '/order/updateStatus',
                    data: {
                        onholdreason: null,
                        id: orderID
                    },
                    success: function (data) {
                        swal('Task Resumed!', data.message, 'success').then((result) => {location.reload();});
                        $('#orderTable').load('/order/refreshList');
                    }
                });
            }
        })
    });

    $(document).on("click", ".takeorder", function () {
        orderID = $(this).attr("data-id");
        swal({
          title: 'Are you sure?',
          text: 'Assign task to yourself?',
          type: 'question',
          showCancelButton: true,
          confirmButtonColor: '#3085d6',
          cancelButtonColor: '#d33',
          confirmButtonText: 'Yes',
          cancelButtonText: 'No'
        }).then((result) => {
          if (result.value) {
            $.ajax({
            type: 'PUT',
            url: '/order/assign',
            data: {
                id: orderID
            },
            success: function (data) {
                swal("Task Assigned!", data.message, "success").then((result) => {location.reload();});
            }
            });
          }
        })
    });
</script>
<!--Custom JavaScript -->
<script src="{{ asset('assets/libs/sweetalert2/dist/sweetalert2.all.min.js') }}"></script>
@endsection
