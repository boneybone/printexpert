<table id="orderTable" class="table no-wrap user-table mb-3">
    <thead>
        <tr>
            <th>ID</th>
            <th>ORDER TYPE</th>
            <th>DEPARTMENT</th>
            <th>URGENCY</th>
            <th>STATUS</th>
            <th>CUSTOMER</th>
            <th>COLLECTION</th>
            <th>CREATED AT</th>
            <th>CREATED BY</th>
            <th>UNPAID COLLECTION</th>
            <th>ACTION</th>
        </tr>
    </thead>
    <tbody>
        @foreach($order as $o)
        <tr>
            <td>{{$o->id}}</td>
            <td>{{$o->form['name']}}</td>
            <td>
                {{$o->department['name']}}<br>
                @if($o->staff_id == null)
                <span class="text-danger">Unassigned</span>
                @else
                <span class="text-muted">{{$o->staff['name']}}</span>
                @endif
            </td>
            <td>
                @if($o->urgency == 'Super Urgent')
                <span class="badge badge-danger">{{$o->urgency}}</span>
                @elseif($o->urgency == 'Urgent')
                <span class="badge badge-warning">{{$o->urgency}}</span>
                @elseif($o->urgency == 'Normal')
                <span class="badge badge-info">{{$o->urgency}}</span>
                @endif
            </td>
            <td>
                @if($o->onholdreason != null)
                <span class="badge badge-secondary">On Hold <button type="button"  title="Resume Task" class="resumeOrder" data-id="{{$o->id}}"><i class="mdi mdi-play"></i></button></span>
                <p><span class="text-muted">{{$o->onholdreason}}</span></p>
                @elseif($o->status == 'Received')
                <span class="badge badge-primary">{{$o->status}}</span>
                @elseif($o->status == 'Delivered')
                <span class="badge badge-success">{{$o->status}}</span>
                @elseif($o->status == 'Pending')
                <span class="badge badge-warning">{{$o->status}} <button type="button" title="Hold Task" class="holdOrder" data-id="{{$o->id}}"><i class="mdi mdi-pause"></i></button></span>
                @else
                <span class="badge badge-info">{{$o->status}} <button type="button" title="Hold Task" class="holdOrder" data-id="{{$o->id}}"><i class="mdi mdi-pause"></i></button></span>
                @endif
            </td>
            <td>{{$o->customer['name']}}<br>
                <span class="text-muted">{{$o->customer['email']}}</span>
            </td>
            <td>{{date('d/m/Y', strtotime($o->collection))}}<br>{{date('g:i A', strtotime($o->collection))}}
            </td>
            <td>{{date('d/m/Y', strtotime($o->created_at))}}<br>{{date('g:i A', strtotime($o->created_at))}}
            </td>
            <td>{{$o->creator['name']}}</td>
            <td align="center">
                @if($o->unpaid_collection == true)
                    <input type="checkbox" class="unpaidCollection" data-id="{{$o->id}}" checked><br><span class="text-muted">Approved by {{$o->unpaid_approver['name']}}</span>
                @else
                    <input type="checkbox" class="unpaidCollection" data-id="{{$o->id}}">
                @endif
            </td>
            <td>
                <a href="{{route('order.detail', $o->id)}}" class="btn btn-circle btn-outline-success btn-sm ml-2" title="View Order"><i class="fas fa-eye"></i></a>
                <a href="{{route('order.edit', $o->id)}}" class="btn btn-outline-info btn-circle btn-sm btn-circle ml-2" title="Edit Order"><i class="ti-pencil-alt"></i></a>
            </td>
        </tr>
        @endforeach
    </tbody>
</table>
