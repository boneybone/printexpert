@extends('layouts.navbar')
@section('head')
<link rel="stylesheet" type="text/css" href="{{ asset('css/jSignature.css')}}">
<link rel="stylesheet" type="text/css" href="{{ asset('assets/libs/select2/dist/css/select2.min.css')}}">
<link rel="stylesheet" type="text/css" href="{{ asset('assets/libs/bootstrap-material-datetimepicker/css/bootstrap-material-datetimepicker.css')}}">
<link rel="stylesheet" type="text/css" href="{{ asset('css/fileUploader.css')}}">
<link rel="stylesheet" type="text/css" href="{{ asset('css/bcPaint.css')}}">
<link rel="stylesheet" type="text/css" href="{{ asset('css/bcPaint.mobile.css')}}">
@endsection
@section('breadcrumb', 'Create Order / '.$form->name)
@section('content')
<div class="col-sm-12">
    <div class="card">
        <form id="form" class="form-horizontal" enctype="multipart/form-data" method="POST"
            action="{{route('order.store', $form->id)}}">
            @csrf
            <div class="d-flex no-block align-items-center col-lg-12 py-3 mb-4" style="background: #2cabe3;">
                <h4 class="card-title mb-0 ml-3 text-white">Create {{$form->name}} Order</h4>
            </div>
            <div class="card-body">
                <h4 class="card-title mb-0">CUSTOMER INFORMATION</h4>
                <hr class="mb-4">
                <div class="row mb-3">
                    <div class="form-group col-sm-6">
                        <div class="d-flex no-block align-items-center">
                            <label class="font-medium">Existing Customer</label>
                            <label class="ml-auto font-medium">New Customer? <a href="#" data-toggle="modal"
                                    data-target="#createuser" style="color: #2cabe3;">Quick Add</a></label>
                        </div>
                        <select class="select2-with-border form-control" style="width: 100%; height:36px;"
                            name="customer_id" id="customer_id" required>
                            <option value="" selected disabled>Search Customer...</option>
                            @foreach($customer as $c)
                            <option value="{{$c->id}}">{{$c->nric}} - {{$c->name}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group col-sm-6">
                        <label class="font-medium">Title</label><br>
                        <div class="form-check form-check-inline">
                            <div class="custom-control custom-checkbox">
                                <input type="checkbox" class="custom-control-input" id="Mr" disabled>
                                <label class="custom-control-label">Mr.</label>
                            </div>
                        </div>
                        <div class="form-check form-check-inline">
                            <div class="custom-control custom-checkbox">
                                <input type="checkbox" class="custom-control-input" id="Mrs" disabled>
                                <label class="custom-control-label">Mrs.</label>
                            </div>
                        </div>
                        <div class="form-check form-check-inline">
                            <div class="custom-control custom-checkbox">
                                <input type="checkbox" class="custom-control-input" id="Miss" disabled>
                                <label class="custom-control-label">Miss</label>
                            </div>
                        </div>
                        <div class="form-check form-check-inline">
                            <div class="custom-control custom-checkbox">
                                <input type="checkbox" class="custom-control-input" id="Sir" disabled>
                                <label class="custom-control-label">Sir</label>
                            </div>
                        </div>
                        <div class="form-check form-check-inline">
                            <div class="custom-control custom-checkbox">
                                <input type="checkbox" class="custom-control-input" id="Madam" disabled>
                                <label class="custom-control-label">Madam</label>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row mb-3">
                    <div class="form-group col-sm-6">
                        <label class="font-medium">Name</label>
                        <input type="text" id="name" placeholder="Name" class="form-control" disabled>
                    </div>
                    <div class="form-group col-sm-6">
                        <label class="font-medium">IC Number</label>
                        <input type="text" id="nric" placeholder="IC Number" class="form-control" disabled>
                    </div>
                </div>
                <div class="row mb-5">
                    <div class="form-group col-sm-6">
                        <label class="font-medium">Mobile Number</label>
                        <input type="text" id="contact" placeholder="Mobile Number" class="form-control" disabled>
                    </div>
                    <div class="form-group col-sm-6">
                        <label class="font-medium">Email</label>
                        <input type="text" id="email" placeholder="Email" class="form-control" disabled>
                    </div>
                </div>

                @if($form->name != 'Graphic Design')
                <h4 class="card-title mb-0">DESIGN DETAILS</h4>
                <hr class="mb-4">

                <div class="form-group">
                    <div class="form-check form-check-inline">
                        <div class="custom-control custom-radio">
                            <input type="radio" id="uploadDesign" name="design" class="custom-control-input"
                                value="uploadDesign" onclick="showUpload()" required>
                            <label class="custom-control-label" for="uploadDesign">Upload Design</label>
                        </div>
                    </div>
                    <div class="form-check form-check-inline">
                        <div class="custom-control custom-radio">
                            <input type="radio" id="requestDesign" name="design" class="custom-control-input"
                                value="requestDesign" onclick="hideUpload()" required>
                            <label class="custom-control-label" for="requestDesign">Request Design</label>
                        </div>
                    </div>
                </div>
                @endif

                <span class="btn btn-light btn-block fileinput-button" id="uploadFile">
                    <h1><span class="mdi mdi-cloud-upload"></span></h1>
                    <p><span>Drag and drop a file here or click</span></p>
                    <div id="output" class="font-light"></div>
                    <input type="file" name="file" id="ufile">
                </span>

                <h4 class="card-title mt-5">DESIGN SKETCH</h4>
                <hr class="mb-4">

                <div class="form-group">
                    <div class="form-check form-check-inline">
                            <div class="custom-control custom-radio">
                                <input type="radio" id="sketchFalse" name="sketchdesign" class="custom-control-input"
                                    value="sketchFalse" onclick="hideSketch()" required>
                                <label class="custom-control-label" for="sketchFalse">No Sketching</label>
                            </div>
                        </div>
                    <div class="form-check form-check-inline">
                        <div class="custom-control custom-radio">
                            <input type="radio" id="sketchTrue" name="sketchdesign" class="custom-control-input"
                                value="sketchTrue" onclick="showSketch()" required>
                            <label class="custom-control-label" for="sketchTrue">Sketch Design</label>
                        </div>
                    </div>
                </div>

                <div id="bcPaint"></div>
                <input type="hidden" name="sketch">

            <h4 class="card-title mt-5">COLLECT INFORMATION</h4>
                <hr class="mb-4">
                <div class="form-group mb-4">
                    <div class="form-check form-check-inline">
                        <div class="custom-control custom-radio">
                            <input type="radio" id="normal" name="urgency" class="custom-control-input" value="Normal"
                                required>
                            <label class="custom-control-label" for="normal">Normal Order</label>
                        </div>
                    </div>
                    <div class="form-check form-check-inline">
                        <div class="custom-control custom-radio">
                            <input type="radio" id="urgent" name="urgency" class="custom-control-input" value="Urgent"
                                required>
                            <label class="custom-control-label" for="urgent">Urgent Order</label>
                        </div>
                    </div>
                    <div class="form-check form-check-inline">
                        <div class="custom-control custom-radio">
                            <input type="radio" id="superurgent" name="urgency" class="custom-control-input"
                                value="Super Urgent" required>
                            <label class="custom-control-label" for="superurgent">Super Urgent Order</label>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="form-group col-sm-4">
                        <label class="font-medium">Collection Time</label>
                        <input type="text" id="date-format" name="collection" class="form-control" required>
                    </div>
                    <div class="form-group col-sm-4">
                        <label class="font-medium">Created Branch</label>
                        <select class="form-control" name="createdbranch_id" id="createdbranch_id">
                            @foreach($branch as $b)
                            <option value="{{$b->id}}">{{$b->name}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group col-sm-4">
                        <label class="font-medium">Collection Branch</label>
                        <select class="form-control" name="collectbranch_id" id="collectbranch_id">
                            @foreach($branch as $b)
                            <option value="{{$b->id}}">{{$b->name}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>

                <h4 class="card-title mt-4">SPECIFICATION DETAILS</h4>
                <hr>
                <?php $i = 0; ?>
                @foreach($form->field as $f)
                <?php ++$i; ?>
                <div class="form-group">
                    @if($f['type'] == 'dropdown')
                    <label class="font-medium">{{$f['name']}}</label>
                    <select class="form-control" name="{{$f['name']}}">
                        @if(array_key_exists('option', $f))
                        @foreach($f['option'] as $o)
                        <option>{{$o}}</option>
                        @endforeach
                        @endif
                    </select>
                    @elseif($f['type'] == 'checkbox')
                    <div class="custom-control custom-checkbox mr-sm-2">
                        <input type="checkbox" class="custom-control-input" id="checkbox{{$i}}{{$f['name']}}" name="{{$f['name']}}" value="Yes">
                        <label class="custom-control-label" for="checkbox{{$i}}{{$f['name']}}">{{$f['name']}}</label>
                    </div>
                    @elseif($f['type'] == 'radio')
                    @if(array_key_exists('option', $f))
                    <label class="font-medium">{{$f['name']}}</label>
                    @foreach($f['option'] as $o)
                    <div class="custom-control custom-radio">
                        <input type="radio" id="customRadio{{$i}}{{$o}}" name="{{$f['name']}}"
                            class="custom-control-input" value="{{$o}}" {{$f['validation']}}>
                        <label class="custom-control-label" for="customRadio{{$i}}{{$o}}">{{$o}}</label>
                    </div>
                    @endforeach
                    @if($f['custom'] == 'true')
                    <div class="custom-control custom-radio">
                        <input type="radio" id="customRadio{{$i}}custom" name="{{$f['name']}}"
                            class="custom-control-input" value="" {{$f['validation']}}>
                        <label class="custom-control-label" for="customRadio{{$i}}custom"><input type="text"
                                placeholder="Others" class="form-control form-control-sm"
                                onChange="$('#customRadio{{$i}}custom').val(this.value  );" onclick="$('#customRadio{{$i}}custom').prop('checked', true);"/></label>
                    </div>
                    @endif
                    @endif

                    @elseif($f['type'] == 'radioImage')
                    @if(array_key_exists('option', $f))
                    <label class="font-medium">{{$f['name']}}</label>
                    <div class="row">
                        @foreach($f['option'] as $index=>$o)
                        <div class="col-lg-3 col-sm-6 mb-2" style="width: 100%;height: 15vw;object-fit: cover;">
                            <div class="custom-control custom-radio py-3">
                                <input type="radio" id="customRadioImage{{$i}}{{$o}}" name="{{$f['name']}}"
                                    class="custom-control-input" value="{{$o}}" {{$f['validation']}}>
                                <label class="custom-control-label text-center"
                                    for="customRadioImage{{$i}}{{$o}}"><img class="img-fluid" style="max-height: 13vw;" src="{{ asset('formimages/'.$f['directory'][$index]) }}"><br>{{$o}}</label>
                            </div>
                        </div>
                        @endforeach
                    </div>

                    @if($f['custom'] == 'true')
                        <div class="custom-control custom-radio">
                            <input type="radio" id="customRadioImage{{$i}}custom" name="{{$f['name']}}"
                                class="custom-control-input" value="" {{$f['validation']}}>
                            <label class="custom-control-label" for="customRadioImage{{$i}}custom"><input
                                    type="text" class="form-control form-control-sm" placeholder="Others"
                                    onChange="$('#customRadioImage{{$i}}custom').val(this.value);" onclick="$('#customRadioImage{{$i}}custom').prop('checked', true);"/></label>
                        </div>
                    @endif
                    @endif

                    @else
                    <label class="font-medium">{{$f['name']}}</label>
                    <input type="{{$f['type']}}" name="{{$f['name']}}" @if($f['type'] == 'number') min="0" @endif class="form-control" {{$f['validation']}}>
                    @endif
                </div>
                @endforeach

                <div class="form-group">
                    <label class="font-medium">Remark (Optional)</label>
                    <textarea rows="3" cols="50" class="form-control" name="remark"></textarea>
                </div>

                <h4 class="card-title mt-5">PAYMENT INFORMATION</h4>
                <hr class="mb-4">
                    <div class="form-group mb-4">
                        <div class="form-check form-check-inline">
                            <div class="custom-control custom-radio">
                                <input type="radio" id="paymentcod" name="paymentmethod" class="custom-control-input"
                                    value="Cash on Delivery" onclick="$('#referencediv').hide();$('#reference').prop('required', false);$('#reference').prop('disabled', true);" required>
                                <label class="custom-control-label" for="paymentcod">Cash on Delivery</label>
                            </div>
                        </div>
                        <div class="form-check form-check-inline">
                            <div class="custom-control custom-radio">
                                <input type="radio" id="paymentlo" name="paymentmethod" class="custom-control-input"
                                    value="Purchase Order" onclick="$('#referencediv').show();$('#reference').prop('required', true);$('#reference').prop('disabled', false);" required>
                                <label class="custom-control-label" for="paymentlo">Purchase Order</label>
                            </div>
                        </div>
                        <div class="form-check form-check-inline">
                            <div class="custom-control custom-radio">
                                <input type="radio" id="paymentterm" name="paymentmethod" class="custom-control-input"
                                    value="Payment Term" onclick="$('#referencediv').show();$('#reference').prop('required', true);$('#reference').prop('disabled', false);" required>
                                <label class="custom-control-label" for="paymentterm">Payment Term</label>
                            </div>
                        </div>
                    </div>
                <div class="row mb-0">
                    <div class="form-group col-sm-3">
                        <label class="font-medium">Total Price (MYR)</label>
                        <input type="number" step="0.01" min="0.01" class="form-control" name="price" id="price" required>
                    </div>
                    <div class="form-group col-sm-3">
                        <label class="font-medium">Discount (%)</label>
                        <input type="number" step="0.1" min="0" max="100" class="form-control" name="discount" id="discount">
                    </div>
                    <div class="form-group col-sm-3">
                        <label class="font-medium">Payment (MYR)</label>
                        <input type="number" step="0.01" class="form-control" name="paid" id="paid" min="0.01" required>
                    </div>
                    <div class="form-group col-sm-3" id="referencediv">
                        <label class="font-medium">Reference</label>
                        <input class="form-control" name="paymentreference" id="reference" required>
                    </div>
                </div>

                <h4 class="card-title mt-5">CUSTOMER SIGNATURE</h4>
                <hr>
                <div id="content">
                    <div id="signatureparent" class="p-0">
                        <div id="signature"></div></div>
                    <div id="tools"></div>
                </div>
                <div id="scrollgrabber"></div>
                </div>

                <input type="hidden" name="status" value="Pending">
                <div class="d-flex no-block align-items-center col-lg-12 mt-4 pt-4 pb-4">
                    <button type="button" id="submitOrder" class="btn btn-success waves-effect waves-light ml-2">Submit</button>
                    <input type="submit" id="confirmOrder" style="display:none;">
                </div>
            </div>
        </form>
    </div>
</div>

<!-- Create UserModal -->
<div class="modal fade" id="createuser" tabindex="-1" role="dialog" aria-labelledby="createModalLabel"
    aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <form method="POST" action="{{ route('user.store') }}">
                @csrf
                <div class="modal-header">
                    <h4 class="modal-title" id="createModalLabel"><i class="ti-user mr-2"></i> Add New Customer</h4>
                    <button class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="input-group mb-3">
                        <select class="form-control" name="title" required>
                            <option value="" disabled selected>Select Title</option>
                            <option>Mr</option>
                            <option>Mrs</option>
                            <option>Miss</option>
                            <option>Sir</option>
                            <option>Madam</option>
                        </select>
                    </div>
                    <div class="input-group mb-3">
                        <input type="text" class="form-control" placeholder="Enter Name Here" name="name" required>
                    </div>
                    <div class="input-group mb-3">
                        <input type="text" class="form-control" placeholder="Enter IC Number Here" name="nric" required>
                    </div>
                    <div class="input-group mb-3">
                        <input type="text" class="form-control" pattern="[0-9]+" placeholder="Enter mobile number here (Number only)" name="contact"
                            required>
                    </div>
                    <div class="input-group mb-3">
                        <input type="Email" class="form-control" placeholder="Enter Email Here" name="email" required>
                    </div>
                    <input type="hidden" name="role" value="Customer">
                </div>
                <div class="modal-footer">
                    <button class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                    <button type="submit" class="btn btn-success"><i class="ti-save mr-2"></i> Save</button>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection

@section('script')
{{-- jSignature --}}
<script>
    (function($) {
        var topics = {};
        $.publish = function(topic, args) {
            if (topics[topic]) {
                var currentTopic = topics[topic],
                args = args || {};

                for (var i = 0, j = currentTopic.length; i < j; i++) {
                    currentTopic[i].call($, args);
                }
            }
        };
        $.subscribe = function(topic, callback) {
            if (!topics[topic]) {
                topics[topic] = [];
            }
            topics[topic].push(callback);
            return {
                "topic": topic,
                "callback": callback
            };
        };
        $.unsubscribe = function(handle) {
            var topic = handle.topic;
            if (topics[topic]) {
                var currentTopic = topics[topic];

                for (var i = 0, j = currentTopic.length; i < j; i++) {
                    if (currentTopic[i] === handle.callback) {
                        currentTopic.splice(i, 1);
                    }
                }
            }
        };
    })(jQuery);

</script>
<script src="{{ asset('js/jSignature/jSignature.min.noconflict.js')}}"></script>
<script src="{{ asset('js/jSignature/modernizr.js')}}"></script>
<script>
    (function($){

    $(document).ready(function() {

        // This is the part where jSignature is initialized.
        var $sigdiv = $("#signature").jSignature({'UndoButton':true})

        , $tools = $('#tools')
        , $extraarea = $('#displayarea')
        , pubsubprefix = 'jSignature.demo.'

        $(document).on("click", "#submitOrder", function () {

            Swal.mixin({
                confirmButtonText: 'Confirm &rarr;',
                showCancelButton: true,
                progressSteps: ['<span class="mdi mdi-calendar-clock"></span>', '<span class="mdi mdi-plus"></span>', '<span class="mdi mdi-truck-delivery"></span>']
            }).queue([{
                    title: 'Collection Time',
                    text: $('#date-format').val()
                },
                {
                    title: 'Created Branch',
                    text: $('#createdbranch_id option:selected').text()
                },
                {
                    title: 'Collection Branch',
                    text: $('#collectbranch_id option:selected').text()
                }
            ]).then((result) => {
                if (result.value) {
                    if($('#sketchTrue').is(':checked'))
                        $.fn.bcPaint.export();
                    var data = $sigdiv.jSignature('getData', 'default');
                    $('textarea', $tools).val(data);
                    $('#confirmOrder').click();
                    return false;
                }
            })

        });

        // $('<div class="row"><input type="button" value="Reset" class="btn btn-block text-center btn-primary mx-3 mt-1"></div>').bind('click', function(e){
        //     $sigdiv.jSignature('reset')
        // }).appendTo($tools)

        $('<div><textarea name="signature" style="display:none;"></textarea></div>').appendTo($tools)

        if (Modernizr.touch){
            $('#scrollgrabber').height($('#content').height())
        }

    })

    })(jQuery)
</script>
<script src="{{ asset('js/bcPaint/bcPaint.js')}}"></script>

<script>

    function showUpload()
	{
        $("input#ufile").prop( "disabled", false );
        $("#uploadFile").show();
	}

    function hideUpload()
    {
        $("input#ufile").prop( "disabled", true );
        $("#uploadFile").hide();
    }

    function showSketch()
	{
        $("#sketch").prop( "disabled", false );
        $("#bcPaint").show();
	}

    function hideSketch()
    {
        $("#sketch").prop( "disabled", true );
        $("#bcPaint").hide();
        $.fn.bcPaint.clearCanvas();
    }

    $(document).ready(function () {
        $('#bcPaint').bcPaint();
        hideSketch();
        hideUpload();
        $('#date-format').bootstrapMaterialDatePicker({ format: 'YYYY-MM-DD H:mm' });

        function ValidateSize(file) {
            if (file.size >  {{ Config::get('medialibrary.max_file_size') }})
                swal("Warning", "File size exceeds " + {{ Config::get('medialibrary.max_file_size') }}/1024/1024 + " MB!", "warning");
            else
                $("#output").append(file.name + "(TYPE: " + file.type + ", SIZE: " + (file.size/1024/1024).toFixed(2) + "MB)");
        }

    $(document).on("change", "#customer_id" , function() {

        $.ajax({
            type: 'GET',
            url: '/order/getCustomer/' + this.value,
            success: function (data) {
                $('input[type="checkbox"]').prop('checked', false);
                $("input[id="+ data.customer.title +"]").prop("checked", true);
                $('#name').attr('value', data.customer.name);
                $('#nric').attr('value', data.customer.nric);
                $('#contact').attr('value', data.customer.contact);
                $('#email').attr('value', data.customer.email);
            }
        });

    });

    $("input#ufile").change(function() {
        $("#output").empty();
        var ele = document.getElementById($(this).attr('id'));
        var result = ele.files;
        for(var x = 0;x< result.length;x++){
        var fle = result[x];
        ValidateSize(fle);
    }
    });

    $('#price').on('change', function () {
        $('#paid').attr("max", $(this).val());
    });

    $('#discount').on('keyup', function () {
        $discount = $(this).val();
        $price = $('#price').val();
        $newprice = (100-$discount)/100*$price;

        if($discount != '')
        {
            $('#paid').attr("placeholder", 'New Price: RM' + $newprice.toFixed(2));
            $('#paid').attr("max", $newprice.toFixed(2));
        }
        else
            $('#paid').removeAttr('placeholder');
    });

    $('#price').on('keyup', function () {
        $discount = $('#discount').val();
        $price = $('#price').val();
        $newprice = (100-$discount)/100*$price;

        if($discount != '')
        {
            $('#paid').attr("placeholder", 'New Price: RM' + $newprice.toFixed(2));
            $('#paid').attr("max", $newprice.toFixed(2));
        }
        else
            $('#paid').removeAttr('placeholder');
    });

});
</script>

<!--Custom JavaScript -->
<script src="{{ asset('assets/libs/sweetalert2/dist/sweetalert2.all.min.js') }}"></script>
<script src="{{ asset('assets/libs/select2/dist/js/select2.full.min.js') }}"></script>
<script src="{{ asset('dist/js/pages/forms/select2/select2.init.js') }}"></script>
<script src="{{ asset('assets/libs/moment/moment.js') }}"></script>
<script src="{{ asset('assets/libs/bootstrap-material-datetimepicker/js/bootstrap-material-datetimepicker-custom.js') }}"></script>
@endsection
