@extends('layouts.navbar')
@section('head')
<link rel="stylesheet" type="text/css" href="{{ asset('assets/libs/bootstrap-material-datetimepicker/css/bootstrap-material-datetimepicker.css')}}">
<link rel="stylesheet" type="text/css" href="{{ asset('css/fileUploader.css')}}">
<link rel="stylesheet" type="text/css" href="{{ asset('css/bcPaint.css')}}">
<link rel="stylesheet" type="text/css" href="{{ asset('css/bcPaint.mobile.css')}}">
@endsection
@section('breadcrumb', 'Edit Order #'.$order->id)
@section('content')
<div class="col-sm-12">
    <div class="card">
            <div class="d-flex no-block align-items-center col-lg-12 py-3 mb-4" style="background: #2cabe3;">
            <h4 class="card-title mb-0 ml-3 text-white">Edit {{$order->form->name}} Order #{{$order->id}}</h4>
            </div>
            <div class="card-body">
                <h4 class="card-title mb-0">CUSTOMER INFORMATION</h4>
                <hr class="mb-4">
                <div class="row mb-3">
                    <div class="form-group col-sm-6">
                        <div class="d-flex no-block align-items-center">
                            <label class="font-medium">Existing Customer</label>
                        </div>
                        <input value="{{$order->customer->name}} - {{$order->customer->nric}}" class="form-control" disabled>
                    </div>
                    <div class="form-group col-sm-6">
                        <label class="font-medium">Title</label><br>
                        <div class="form-check form-check-inline">
                            <div class="custom-control custom-checkbox">
                                <input type="checkbox" class="custom-control-input" id="Mr" disabled {{$order->customer->title == 'Mr'  ? 'checked' : ''}}>
                                <label class="custom-control-label">Mr.</label>
                            </div>
                        </div>
                        <div class="form-check form-check-inline">
                            <div class="custom-control custom-checkbox">
                                <input type="checkbox" class="custom-control-input" id="Mrs" disabled {{$order->customer->title == 'Mrs'  ? 'checked' : ''}}>
                                <label class="custom-control-label">Mrs.</label>
                            </div>
                        </div>
                        <div class="form-check form-check-inline">
                            <div class="custom-control custom-checkbox">
                                <input type="checkbox" class="custom-control-input" id="Miss" disabled {{$order->customer->title == 'Miss'  ? 'checked' : ''}}>
                                <label class="custom-control-label">Miss</label>
                            </div>
                        </div>
                        <div class="form-check form-check-inline">
                            <div class="custom-control custom-checkbox">
                                <input type="checkbox" class="custom-control-input" id="Sir" disabled {{$order->customer->title == 'Sir'  ? 'checked' : ''}}>
                                <label class="custom-control-label">Sir</label>
                            </div>
                        </div>
                        <div class="form-check form-check-inline">
                            <div class="custom-control custom-checkbox">
                                <input type="checkbox" class="custom-control-input" id="Madam" disabled {{$order->customer->title == 'Madam'  ? 'checked' : ''}}>
                                <label class="custom-control-label">Madam</label>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row mb-3">
                    <div class="form-group col-sm-6">
                        <label class="font-medium">Name</label>
                        <input type="text" value="{{$order->customer->name}}" id="name" placeholder="Name" class="form-control" disabled>
                    </div>
                    <div class="form-group col-sm-6">
                        <label class="font-medium">IC Number</label>
                        <input type="text" value="{{$order->customer->nric}}" id="nric" placeholder="IC Number" class="form-control" disabled>
                    </div>
                </div>
                <div class="row mb-5">
                    <div class="form-group col-sm-6">
                        <label class="font-medium">Mobile Number</label>
                        <input type="text" id="contact" value="{{$order->customer->contact}}" placeholder="Mobile Number" class="form-control" disabled>
                    </div>
                    <div class="form-group col-sm-6">
                        <label class="font-medium">Email</label>
                        <input type="text" id="email" value="{{$order->customer->email}}" placeholder="Email" class="form-control" disabled>
                    </div>
                </div>

                <form id="form" class="form-horizontal" enctype="multipart/form-data" method="POST"
                action="{{route('order.update', $order->id)}}">
                {{ csrf_field() }}
                {{ method_field('PUT') }}
                <h4 class="card-title mb-0">DESIGN DETAILS</h4>
                <hr class="mb-4">

                @if($order->form->name != 'Graphic Design')
                @if($order->file() == '#')
                <div class="form-group">
                    <div class="form-check form-check-inline">
                        <div class="custom-control custom-radio">
                            <input type="radio" id="uploadDesign" name="design" class="custom-control-input"
                                value="uploadDesign" onclick="showUpload()" required>
                            <label class="custom-control-label" for="uploadDesign">Upload Design</label>
                        </div>
                    </div>
                    <div class="form-check form-check-inline">
                        <div class="custom-control custom-radio">
                            <input type="radio" id="requestDesign" name="design" class="custom-control-input"
                                value="requestDesign" onclick="hideUpload()" required {{$order->file() == '#'  ? 'checked' : ''}}>
                            <label class="custom-control-label" for="requestDesign">Request Design</label>
                        </div>
                    </div>
                </div>

                <span class="btn btn-light btn-block fileinput-button" id="uploadFile">
                    <h1><span class="mdi mdi-cloud-upload"></span></h1>
                    <p><span>Drag and drop a file here or click</span></p>
                    <div id="output" class="font-light"></div>
                    <input type="file" name="file" id="ufile">
                </span>
                @else
                <a href="{{$order->file()}}" target="_blank" class="btn btn-block btn-success mt-3"><i class="fas fa-download"></i> DOWNLOAD DESIGN</a>

                <span class="btn btn-light btn-block fileinput-button" id="updateFile">
                    <h1><span class="mdi mdi-cloud-upload"></span></h1>
                    <p><span>UPLOAD NEW DESIGN</span></p>
                    <div id="output" class="font-light"></div>
                    <input type="file" name="file" id="udfile">
                </span>
                @endif
                @endif

                <h4 class="card-title mt-5">DESIGN SKETCH</h4>
                <hr class="mb-4">

                @if($order->sketch() == '#')
                    <div class="form-group">
                        <div class="form-check form-check-inline">
                                <div class="custom-control custom-radio">
                                    <input type="radio" id="sketchFalse" name="sketchdesign" class="custom-control-input"
                                        value="sketchFalse" onclick="hideSketch()" required  {{$order->sketch() == '#'  ? 'checked' : ''}}>
                                    <label class="custom-control-label" for="sketchFalse">No Sketching</label>
                                </div>
                            </div>
                        <div class="form-check form-check-inline">
                            <div class="custom-control custom-radio">
                                <input type="radio" id="sketchTrue" name="sketchdesign" class="custom-control-input"
                                    value="sketchTrue" onclick="showSketch()" required>
                                <label class="custom-control-label" for="sketchTrue">Sketch Design</label>
                            </div>
                        </div>
                    </div>

                    <div id="bcPaint"></div>
                    <input type="hidden" name="sketch">
                @else
                    <img id="oldSketch" style="border:1px solid;" class="img-fluid" src="{{$order->sketch()}}" alt="None">
                    <div id="bcPaint"></div>
                    <input type="hidden" name="sketch">
                    <input type="hidden" id="sketchdesign" name="sketchdesign">
                    <button id="updateSketch" onClick="showSketch(); $('#oldSketch').hide(); $(this).hide(); $('#sketchdesign').val('sketchTrue')" type="button" class="btn btn-block btn-primary mt-3"><i class="fas fa-edit"></i> UPDATE NEW SKETCH</button>
                @endif

            <h4 class="card-title mt-5">COLLECT INFORMATION</h4>
                <hr class="mb-4">
                <div class="form-group mb-4">
                    <div class="form-check form-check-inline">
                        <div class="custom-control custom-radio">
                            <input type="radio" id="normal" name="urgency" class="custom-control-input" value="Normal"
                                required {{$order->urgency == 'Normal'  ? 'checked' : ''}}>
                            <label class="custom-control-label" for="normal">Normal Order</label>
                        </div>
                    </div>
                    <div class="form-check form-check-inline">
                        <div class="custom-control custom-radio">
                            <input type="radio" id="urgent" name="urgency" class="custom-control-input" value="Urgent"
                                required  {{$order->urgency == 'Urgent'  ? 'checked' : ''}}>
                            <label class="custom-control-label" for="urgent">Urgent Order</label>
                        </div>
                    </div>
                    <div class="form-check form-check-inline">
                        <div class="custom-control custom-radio">
                            <input type="radio" id="superurgent" name="urgency" class="custom-control-input"
                                value="Super Urgent" required  {{$order->urgency == 'Super Urgent'  ? 'checked' : ''}}>
                            <label class="custom-control-label" for="superurgent">Super Urgent Order</label>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="form-group col-sm-4">
                        <label class="font-medium">Collection Time</label>
                    <input type="text" id="date-format" name="collection" class="form-control" required value="{{$order->collection}}">
                    </div>
                    <div class="form-group col-sm-4">
                        <label class="font-medium">Created Branch</label>
                        <select class="form-control" name="createdbranch_id" id="createdbranch_id">
                            @foreach($branch as $b)
                            <option value="{{$b->id}}" {{$b->id == $order->createdbranch_id  ? 'selected' : ''}}>{{$b->name}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group col-sm-4">
                        <label class="font-medium">Collection Branch</label>
                        <select class="form-control" name="collectbranch_id" id="collectbranch_id">
                            @foreach($branch as $b)
                            <option value="{{$b->id}}" {{$b->id == $order->collectbranch_id  ? 'selected' : ''}}>{{$b->name}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>

                <h4 class="card-title mt-4">SPECIFICATION DETAILS</h4>
                <hr>
                <?php $i = 0; ?>
                @foreach($order->form->field as $f)
                <?php ++$i; ?>
                <div class="form-group">
                    @if($f['type'] == 'dropdown')
                    <label class="font-medium">{{$f['name']}}</label>
                    <select class="form-control" name="{{$f['name']}}">
                        @if(array_key_exists('option', $f))
                        @foreach($f['option'] as $o)
                        <option {{$o == $order->details[$i-1][$f['name']]  ? 'selected' : ''}}>{{$o}}</option>
                        @endforeach
                        @endif
                    </select>
                    @elseif($f['type'] == 'checkbox')
                    <div class="custom-control custom-checkbox mr-sm-2">
                        <input type="checkbox" class="custom-control-input" id="checkbox{{$i}}{{$f['name']}}" name="{{$f['name']}}" value="Yes" {{$order->details[$i-1][$f['name']] == 'Yes'  ? 'checked' : ''}}>
                        <label class="custom-control-label" for="checkbox{{$i}}{{$f['name']}}">{{$f['name']}}</label>
                    </div>
                    @elseif($f['type'] == 'radio')
                    <?php $j=0; ?>
                    @if(array_key_exists('option', $f))
                    <label class="font-medium">{{$f['name']}}</label>
                    @foreach($f['option'] as $o)
                    <div class="custom-control custom-radio">
                        <input type="radio" id="customRadio{{$i}}{{$o}}" name="{{$f['name']}}"
                            class="custom-control-input" value="{{$o}}" {{$f['validation']}} {{$o == $order->details[$i-1][$f['name']]  ? 'checked' : ''}}>
                        <label class="custom-control-label" for="customRadio{{$i}}{{$o}}">{{$o}}</label>
                    </div>
                    @if($o == $order->details[$i-1][$f['name']])
                    <?php $j=1; ?>
                    @endif
                    @endforeach
                    @if($f['custom'] == 'true')
                    <div class="custom-control custom-radio">
                        <input type="radio" id="customRadio{{$i}}custom" name="{{$f['name']}}"
                    class="custom-control-input" value="{{$order->details[$i-1][$f['name']]}}" {{$f['validation']}} {{$j == 0  ? 'checked' : ''}}>
                        <label class="custom-control-label" for="customRadio{{$i}}custom">
                            <input type="text" placeholder="Others" class="form-control form-control-sm"
                                onChange="$('#customRadio{{$i}}custom').val(this.value);" onclick="$('#customRadio{{$i}}custom').prop('checked', true);" value="{{$j == 0  ? $order->details[$i-1][$f['name']] : ''}}"/>
                        </label>
                    </div>
                    @endif
                    @endif

                    @elseif($f['type'] == 'radioImage')
                    <?php $k=0; ?>
                    @if(array_key_exists('option', $f))
                    <label class="font-medium">{{$f['name']}}</label>
                    <div class="row">
                        @foreach($f['option'] as $index=>$o)
                        <div class="col-lg-3 col-sm-6 mb-2" style="width: 100%;height: 15vw;object-fit: cover;">
                            <div class="custom-control custom-radio py-3">
                                <input type="radio" id="customRadioImage{{$i}}{{$o}}" name="{{$f['name']}}"
                                    class="custom-control-input" value="{{$o}}" {{$f['validation']}} {{$o == $order->details[$i-1][$f['name']]  ? 'checked' : ''}}>
                                <label class="custom-control-label text-center"
                                    for="customRadioImage{{$i}}{{$o}}"><img class="img-fluid" style="max-height: 13vw;" src="{{ asset('formimages/'.$f['directory'][$index]) }}"><br>{{$o}}</label>
                            </div>
                        </div>
                        @if($o == $order->details[$i-1][$f['name']])
                        <?php $k=1; ?>
                        @endif
                        @endforeach
                    </div>

                    @if($f['custom'] == 'true')
                        <div class="custom-control custom-radio">
                            <input type="radio" id="customRadioImage{{$i}}custom" name="{{$f['name']}}"
                                class="custom-control-input" value="{{$order->details[$i-1][$f['name']]}}" {{$f['validation']}} {{$k == 0  ? 'checked' : ''}}>
                            <label class="custom-control-label" for="customRadioImage{{$i}}custom"><input
                                    type="text" class="form-control form-control-sm" placeholder="Others"
                                    onChange="$('#customRadioImage{{$i}}custom').val(this.value);" onclick="$('#customRadioImage{{$i}}custom').prop('checked', true);" value="{{$k == 0  ? $order->details[$i-1][$f['name']] : ''}}"/></label>
                        </div>
                    @endif
                    @endif

                    @else
                    <label class="font-medium">{{$f['name']}}</label>
                    <input type="{{$f['type']}}" name="{{$f['name']}}" class="form-control" {{$f['validation']}} value="{{$order->details[$i-1][$f['name']]}}">
                    @endif
                </div>
                @endforeach

                <div class="form-group">
                    <label class="font-medium">Remark (Optional)</label>
                    <textarea rows="3" cols="50" class="form-control" name="remark">{{$order->remark}}</textarea>
                </div>

                <h4 class="card-title mt-5">PAYMENT INFORMATION</h4>
                <hr class="mb-4">
                <div class="form-group mb-4">
                    <div class="form-check form-check-inline">
                        <div class="custom-control custom-radio">
                            <input type="radio" id="paymentcod" name="paymentmethod" class="custom-control-input"
                                value="Cash on Delivery" onclick="$('#referencediv').hide();$('#reference').prop('required', false);$('#reference').prop('disabled', true);" required {{$order->paymentmethod == 'Cash on Delivery'  ? 'checked' : ''}}>
                            <label class="custom-control-label" for="paymentcod">Cash on Delivery</label>
                        </div>
                    </div>
                    <div class="form-check form-check-inline">
                        <div class="custom-control custom-radio">
                            <input type="radio" id="paymentlo" name="paymentmethod" class="custom-control-input"
                                value="Purchase Order" onclick="$('#referencediv').show();$('#reference').prop('required', true);$('#reference').prop('disabled', false);" required {{$order->paymentmethod == 'Purchase Order'  ? 'checked' : ''}}>
                            <label class="custom-control-label" for="paymentlo">Purchase Order</label>
                        </div>
                    </div>
                    <div class="form-check form-check-inline">
                        <div class="custom-control custom-radio">
                            <input type="radio" id="paymentterm" name="paymentmethod" class="custom-control-input"
                                value="Payment Term" onclick="$('#referencediv').show();$('#reference').prop('required', true);$('#reference').prop('disabled', false);" required {{$order->paymentmethod == 'Payment Term'  ? 'checked' : ''}}>
                            <label class="custom-control-label" for="paymentterm">Payment Term</label>
                        </div>
                    </div>
                </div>
                <div class="row mb-0">
                    <div class="form-group col-sm-3">
                        <label class="font-medium">Total Price (MYR)</label>
                        <input type="number" step="0.01" min="0.01" class="form-control" name="price" id="price" required value="{{$order->price}}">
                    </div>
                    <div class="form-group col-sm-3">
                        <label class="font-medium">Discount (%)</label>
                        <input type="number" step="0.1" min="0" max="100" class="form-control" name="discount" id="discount" value="{{$order->discount}}">
                    </div>
                    <div class="form-group col-sm-3">
                        <label class="font-medium">Payment (MYR)</label>
                        <input type="number" step="0.01" class="form-control" name="paid" id="paid" value="{{$order->paid}}" min="0.01" max="{{(100-$order->discount)/100*$order->price}}" required>
                    </div>
                    <div class="form-group col-sm-3"  id="referencediv" {!!$order->paymentmethod == 'Cash on Delivery'  ? 'style="display: none;"' : ''!!}>
                        <label class="font-medium">Reference</label>
                        <input class="form-control" name="paymentreference" id="reference" value="{{$order->paymentreference}}" {{$order->paymentmethod == 'Cash on Delivery'  ? 'disabled' : ''}}>
                    </div>
                </div>

                <h4 class="card-title mt-5">CUSTOMER SIGNATURE</h4>
                <hr>
                <div style="margin-right:50%;">
                    <img class="img-fluid" src="{{$order->signature()}}" alt="None">
                </div>

                <div class="d-flex no-block align-items-center col-lg-12 mt-4 pt-4 pb-4">
                    <button type="button" id="submitOrder" class="btn btn-success waves-effect waves-light ml-2">Submit</button>
                    <input type="submit" id="confirmOrder" style="display:none;">
                </div>
            </div>
        </form>
    </div>
</div>
@endsection

@section('script')
<script src="{{ asset('js/bcPaint/bcPaint.js')}}"></script>
<script>

    function showUpload()
	{
        $("input#ufile").prop( "disabled", false );
        $("#uploadFile").show();
	}

    function hideUpload()
    {
        $("input#ufile").prop( "disabled", true );
        $("#uploadFile").hide();
    }

    function showSketch()
	{
        $("#sketch").prop( "disabled", false );
        $("#bcPaint").show();
	}

    function hideSketch()
    {
        $("#sketch").prop( "disabled", true );
        $("#bcPaint").hide();
        $.fn.bcPaint.clearCanvas();
    }

    $(document).on("click", "#submitOrder", function () {
        if ($('#sketchdesign').val() == 'sketchTrue' || $('#sketchTrue').val() == 'sketchTrue' )
            $.fn.bcPaint.export();
        $('#confirmOrder').click();
        return false;
    });

    $(document).ready(function () {
        $('#bcPaint').bcPaint();
        hideSketch();
        hideUpload();
        $('#date-format').bootstrapMaterialDatePicker({ format: 'YYYY-MM-DD H:mm' });

        function ValidateSize(file) {
            if (file.size >  {{ Config::get('medialibrary.max_file_size') }})
                swal("Warning", "File size exceeds " + {{ Config::get('medialibrary.max_file_size') }}/1024/1024 + " MB!", "warning");
            else
                $("#output").append(file.name + "(TYPE: " + file.type + ", SIZE: " + (file.size/1024/1024).toFixed(2) + "MB)");
        }

    $("input#ufile").change(function() {
        $("#output").empty();
        var ele = document.getElementById($(this).attr('id'));
        var result = ele.files;
        for(var x = 0;x< result.length;x++){
        var fle = result[x];
        ValidateSize(fle);
    }
    });

    $("input#udfile").change(function() {
        $("#output").empty();
        var ele = document.getElementById($(this).attr('id'));
        var result = ele.files;
        for(var x = 0;x< result.length;x++){
        var fle = result[x];
        ValidateSize(fle);
    }
    });

    $('#price').on('change', function () {
        $('#paid').attr("max", $(this).val());
    });

    $('#discount').on('keyup', function () {
        $discount = $(this).val();
        $price = $('#price').val();
        $newprice = (100-$discount)/100*$price;

        if($discount != '')
        {
            $('#paid').attr("placeholder", 'New Price: RM' + $newprice.toFixed(2));
            $('#paid').attr("max", $newprice.toFixed(2));
        }
        else
            $('#paid').removeAttr('placeholder');
    });

    $('#price').on('keyup', function () {
        $discount = $('#discount').val();
        $price = $('#price').val();
        $newprice = (100-$discount)/100*$price;

        if($discount != '')
        {
            $('#paid').attr("placeholder", 'New Price: RM' + $newprice.toFixed(2));
            $('#paid').attr("max", $newprice.toFixed(2));
        }
        else
            $('#paid').removeAttr('placeholder');
    });

});
</script>

<!--Custom JavaScript -->
<script src="{{ asset('assets/libs/sweetalert2/dist/sweetalert2.all.min.js') }}"></script>
<script src="{{ asset('assets/libs/moment/moment.js') }}"></script>
<script src="{{ asset('assets/libs/bootstrap-material-datetimepicker/js/bootstrap-material-datetimepicker-custom.js') }}"></script>
@endsection
