@if($idsearch->count() > 0)
<h5>ORDER ID SEARCH</h5>
<table id="idSearchTable" class="table no-wrap user-table mb-3">
    <thead>
        <tr>
            <th>ORDER ID</th>
            <th>ORDER TYPE</th>
            <th>DEPT IN CHARGE</th>
            <th>URGENCY</th>
            <th>STATUS</th>
            <th>CUSTOMER</th>
            <th>COLLECTION TIME</th>
            <th>CREATED AT</th>
            <th>CREATED BY</th>
        </tr>
    </thead>
    <tbody>
            @foreach($idsearch as $s)
            <tr>
                <td>{{$s->id}}</td>
                <td>{{$s->form['name']}}</td>
                <td>
                    {{$s->department['name']}}<br>
                    @if($s->staff_id == null)
                    <span class="text-danger">Unassigned</span>
                    @else
                    <span class="text-muted">{{$s->staff['name']}}</span>
                    @endif
                </td>
                <td>
                    @if($s->urgency == 'Super Urgent')
                    <span class="badge badge-danger">{{$s->urgency}}</span>
                    @elseif($s->urgency == 'Urgent')
                    <span class="badge badge-warning">{{$s->urgency}}</span>
                    @elseif($s->urgency == 'Normal')
                    <span class="badge badge-info">{{$s->urgency}}</span>
                    @endif
                </td>
                <td>
                    @if($s->status == 'Received')
                    <span class="badge badge-primary">Received</span>
                    @elseif($s->status == 'Delivered')
                    <span class="badge badge-success">Delivered</span>
                    @elseif($s->status == 'In Process')
                    <span class="badge badge-info">In Process <button type="button" title="Hold Task" class="holdOrder"
                            data-id="{{$s->id}}"><i class="mdi mdi-pause"></i></button></span>
                    @elseif($s->status == 'On Hold')
                    <span class="badge badge-secondary">On Hold <button type="button" title="Resume Task"
                            class="resumeOrder" data-id="{{$s->id}}"><i class="mdi mdi-play"></i></button></span>
                    <p><span class="text-muted">{{$s->onholdreason}}</span></p>
                    @else
                    <span class="badge badge-info">{{$s->status}}</span>
                    @endif
                </td>
                <td>{{$s->customer['name']}}<br>
                    <span class="text-muted">{{$s->customer['email']}}</span>
                </td>
                <td>{{date('d/m/Y', strtotime($s->collection))}}<br>{{date('g:i A', strtotime($s->collection))}}
                </td>
                <td>{{date('d/m/Y', strtotime($s->created_at))}}<br>{{date('g:i A', strtotime($s->created_at))}}
                </td>
                <td>{{$s->creator['name']}}</td>
            </tr>
            @endforeach
    </tbody>
</table>
@endif

@if($contactsearch->count() > 0)
<h5>PHONE NUMBER SEARCH</h5>
<table id="contactSearchTable" class="table no-wrap user-table mb-3">
    <thead>
        <tr>
            <th>ORDER ID</th>
            <th>ORDER TYPE</th>
            <th>DEPT IN CHARGE</th>
            <th>URGENCY</th>
            <th>STATUS</th>
            <th>CUSTOMER</th>
            <th>COLLECTION TIME</th>
            <th>CREATED AT</th>
            <th>CREATED BY</th>
        </tr>
    </thead>
    <tbody>
        @foreach($contactsearch as $s)
        <tr>
            <td>{{$s->id}}</td>
            <td>{{$s->form['name']}}</td>
            <td>
                {{$s->department['name']}}<br>
                @if($s->staff_id == null)
                <span class="text-danger">Unassigned</span>
                @else
                <span class="text-muted">{{$s->staff['name']}}</span>
                @endif
            </td>
            <td>
                @if($s->urgency == 'Super Urgent')
                <span class="badge badge-danger">{{$s->urgency}}</span>
                @elseif($s->urgency == 'Urgent')
                <span class="badge badge-warning">{{$s->urgency}}</span>
                @elseif($s->urgency == 'Normal')
                <span class="badge badge-info">{{$s->urgency}}</span>
                @endif
            </td>
            <td>
                @if($s->status == 'Received')
                <span class="badge badge-primary">Received</span>
                @elseif($s->status == 'Delivered')
                <span class="badge badge-success">Delivered</span>
                @elseif($s->status == 'In Process')
                <span class="badge badge-info">In Process <button type="button" title="Hold Task" class="holdOrder"
                        data-id="{{$s->id}}"><i class="mdi mdi-pause"></i></button></span>
                @elseif($s->status == 'On Hold')
                <span class="badge badge-secondary">On Hold <button type="button" title="Resume Task"
                        class="resumeOrder" data-id="{{$s->id}}"><i class="mdi mdi-play"></i></button></span>
                <p><span class="text-muted">{{$s->onholdreason}}</span></p>
                @else
                <span class="badge badge-info">{{$s->status}}</span>
                @endif
            </td>
            <td>{{$s->customer['name']}}<br>
                <span class="text-muted">{{$s->customer['email']}}</span>
            </td>
            <td>{{date('d/m/Y', strtotime($s->collection))}}<br>{{date('g:i A', strtotime($s->collection))}}
            </td>
            <td>{{date('d/m/Y', strtotime($s->created_at))}}<br>{{date('g:i A', strtotime($s->created_at))}}
            </td>
            <td>{{$s->creator['name']}}</td>
        </tr>
        @endforeach
    </tbody>
</table>
@endif

@if($creatorsearch->count() > 0)
<h5>CREATOR SEARCH</h5>
<table id="creatorSearchTable" class="table no-wrap user-table mb-3">
    <thead>
        <tr>
            <th>ORDER ID</th>
            <th>ORDER TYPE</th>
            <th>DEPT IN CHARGE</th>
            <th>URGENCY</th>
            <th>STATUS</th>
            <th>CUSTOMER</th>
            <th>COLLECTION TIME</th>
            <th>CREATED AT</th>
            <th>CREATED BY</th>
        </tr>
    </thead>
    <tbody>
        @foreach($creatorsearch as $s)
        <tr>
            <td>{{$s->id}}</td>
            <td>{{$s->form['name']}}</td>
            <td>
                {{$s->department['name']}}<br>
                @if($s->staff_id == null)
                <span class="text-danger">Unassigned</span>
                @else
                <span class="text-muted">{{$s->staff['name']}}</span>
                @endif
            </td>
            <td>
                @if($s->urgency == 'Super Urgent')
                <span class="badge badge-danger">{{$s->urgency}}</span>
                @elseif($s->urgency == 'Urgent')
                <span class="badge badge-warning">{{$s->urgency}}</span>
                @elseif($s->urgency == 'Normal')
                <span class="badge badge-info">{{$s->urgency}}</span>
                @endif
            </td>
            <td>
                @if($s->status == 'Received')
                <span class="badge badge-primary">{{$s->status}}</span>
                @elseif($s->status == 'Delivered')
                <span class="badge badge-success">{{$s->status}}</span>
                @elseif($s->onholdreason != null)
                <span class="badge badge-secondary">On Hold <button type="button"  title="Resume Task" class="resumeOrder" data-id="{{$s->id}}"><i class="mdi mdi-play"></i></button></span>
                <p><span class="text-muted">{{$s->onholdreason}}</span></p>
                @elseif($s->status == 'Pending')
                <span class="badge badge-warning">{{$s->status}} <button type="button" title="Hold Task" class="holdOrder" data-id="{{$s->id}}"><i class="mdi mdi-pause"></i></button></span>
                @else
                <span class="badge badge-info">{{$s->status}} <button type="button" title="Hold Task" class="holdOrder" data-id="{{$s->id}}"><i class="mdi mdi-pause"></i></button></span>
                @endif
            </td>
            <td>{{$s->customer['name']}}<br>
                <span class="text-muted">{{$s->customer['email']}}</span>
            </td>
            <td>{{date('d/m/Y', strtotime($s->collection))}}<br>{{date('g:i A', strtotime($s->collection))}}
            </td>
            <td>{{date('d/m/Y', strtotime($s->created_at))}}<br>{{date('g:i A', strtotime($s->created_at))}}
            </td>
            <td>{{$s->creator['name']}}</td>
        </tr>
        @endforeach
    </tbody>
</table>
@endif
