@extends('layouts.navbar')
@section('breadcrumb', 'Create Order')
@section('content')
<div class="row el-element-overlay">
    @foreach($form as $f)
    <div class="col-lg-3 col-sm-6">
        <div class="card">
            <div class="el-card-item">
                <div class="el-card-avatar el-overlay-1"> <img src="{!! $f->photo() !!}" alt="user" style="width: 100%;height: 20vw;object-fit: cover;"/>
                    <div class="el-overlay">
                        <ul class="list-style-none el-info">
                            <li class="el-item"><a class="btn default btn-outline image-popup-vertical-fit el-link" href="{{route('order.form', $f->id)}}"><i class="icon-plus"></i></a></li>
                        </ul>
                    </div>
                </div>
                <div class="el-card-content">
                    <h4 class="mb-0">{{$f->name}}</h4> <span class="text-muted">{!! $f->description == null  ? "<br>" : $f->description !!}</span>
                </div>
            </div>
        </div>
    </div>
    @endforeach
</div>
@endsection
