@extends('layouts.master')

@section('main')
<div class="page-wrapper">
    <div class="page-breadcrumb bg-light">
        <table border="0" cellpadding="0" cellspacing="0" style="width: 100%;">
            <tbody>
                <tr>
                    <td style="vertical-align: top;" align="center"><a href="{{route('login')}}" target="_blank">
                            <img src={{ asset('assets/images/logos/printexpert-logo.png') }} alt="PrintExpert"
                                style="border:none;width:150px;height:50px"></a> </td>
                </tr>
            </tbody>
        </table>
    </div>

    <div class="page-content container-fluid">
        <div class="col-lg-12">
            @if(session()->has('message'))
                <div class="alert alert-success"> <i class="ti-check"></i>
                    {{ session()->get('message') }}
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span
                            aria-hidden="true">×</span>
                    </button>
                </div>
            @endif
            <div class="card">
                <div class="card-body">
                    <div class="card-title">
                        <h2 class="qr-title-top box-title mt-0" align="center">Order #{{$order->id}}</h2>
                    </div>
                    @if($order->department['name'] == 'Production' && auth()->user()->department['name'] == 'Collection')
                    <div class="row">
                        <div class="col-lg-12 col-md-12 col-sm-12">
                            <form action="{{route('qrDetailUpdate', $order->id)}}" method="POST">
                                {{ csrf_field() }}
                                {{ method_field('PUT') }}
                                <button type="submit" class="btn btn-outline-success btn-block"><i class="far fa-check-circle"></i> Mark as Completed</button>
                            </form>
                        </div>
                    </div>
                    @endif
                    <div class="row">
                        <div class="col-lg-12 col-md-12 col-sm-12">
                            <h3 class="qr-title-bottom box-title mt-3">Attached Order Details</h3>
                            <div class="table-responsive">
                                <table class="table">
                                    <tbody>
                                        <tr>
                                            <th>ID</th>
                                            <td>:</td>
                                            <td>{{$order->id}}</td>
                                        </tr>
                                        <tr>
                                            <th>Order Type</th>
                                            <td>:</td>
                                            <td>{{$order->form['name']}}</td>
                                        </tr>
                                        <tr>
                                            <th>Status</th>
                                            <td>:</td>
                                            <td>{{$order->status}}</td>
                                        </tr>
                                        <tr>
                                            <th>Urgency</th>
                                            <td>:</td>
                                            <td>
                                                {{$order->urgency}}
                                            </td>
                                        </tr>
                                        <tr>
                                            <th>Dept in Charge</th>
                                            <td>:</td>
                                            <td>
                                                {{$order->department['name']}}<br>
                                                @if($order->staff_id == null)
                                                <span class="text-danger">Unassigned</span>
                                                @else
                                                <span class="text-muted">{{$order->staff['name']}}</span>
                                                @endif</td>
                                        </tr>
                                        <tr>
                                            <th>Collect Date</th>
                                            <td>:</td>
                                            <td>{{date('d/m/Y', strtotime($order->collection))}}</td>
                                        </tr>
                                        <tr>
                                            <th>Collect Time</th>
                                            <td>:</td>
                                            <td>{{date('g:i A', strtotime($order->collection))}}</td>
                                        </tr>
                                        <tr>
                                            <th>Created Branch</th>
                                            <td>:</td>
                                            <td>{{$order->createdbranch['name']}}</td>
                                        </tr>
                                        <tr>
                                            <th>Collection Branch</th>
                                            <td>:</td>
                                            <td>{{$order->collectbranch['name']}}</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div class="col-lg-12 col-md-12 col-sm-12">
                            <h3 class="qr-title-bottom box-title mt-2">Customer Info</h3>
                            <div class="table-responsive">
                                <table class="table">
                                    <tbody>
                                        <tr>
                                            <th>Name</th>
                                            <td>:</td>
                                            <td>{{$order->customer->name}}</td>
                                        </tr>
                                        <tr>
                                            <th>Mobile No</th>
                                            <td>:</td>
                                            <td>{{$order->customer->contact}}</td>
                                        </tr>
                                        <tr>
                                            <th>Email</th>
                                            <td>:</td>
                                            <td>{{$order->customer->email}}</td>
                                        </tr>
                                        <tr>
                                            <th>Signature</th>
                                            <td>:</td>
                                            <td>
                                                <img class="img-fluid" src="{{$order->signature()}}" alt="None">
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div class="col-lg-12 col-md-12 col-sm-12">
                            <h3 class="qr-title-bottom box-title mt-2">Order Info</h3>
                            <div class="table-responsive">
                                <table class="table">
                                    <tbody>
                                        @foreach($order->details as $d)
                                        @foreach($d as $key=>$val)
                                        @if($key == 'Roundcorner')
                                        <tr>
                                        <div id="d-roundcorner" style="display: none;" data-value="{{$val}}"></div>
                                        <div class="container" id="carddesign" style="border: 2px #8898aa solid; height:100px;">
                                                <h3 style="text-align: center; vertical-align: middle; line-height: 100px; color: #8898aa">Card Corner</h3>
                                        </div>
                                    </tr>
                                        @continue
                                        @endif
                                        @if($val)
                                        <tr>
                                            <th>{{$key}}</th>
                                            <td>:</td>
                                            <td>{{$val}}</td>
                                        </tr>
                                        @endif
                                        @endforeach
                                        @endforeach
                                        @if($order->remark)
                                        <tr>
                                            <th>Remark</th>
                                            <td>:</td>
                                            <td>{{$order->remark}}</td>
                                        </tr>
                                        @endif
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        @if($order->sketch() != '#')
                        <div class="col-lg-12 col-md-12 col-sm-12">
                            <h3 class="qr-title-bottom box-title mt-3">Design Sketch</h3>
                            <div class="container MY-2" style="border: solid 1px #dee2e6;">
                                <img class="img-fluid" src="{{$order->sketch()}}">
                            </div>
                        </div>
                        @endif
                        @if($order->file() != '#')
                        <a href="{{$order->file()}}" target="_blank" class="btn btn-block btn-success mt-3"><i class="fas fa-download"></i> DOWNLOAD</a>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('script')
<script>
$(document).ready(function () {
    if( $('#d-roundcorner').length )
        {
            if($('#d-roundcorner').attr("data-value").indexOf('1') != -1)
                $('#carddesign').css('border-top-left-radius', '20px');
            else
                $('#carddesign').css('border-top-left-radius', '2px');

            if($('#d-roundcorner').attr("data-value").indexOf('2') != -1)
                $('#carddesign').css('border-top-right-radius', '20px');
            else
                $('#carddesign').css('border-top-right-radius', '2px');

            if($('#d-roundcorner').attr("data-value").indexOf('3') != -1)
                $('#carddesign').css('border-bottom-left-radius', '20px');
            else
                $('#carddesign').css('border-bottom-left-radius', '2px');

            if($('#d-roundcorner').attr("data-value").indexOf('4') != -1)
                $('#carddesign').css('border-bottom-right-radius', '20px');
            else
                $('#carddesign').css('border-bottom-right-radius', '2px');
        }
});
</script>
@endsection

