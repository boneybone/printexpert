<table id="orderTable" class="table no-wrap user-table mb-3">
    <thead>
        <tr>
            <th>ID</th>
            <th>ORDER TYPE</th>
            <th>DEPARTMENT</th>
            <th>URGENCY</th>
            <th>STATUS</th>
            <th>CUSTOMER</th>
            <th>COLLECTION</th>
            <th>CREATED AT</th>
            <th>CREATED BY</th>
            <th>ACTION</th>
        </tr>
    </thead>
    <tbody>
        @foreach($order as $o)
        <tr>
            <td>{{$o->id}}</td>
            <td>{{$o->form['name']}}</td>
            <td>
                {{$o->department['name']}}<br>
                @if($o->staff_id == null)
                <span class="text-danger">Unassigned</span>
                @else
                <span class="text-muted">{{$o->staff['name']}}</span>
                @endif
            </td>
            <td>
                @if($o->urgency == 'Super Urgent')
                <span class="badge badge-danger">{{$o->urgency}}</span>
                @elseif($o->urgency == 'Urgent')
                <span class="badge badge-warning">{{$o->urgency}}</span>
                @elseif($o->urgency == 'Normal')
                <span class="badge badge-info">{{$o->urgency}}</span>
                @endif
            </td>
            <td>
                @if($o->onholdreason != null)
                <span class="badge badge-secondary">On Hold <button type="button"  title="Resume Task" class="resumeOrder" data-id="{{$o->id}}"><i class="mdi mdi-play"></i></button></span>
                <p><span class="text-muted">{{$o->onholdreason}}</span></p>
                @elseif($o->status == 'Received')
                <span class="badge badge-primary">{{$o->status}}</span>
                @elseif($o->status == 'Delivered')
                <span class="badge badge-success">{{$o->status}}</span>
                @elseif($o->status == 'Pending')
                <span class="badge badge-warning">{{$o->status}} <button type="button" title="Hold Task" class="holdOrder" data-id="{{$o->id}}"><i class="mdi mdi-pause"></i></button></span>
                @else
                <span class="badge badge-info">{{$o->status}} <button type="button" title="Hold Task" class="holdOrder" data-id="{{$o->id}}"><i class="mdi mdi-pause"></i></button></span>
                @endif
            </td>
            <td>{{$o->customer['name']}}<br>
                <span class="text-muted">{{$o->customer['email']}}</span>
            </td>
            <td>{{date('d/m/Y', strtotime($o->collection))}}<br>{{date('g:i A', strtotime($o->collection))}}
            </td>
            <td>{{date('d/m/Y', strtotime($o->created_at))}}<br>{{date('g:i A', strtotime($o->created_at))}}
            </td>
            <td>{{$o->creator['name']}}</td>
            <td>
                <a href="{{route('order.detail', $o->id)}}" class="btn btn-circle btn-outline-success btn-sm ml-2" title="View Order"><i class="fas fa-eye"></i></a>
            @if($o->staff_id == null && ($o->department->id == Auth::user()->department_id && Auth::user()->department->name != 'Collection'))
                <button class="takeorder btn btn-circle btn-outline-info btn-sm ml-2" data-id="{{$o->id}}" title="Assign Order to Yourself"><i class="fas fa-reply"></i></button>
            @endif
            </td>
        </tr>
        @endforeach
    </tbody>
</table>
