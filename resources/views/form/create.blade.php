@extends('layouts.navbar')
@section('breadcrumb', 'Create Form')
@section('content')
<div class="col-sm-12">
    <div class="card">

        <form class="form-horizontal" enctype="multipart/form-data" method="POST" action="{{route('form.store')}}">
            @csrf
            <div class="d-flex no-block align-items-center col-lg-12 py-3" style="background: #2cabe3;">
                <h4 class="card-title mb-0 ml-3 text-white">Create Form</h4>
            </div>
            <div class="card-body">
                <div class="form-group">
                    <label>Form Name</label>
                    <input type="text" class="form-control" name="name" required>
                </div>
                <div class="form-group">
                    <label>Form Description</label>
                    <input type="text" class="form-control" name="description">
                </div>
                <div class="form-group">
                    <label>Form Thumbnail</label>
                    <input type="file" name="file" class="form-control-file" onchange="ValidateSize(this)" required>
                </div>
                <hr>
                <div id="form-row">
                    <div id="field1">
                    <div class="row">
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label>Field Name</label>
                                <input type="text" class="form-control" name="name1" required>
                            </div>
                        </div>
                        <div class="col-sm-4" id="type1">
                            <div class="form-group">
                                <label>Field Type</label>
                                <select class="select-type form-control" name="type1">
                                        <option value="checkbox">Checkbox</option>
                                        <option value="date">Date</option>
                                        <option value="dropdown">Dropdown</option>
                                        <option value="email">Email</option>
                                        <option value="number">Number</option>
                                        <option value="radio">Radio Button</option>
                                        <option value="radioImage">Radio Button + Image</option>
                                        <option value="text" selected>Text</option>
                                        <option value="time">Time</option>
                                        <option value="url">URL</option>

                                </select>
                            </div>
                        </div>

                        <div class="col-sm-2">
                                <div class="form-group">
                                    <label>Others Field</label>
                                    <div class="custom-control custom-switch">
                                        <input type="checkbox" class="custom-control-input form-control"
                                            name="custom1" id="otherSwitch1" value="true" disabled>
                                        <label class="custom-control-label" for="otherSwitch1">Required</label>
                                    </div>
                                </div>
                            </div>
                        <div class="col-sm-2">
                            <div class="form-group">
                                <label>Validation</label>
                                <div class="custom-control custom-switch">
                                    <input type="checkbox" class="custom-control-input form-control" name="validation1"
                                        id="customSwitch1" value="required">
                                    <label class="custom-control-label" for="customSwitch1">Required</label>
                                </div>
                            </div>
                        </div>
                        <input type="hidden" name="optioncount1" id="optioncount1" value="0">
                    </div>
                    </div>
                </div>
                <a href="#" class="add-field">Add Field</a>
                <input type="hidden" name="fieldcount" value="1" id="fieldcount">
                <div class="d-flex no-block align-items-center col-lg-12 mt-4 pt-4 pl-0" style="border-top: 1px solid #eaeaea;">
                    <a href="#" class="reset-field btn btn-danger waves-effect waves-light">Reset</a>
                    <button type="submit" class="btn btn-success waves-effect waves-light ml-2">Submit</button>
                </div>
            </div>
        </form>
    </div>
</div>
@endsection

@section('script')
<script>
$(document).ready(function () {
//in case user change dropdown, error will happen
    $(document).on("change", "select.select-type" , function() {

        var selectName = this.name;
        var row = selectName.slice(4, selectName.length);
        var originalElement = '<div class="row"> <div class="col-sm-4"> <div class="form-group"> <label>Field Name</label> <input type="text" class="form-control" name="name' + row + '" required> </div> </div> <div class="col-sm-4" id="type' + row + '"> <div class="form-group"> <label>Field Type</label> <select class="select-type form-control" name="type' + row + '"> <option value="checkbox">Checkbox</option> <option value="date">Date</option> <option value="dropdown">Dropdown</option> <option value="email">Email</option> <option value="number">Number</option> <option value="radio">Radio Button</option> <option value="radioImage">Radio Button + Image</option> <option value="text">Text</option> <option value="time">Time</option> <option value="url">URL</option> </select> </div> </div> <div class="col-sm-2"> <div class="form-group"> <label>Others Field</label> <div class="custom-control custom-switch"> <input type="checkbox" class="custom-control-input form-control" name="custom' + row + '" id="otherSwitch' + row + '" value="true" disabled> <label class="custom-control-label" for="otherSwitch' + row + '">Required</label> </div> </div> </div> <div class="col-sm-2"> <div class="form-group"> <label>Validation</label> <div class="custom-control custom-switch"> <input type="checkbox" class="custom-control-input form-control" name="validation' + row + '" id="customSwitch' + row + '" value="required"> <label class="custom-control-label" for="customSwitch' + row + '">Required</label> </div> </div> </div> </div>';
        var fieldRow = document.getElementById('field' + row);
        if(this.value == "dropdown" || this.value == "radio" || this.value == "radioImage")
        {
            swal({
            title: 'Enter number of options:',
            input: 'number',
            showCancelButton: true,
            inputValidator: (value) => {
                if (value == '') {
                    return 'You need to write something!'
                }
            }
            }).then((result) => {
                if (result.value) {

                    for($i=1; $i<=result.value; $i++)
                    {
                        if(this.value == "dropdown" || this.value == "radio")
                            var newElement = '<div class="row"> <div class="col-sm-4 offset-sm-4"> <div class="row"> <div class="col-sm-12"> <div class="form-group"> <input type="text" placeholder="Option ' + $i +'" class="form-control" name="option' + row +'index' + $i +'" required> </div> </div> </div> </div> </div></div>';
                        else if(this.value == "radioImage")
                            var newElement = '<div class="row"> <div class="col-sm-4 offset-sm-4"> <div class="row"> <div class="col-sm-6"> <div class="form-group"> <input type="text" placeholder="Option ' + $i +'" class="form-control" name="option' + row +'index' + $i +'" required> </div> </div> <div class="col-sm-6"> <div class="form-group"> <input type="file" onchange="ValidateSize(this)" class="form-control-file" accept="image/*" name="optionimage' + row +'index' + $i +'" required> </div> </div> </div> </div></div>';

                        fieldRow.insertAdjacentHTML( 'beforeend', newElement );

                    }
                    var optioncount = document.getElementById('optioncount' + row);
                    optioncount.value = result.value;
                }
            })
        }

        if(this.value == "dropdown")
            $('input[name=custom' + row +']').prop("disabled", true);
        else if(this.value == "radio")
            $('input[name=custom' + row +']').prop("disabled", false);
        else if(this.value == "radioImage")
            $('input[name=custom' + row +']').prop("disabled", false);
        else
            $('input[name=custom' + row +']').prop("disabled", true);

    });

    var counter = 1;
    jQuery('a.add-field').click(function (event) {
        event.preventDefault();
        counter++;
        var element = document.getElementById('form-row');
        var newElement = '<div id="field' + counter + '"> <div class="row"> <div class="col-sm-4"> <div class="form-group"> <label>Field Name</label> <input type="text" class="form-control" name="name' + counter + '" required> </div> </div> <div class="col-sm-4" id="type' + counter + '"> <div class="form-group"> <label>Field Type</label> <select class="select-type form-control" name="type' + counter + '"> <option value="checkbox">Checkbox</option> <option value="date">Date</option> <option value="dropdown">Dropdown</option> <option value="email">Email</option> <option value="number">Number</option> <option value="radio">Radio Button</option> <option value="radioImage">Radio Button + Image</option> <option value="text" selected>Text</option> <option value="time">Time</option> <option value="url">URL</option> </select> </div> </div> <div class="col-sm-2"> <div class="form-group"> <label>Others Field</label> <div class="custom-control custom-switch"> <input type="checkbox" class="custom-control-input form-control" name="custom' + counter + '" id="otherSwitch' + counter + '" value="true" disabled> <label class="custom-control-label" for="otherSwitch' + counter + '">Required</label> </div> </div> </div> <div class="col-sm-2"> <div class="form-group"> <label>Validation</label> <div class="custom-control custom-switch"> <input type="checkbox" class="custom-control-input form-control" name="validation' + counter + '" id="customSwitch' + counter + '" value="required"> <label class="custom-control-label" for="customSwitch' + counter + '">Required</label> </div> </div> </div> <input type="hidden" name="optioncount' + counter + '" id="optioncount' + counter + '" value="0"> </div> </div>';
        element.insertAdjacentHTML( 'beforeend', newElement );

        var fieldcount = document.getElementById('fieldcount');
        fieldcount.value = counter;
    });

    jQuery('a.reset-field').click(function (event) {
        event.preventDefault();
        var element = document.getElementById('form-row');
        document.getElementById("form-row").innerHTML = ' <div id="field1"> <div class="row"> <div class="col-sm-4"> <div class="form-group"> <label>Field Name</label> <input type="text" class="form-control" name="name1" required> </div> </div> <div class="col-sm-4" id="type1"> <div class="form-group"> <label>Field Type</label> <select class="select-type form-control" name="type1"> <option value="checkbox">Checkbox</option> <option value="date">Date</option> <option value="dropdown">Dropdown</option> <option value="email">Email</option> <option value="number">Number</option> <option value="radio">Radio Button</option> <option value="radioImage">Radio Button + Image</option> <option value="text" selected>Text</option> <option value="time">Time</option> <option value="url">URL</option> </select> </div> </div> <div class="col-sm-2"> <div class="form-group"> <label>Others Field</label> <div class="custom-control custom-switch"> <input type="checkbox" class="custom-control-input form-control" name="custom1" id="otherSwitch1" value="true" disabled> <label class="custom-control-label" for="otherSwitch1">Required</label> </div> </div> </div> <div class="col-sm-2"> <div class="form-group"> <label>Validation</label> <div class="custom-control custom-switch"> <input type="checkbox" class="custom-control-input form-control" name="validation1" id="customSwitch1" value="required"> <label class="custom-control-label" for="customSwitch1">Required</label> </div> </div> </div> <input type="hidden" name="optioncount1" id="optioncount1" value="0"> </div> </div>';
        counter = 1;
    });

});

function ValidateSize(file) {
    var FileSize = file.files[0].size / 1024 / 1024; // in MB
    if (FileSize > 1) {
        swal("Warning", "File size exceeds 1 MB!", "warning");
        $(file).val(''); //for clearing with Jquery
    }
}
</script>
<!--Custom JavaScript -->
<script src="{{ asset('assets/libs/sweetalert2/dist/sweetalert2.all.min.js') }}"></script>
@endsection
