@extends('layouts.navbar')
@section('breadcrumb', 'Form List')
@section('content')
<div class="col-sm-12">
    <div class="card">
            <div class="d-flex no-block align-items-center col-lg-12 py-3" style="border-bottom: 1px solid #eaeaea;">
                    <h4 class="card-title mb-0 ml-4">FORM LIST</h4>
                    <div class="ml-auto mr-4">
                        @can('Manage Department')
                        <div class="btn-group">
                           <a href="{{route('form.create')}}" class="btn btn-info btn-rounded"><i class="mdi mdi-file-document mr-2"></i>Add New Form</a>
                        </div>
                        @endcan
                    </div>
                </div>
        <div class="card-body">
            <div class="table-responsive col-lg-12">
                <table id="departmentTable" class="table no-wrap user-table mb-3">
                    <thead>
                        <tr>
                            <th>FORM NAME</th>
                            <th>DESCRIPTION</th>
                            <th>CREATED AT</th>
                            <th>ACTION</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($form as $f)
                        <tr>
                            <td>{{$f->name}}</td>
                            <td>{{$f->description}}</td>
                            <td>{{$f->created_at}}</td>
                            <td>
                                <form action="{{route('form.delete', $f->id)}}" method="POST">
                                        {{ csrf_field() }}
                                        {{ method_field('DELETE') }}
                                        {{-- <a href="{{route('form.show', $f->id)}}" class="btn btn-outline-info btn-circle btn-sm btn-circle ml-2"><i class="ti-eye"></i></a> --}}
                                        {{-- <a href="{{route('form.edit', $f->id)}}" class="btn btn-outline-info btn-circle btn-sm btn-circle ml-2"><i class="ti-pencil-alt"></i></a> --}}
                                        <a href="{{route('form.sequence', $f->id)}}" class="btn btn-outline-info btn-circle btn-sm btn-circle ml-2"><i class="ti-exchange-vertical"></i></a>
                                        {{-- <button type="submit" class="btn btn-outline-danger btn-circle btn-sm btn-circle ml-2"><i class="ti-trash"></i></button> --}}
                                </form>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
@endsection

@section('script')
<script>
    $(document).ready(function () {
        $('#departmentTable').DataTable();
    });
</script>
@endsection
