@extends('layouts.navbar')
@section('breadcrumb', 'Manage Sequence')
@section('content')
<div class="col-sm-12">
    <div class="card">
        <form class="form-horizontal" method="POST" action="{{route('form.storeSequence', $form->id)}}">
            {{ csrf_field() }}
            {{ method_field('PUT') }}
            <div class="d-flex no-block align-items-center col-lg-12 py-3" style="background: #2cabe3;">
                <h4 class="card-title mb-0 ml-3 text-white">{{$form->name}} Form Sequence</h4>
            </div>
            <div class="card-body">

                <div id="form-row">
                    @if($form->department_sequence == null)
                    <div class="row">
                        <div class="form-group col-sm-4">
                            <label>Department 1</label>
                            <select class="form-control" name="department1" required>
                                <option></option>
                                @foreach($department as $d)
                                <option value="{{$d->id}}">{{$d->name}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group col-sm-8">
                            <label>Estimated Time</label>
                            <div class="form-row">
                                <input type="number" class="form-control col-sm-2" name="day1" min=0>
                                <div class="input-group-append">
                                    <span class="input-group-text">Day</span>
                                </div>
                                <input type="number" class="form-control col-sm-2 offset-sm-1" name="hour1" min=0 max=23>
                                    <div class="input-group-append">
                                        <span class="input-group-text">Hour</span>
                                    </div>
                                <input type="number" class="form-control col-sm-2 offset-sm-1" name="minute1" min=0 max=59>
                                    <div class="input-group-append">
                                        <span class="input-group-text">Minute</span>
                                    </div>
                            </div>
                        </div>
                    </div>

                    <input type="hidden" name="fieldcount" value="1" id="fieldcount">

                    @else

                    <?php $i=0 ?>
                    @foreach($form->department_sequence as $f)
                    <div class="row">
                        <div class="form-group col-sm-4">
                            <?php $i++ ?>
                            <label>Department {{$i}}</label>
                            <select class="form-control" name="department{{$i}}">
                                @foreach($department as $d)
                                <option value="{{$d->id}}" {{$f['department'] == $d->id  ? 'selected' : ''}}>{{$d->name}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group col-sm-8">
                            <label>Estimated Time</label>
                            <div class="form-row">
                                <input type="number" class="form-control col-sm-2" name="day{{$i}}" min=0
                                    placeholder="Day" value="{{$f['day']}}">
                                    <div class="input-group-append">
                                        <span class="input-group-text">Day</span>
                                    </div>
                                <input type="number" class="form-control col-sm-2 offset-sm-1" name="hour{{$i}}" min=0
                                    max=23 placeholder="Hour" value="{{$f['hour']}}">
                                    <div class="input-group-append">
                                        <span class="input-group-text">Hour</span>
                                    </div>
                                <input type="number" class="form-control col-sm-2 offset-sm-1" name="minute{{$i}}" min=0
                                    max=59 placeholder="Minute" value="{{$f['minute']}}">
                                    <div class="input-group-append">
                                        <span class="input-group-text">Minute</span>
                                    </div>
                            </div>
                        </div>
                    </div>
                    @endforeach

                    <input type="hidden" name="fieldcount" value="{{$i}}" id="fieldcount">

                    @endif
                </div>
                <a href="#" class="add-field">Add Department</a>
                <div class="d-flex no-block align-items-center col-lg-12 mt-4 pt-4 pl-0" style="border-top: 1px solid #eaeaea;">
                    <a href="#" class="reset-field btn btn-danger waves-effect waves-light">Reset</a>
                    <button type="submit" class="btn btn-success waves-effect waves-light ml-2">Submit</button>
                </div>
            </div>
        </form>
    </div>
</div>
@endsection

@section('script')
<script>
$(document).ready(function () {
    $(document).on('click', 'a.add-field', function (event) {
        event.preventDefault();
        var fieldcount = document.getElementById('fieldcount');
        var counter = fieldcount.value;
        counter++;
        var element = document.getElementById('form-row');
        var newElement = ' <div class="row"> <div class="form-group col-sm-4"> <label>Department ' + counter + '</label> <select class="form-control" name="department' + counter + '" required> <option></option> @foreach($department as $d) <option value="{{$d->id}}">{{$d->name}}</option> @endforeach </select> </div> <div class="form-group col-sm-8"> <label>Estimated Time</label> <div class="form-row"> <input type="number" class="form-control col-sm-2" name="day' + counter + '" min=0> <div class="input-group-append"> <span class="input-group-text">Day</span> </div> <input type="number" class="form-control col-sm-2 offset-sm-1" name="hour' + counter + '" min=0 max=23> <div class="input-group-append"> <span class="input-group-text">Hour</span> </div> <input type="number" class="form-control col-sm-2 offset-sm-1" name="minute' + counter + '" min=0 max=59> <div class="input-group-append"> <span class="input-group-text">Minute</span> </div> </div> </div> </div> <input type="hidden" name="fieldcount" value="' + counter + '" id="fieldcount">';
        element.insertAdjacentHTML( 'beforeend', newElement );
        fieldcount.value = counter;

    });

    jQuery('a.reset-field').click(function (event) {
        event.preventDefault();
        var fieldcount = document.getElementById('fieldcount');
        var counter = fieldcount.value;
        var element = document.getElementById('form-row');
        document.getElementById("form-row").innerHTML = ' <div class="row"> <div class="form-group col-sm-4"> <label>Department 1</label> <select class="form-control" name="department1" required> <option></option> @foreach($department as $d) <option value="{{$d->id}}">{{$d->name}}</option> @endforeach </select> </div> <div class="form-group col-sm-8"> <label>Estimated Time</label> <div class="form-row"> <input type="number" class="form-control col-sm-2" name="day1" min=0> <div class="input-group-append"> <span class="input-group-text">Day</span> </div> <input type="number" class="form-control col-sm-2 offset-sm-1" name="hour1" min=0 max=23> <div class="input-group-append"> <span class="input-group-text">Hour</span> </div> <input type="number" class="form-control col-sm-2 offset-sm-1" name="minute1" min=0 max=59> <div class="input-group-append"> <span class="input-group-text">Minute</span> </div> </div> </div> </div> <input type="hidden" name="fieldcount" value="1" id="fieldcount">';
    });
});
</script>

@endsection
