@extends('layouts.navbar')
@section('breadcrumb', 'Edit / '.$form->name)
@section('content')
<div class="col-sm-12">
    <div class="card">
        <form class="form-horizontal" enctype="multipart/form-data" method="POST" action="{{route('form.update', $form->id)}}">
            {{ csrf_field() }}
            {{ method_field('PUT') }}
            <div class="d-flex no-block align-items-center col-lg-12 py-3" style="background: #2cabe3;">
                <h4 class="card-title mb-0 ml-3 text-white">Edit {{$form->name}}</h4>
            </div>
            <div class="card-body">
                <div class="form-group">
                    <label>Form Name</label>
                    <input type="text" class="form-control" name="name" value="{{$form->name}}" required>
                </div>
                <div class="form-group">
                    <label>Form Description</label>
                    <input type="text" class="form-control" name="description" value="{{$form->description}}">
                </div>
                <div class="form-group">
                    <label>Form Thumbnail</label>
                    <input type="file" name="file" class="form-control-file" onchange="ValidateSize(this)">
                </div>
                <hr>
                <?php $i=1 ?>
                <div class="form-field">
                @foreach($form->field as $f)
                <div id="form-row{{$i}}">
                    <div class="row">
                        <div class="col-sm-2">
                            <div class="form-group">
                                <label>Field Name</label>
                                <input type="text" class="form-control" name="name{{$i}}" value="{{$f['name']}}"
                                    required>
                            </div>
                        </div>
                        <div class="col-sm-2" id="type1">
                            <div class="form-group">
                                <label>Field Type</label>
                                <select class="select-type form-control" name="type{{$i}}">
                                    <option value="checkbox" {{$f['type'] == "checkbox"  ? 'selected' : ''}}>Checkbox</option>
                                    <option value="date" {{$f['type'] == "date"  ? 'selected' : ''}}>Date</option>
                                    <option value="dropdown" {{$f['type'] == "dropdown"  ? 'selected' : ''}}>Dropdown</option>
                                    <option value="email" {{$f['type'] == "email"  ? 'selected' : ''}}>Email</option>
                                    <option value="file" {{$f['type'] == "file"  ? 'selected' : ''}}>File</option>
                                    <option value="number" {{$f['type'] == "number"  ? 'selected' : ''}}>Number</option>
                                    <option value="radio" {{$f['type'] == "radio"  ? 'selected' : ''}}>Radio Button</option>
                                    <option value="text" {{$f['type'] == "text"  ? 'selected' : ''}}>Text</option>
                                    <option value="time" {{$f['type'] == "time"  ? 'selected' : ''}}>Time</option>
                                    <option value="url" {{$f['type'] == "url"  ? 'selected' : ''}}>URL</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-sm-5">
                                <div class="row">
                                    <div class="col-sm-8">
                                        <div class="form-group">
                                                <label>Field Options</label>
                                                <input type="text" class="form-control" name="option{{$i}}" placeholder="Separate by (;)" @if(array_key_exists('option', $f))
                                                    value="@foreach($f['option'] as $o)@if(next($f['option'])){{$o}};@else{{$o}}@endif @endforeach"@endif {{$f['type'] == "radio" || $f['type'] == "dropdown"  ? '' : 'disabled'}}>
                                        </div>
                                    </div>
                                    <div class="col-sm-4">
                                        <div class="form-group">
                                                <label>Others Field</label>
                                                <div class="custom-control custom-switch">
                                                    <input type="checkbox" class="custom-control-input form-control"
                                                        name="custom{{$i}}" id="otherSwitch{{$i}}" value="true" {{$f['type'] == "radio"  ? '' : 'disabled'}}>
                                                    <label class="custom-control-label" for="otherSwitch{{$i}}">Required</label>
                                                </div>
                                            </div>
                                    </div>
                                </div>

                        </div>
                        <div class="col-sm-2">
                            <div class="form-group">
                                <label>Field Validation</label>
                                <div class="custom-control custom-switch">
                                    <input type="checkbox" class="custom-control-input form-control"
                                        name="validation{{$i}}" id="customSwitch{{$i}}" value="required"
                                        {{$f['validation'] == "required"  ? 'checked' : ''}}>
                                    <label class="custom-control-label" for="customSwitch{{$i}}">Required</label>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-1">
                            <div class="form-group">
                                <a href="#" class="remove-field" id="{{$i}}">Remove Field</a>
                            </div>
                        </div>
                    </div>
                </div>
                <?php $i++ ?>
                @endforeach
                </div>
                <a href="#" class="add-field">Add Field</a>
                <input type="hidden" name="fieldcount" value="{{$i-1}}" id="fieldcount">
                <div class="d-flex no-block align-items-center col-lg-12 mt-4 pt-4 pl-0" style="border-top: 1px solid #eaeaea;">
                    <a href="{{URL::current()}}" class="reset-field btn btn-danger waves-effect waves-light">Reset</a>
                    <button type="submit" class="btn btn-info waves-effect waves-light ml-2">Update</button>
                </div>
            </div>
        </form>
    </div>
</div>
@endsection

@section('script')
<script>
    function ValidateSize(file) {
        var FileSize = file.files[0].size / 1024 / 1024; // in MB
        if (FileSize > 1) {
            swal("Warning", "File size exceeds 1 MB!", "warning");
            $(file).val(''); //for clearing with Jquery
        }
    }
    $(document).ready(function () {

        var fieldcount = document.getElementById('fieldcount');
        jQuery('a.add-field').click(function (event) {
            event.preventDefault();
            var counter = parseInt(fieldcount.value) + 1;
            var element = document.getElementById('form-row' + fieldcount.value);
            var newElement = ' <div id="form-row' + counter + '"> <div class="row"> <div class="col-sm-2"> <div class="form-group"> <label>Field Name</label> <input type="text" class="form-control" name="name' + counter + '" required> </div> </div> <div class="col-sm-2" id="type' + counter + '"> <div class="form-group"> <label>Field Type</label> <select class="select-type form-control" name="type' + counter + '"> <option value="checkbox">Checkbox</option> <option value="date">Date</option> <option value="dropdown">Dropdown</option> <option value="email">Email</option> <option value="file">File</option> <option value="number">Number</option> <option value="radio">Radio Button</option> <option value="text" selected>Text</option> <option value="time">Time</option> <option value="url">URL</option> <option value="selectStatus">Select Status</option> </select> </div> </div> <div class="col-sm-5"> <div class="row"> <div class="col-sm-8"> <div class="form-group"> <label>Field Options</label> <input type="text" class="form-control" name="option' + counter + '" placeholder="Separate by (;)" disabled> </div> </div> <div class="col-sm-4"> <div class="form-group"> <label>Others Field</label> <div class="custom-control custom-switch"> <input type="checkbox" class="custom-control-input form-control" name="custom' + counter + '" id="otherSwitch' + counter + '" value="true" disabled> <label class="custom-control-label" for="otherSwitch' + counter + '">Required</label> </div> </div> </div> </div> </div> <div class="col-sm-2"> <div class="form-group"> <label>Field Validation</label> <div class="custom-control custom-switch"> <input type="checkbox" class="custom-control-input form-control" name="validation' + counter + '" id="customSwitch' + counter + '" value="required"> <label class="custom-control-label" for="customSwitch' + counter + '">Required</label> </div> </div> </div> </div> </div>';
            element.insertAdjacentHTML('beforeend', newElement );

            fieldcount.value = counter;
        });

        $("a.remove-field").click(function(e){
            var idClicked = e.target.id;
            document.getElementById('form-row'+idClicked).remove();

            for (i = parseInt(idClicked); i <  fieldcount.value; i++) {
                var j = i + 1;
                document.getElementById('form-row' + j).id = 'form-row' + i;
                $('input[name="name' + j + '"]').attr('name', 'name' + i);
                $('select[name="type' + j + '"]').attr('name', 'type' + i);
                $('input[name="option' + j + '"]').attr('name', 'option' + i);
                $('input[name="validation' + j + '"]').attr('name', 'validation' + i);
                document.getElementById(j).id = i;
            };

            fieldcount.value -= 1;
        });

        $(document).on("change", "select.select-type" , function() {

        var selectName = this.name;
        var row = selectName[selectName.length - 1];
        $('input[name=option' + row +']').prop("required", false);
        $('input[name=option' + row +']').prop("disabled", true);
        if(this.value == "dropdown")
        {
            $('input[name=option' + row +']').prop("required", true);
            $('input[name=option' + row +']').prop("disabled", false);
            $('input[name=custom' + row +']').prop("disabled", true);

        }
        else if(this.value == "radio")
        {
            $('input[name=option' + row +']').prop("required", true);
            $('input[name=option' + row +']').prop("disabled", false);
            $('input[name=custom' + row +']').prop("disabled", false);
        }
        else
        {
            $('input[name=option' + row +']').prop("required", false);
            $('input[name=option' + row +']').prop("disabled", true);
            $('input[name=custom' + row +']').prop("disabled", true);
        }

});

    });
</script>
<!--Custom JavaScript -->
<script src="{{ asset('assets/libs/sweetalert2/dist/sweetalert2.all.min.js') }}"></script>
@endsection
