@extends('layouts.navbar')
@section('breadcrumb', 'Form List / '.$form->name)
@section('content')
<div class="col-sm-12">
    <div class="card">
        <form>
            <div class="card-body">
                <h4 class="card-title">{{$form->name}} Form Preview</h4>
                @foreach($form->field as $f)
                <div class="form-group">
                    @if($f['type'] == 'dropdown')
                    <label>{{$f['name']}}</label>
                    <select class="form-control" name="{{$f['name']}}">
                        @if(array_key_exists('option', $f))
                            @foreach($f['option'] as $o)
                                <option>{{$o}}</option>
                            @endforeach
                        @endif
                    </select>
                    @elseif($f['type'] == 'selectBranch')
                    <label>{{$f['name']}}</label>
                    <select class="form-control" name="{{$f['name']}}">
                            @foreach($branch as $b)
                                <option>{{$b->name}}</option>
                            @endforeach
                    </select>
                    @elseif($f['type'] == 'selectStatus')
                    <label>{{$f['name']}}</label>
                    <select class="form-control" name="{{$f['name']}}">
                            @foreach($status as $s)
                                <option>{{$s->name}}</option>
                            @endforeach
                    </select>
                    @elseif($f['type'] == 'checkbox')
                    <div class="custom-control custom-checkbox mr-sm-2">
                        <input type="checkbox" class="custom-control-input" id="checkbox1" name="{{$f['name']}}">
                        <label class="custom-control-label" for="checkbox1">{{$f['name']}}</label>
                    </div>
                    @elseif($f['type'] == 'radio')
                    @if(array_key_exists('option', $f))
                        @foreach($f['option'] as $o)
                            <div class="custom-control custom-radio">
                                <input type="radio" id="customRadio{{$f['name']}}{{$o}}" name="{{$f['name']}}" class="custom-control-input" {{$f['validation']}}>
                                <label class="custom-control-label" for="customRadio{{$f['name']}}{{$o}}">{{$o}}</label>
                            </div>
                        @endforeach
                    @endif
                    @else
                    <label>{{$f['name']}}</label>
                    <input type="{{$f['type']}}" name="{{$f['name']}}" class="form-control" {{$f['validation']}}>
                    @endif
                </div>
                @endforeach
                <button type="submit" class="btn btn-info waves-effect waves-light pull-right">Submit</button>
            </div>
        </form>
    </div>
</div>
@endsection
