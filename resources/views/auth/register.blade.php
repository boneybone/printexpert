@extends('layouts.master')

@section('main')
<div class="auth-wrapper d-flex no-block justify-content-flex-start align-items-center" style="background:url({{('assets/images/background/bg.png')}}) no-repeat center center;">
    <div class="mx-5 px-5">
        <span class="text-white" style="font-size:3.5rem; font-weight:500; line-height: 1.2; ">We Print Your<br>Ideas.</span>
        <p class="mt-3 text-white">Everything you create matters. Everything to market your business.<br>Best printing. Best price.</p>
    </div>
    <div class="auth-box on-sidebar">
        <div>
            <div class="logo">
                <span class="db"><img src="{{('assets/images/logos/printexpert-logo.png')}}" alt="logo" style="width:150px;height:50px" /></span>
                <h5 class="font-medium mb-3">Sign Up New Account</h5>
            </div>
            <!-- Form -->
            <div class="row mt-5">
                <div class="col-12">
                    <form class="form-horizontal mt-3" method="POST" action="{{ route('register') }}">
                        @csrf

                        <div class="form-group row ">
                            <div class="col-12 ">
                                <input id="name" type="text" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }} form-control-lg"
                                    name="name" value="{{ old('name') }}" placeholder="Name" required autofocus>

                                @if ($errors->has('name'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('name') }}</strong>
                                </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-12 ">
                                <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }} form-control-lg"
                                    name="email" value="{{ old('email') }}" placeholder="Email" required>

                                @if ($errors->has('email'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('email') }}</strong>
                                </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group row ">
                                <div class="col-12 ">
                                    <input type="text" class="form-control form-control-lg"
                                        name="nric" placeholder="IC Number" required>
                                </div>
                        </div>
                        <div class="form-group row ">
                                <div class="col-12 ">
                                    <input type="text" class="form-control form-control-lg"
                                        name="contact" placeholder="Mobile Number" required>
                                </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-12 ">
                                <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }} form-control-lg"
                                    name="password" placeholder="Password" required>

                                @if ($errors->has('password'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('password') }}</strong>
                                </span>
                                @endif </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-12 ">
                                <input id="password-confirm" type="password" class="form-control form-control-lg" name="password_confirmation" placeholder="Confirm Password"
                                    required>
                            </div>
                        </div>

                        <div class="form-group text-center mt-5">
                            <div class="col-xs-12 pb-3 ">
                                <button class="btn btn-block btn-lg btn-info btn-rounded" type="submit ">SIGN UP</button>
                            </div>
                        </div>
                        <div class="form-group mb-0 mt-2 ">
                            <div class="col-sm-12 text-center ">
                                Already have an account? <a href="{{route('login')}}" class="text-info ml-1 "><b>Sign
                                        In</b></a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
