@extends('layouts.master')

@section('main')
<div class="auth-wrapper d-flex no-block justify-content-flex-start align-items-center" style="background:url({{asset('assets/images/background/bg.png')}}) no-repeat center center;">
    <div class="mx-5 px-5">
        <span class="text-white" style="font-size:3.5rem; font-weight:500; line-height: 1.2; ">We Print Your<br>Ideas.</span>
        <p class="mt-3 text-white">Everything you create matters. Everything to market your business.<br>Best printing. Best price.</p>
    </div>
    <div class="auth-box on-sidebar">
        <div id="loginform">
            <div class="logo">
                <span class="db"><img src="{{asset('assets/images/logos/printexpert-logo.png')}}" alt="logo" style="width:150px;height:50px"/></span>
                <h5 class="font-medium mb-3">Log In to Account</h5>
            </div>
            <!-- Form -->
            <div class="row mt-5">
                <div class="col-12">
                    <form class="form-horizontal mt-3" id="loginform" method="POST" action="{{ route('login') }}">
                        @csrf
                        <div class="input-group mb-3">
                            <div class="input-group-prepend">
                                <span class="input-group-text" id="basic-addon1"><i class="ti-user"></i></span>
                            </div>
                            <input id="email" type="email" class="form-control form-control-lg{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" placeholder="Email" aria-label="Email" aria-describedby="basic-addon1" required autofocus>
                            @if ($errors->has('email'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('email') }}</strong>
                            </span>
                            @endif
                        </div>
                        <div class="input-group mb-3">
                            <div class="input-group-prepend">
                                <span class="input-group-text" id="basic-addon2"><i class="ti-pencil"></i></span>
                            </div>
                            <input id="password" type="password" class="form-control form-control-lg{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" placeholder="Password" required>
                            @if ($errors->has('password'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('password') }}</strong>
                            </span>
                            @endif
                        </div>
                        <div class="form-group row">
                            <div class="col-md-12">
                                <div class="custom-control custom-checkbox">
                                    <input type="checkbox" class="custom-control-input" id="customCheck1">
                                    <label class="custom-control-label" for="customCheck1">Remember me</label>
                                    {{-- <a href="javascript:void(0)" id="to-recover" class="text-dark float-right"><i class="fa fa-lock mr-1"></i> Forgot pwd?</a> --}}
                                </div>
                            </div>
                        </div>
                        <div class="form-group text-center mt-5">
                            <div class="col-xs-12 pb-3">
                                <button class="btn btn-block btn-lg btn-info btn-rounded" type="submit">Log In</button>
                            </div>
                        </div>
                        {{-- <div class="form-group mb-0 mt-2">
                            <div class="col-sm-12 text-center">
                                Are you new to Print Expert? <a href="{{route('register')}}" class="text-info ml-1"><b>Sign Up</b></a>
                            </div>
                        </div> --}}
                    </form>
                </div>
            </div>
        </div>

        <div id="recoverform">
            <div class="logo">
                <span class="db"><img src="{{asset('assets/images/logos/printexpert-logo.png')}}" alt="logo" style="width:150px;height:50px"/></span>
                <h5 class="font-medium mb-5">Recover Password</h5>
                <span>Enter your Email and instructions will be sent to you!</span>
            </div>
            <div class="row mt-3">
                <!-- Form -->
                <form class="col-12" method="POST" action="{{ route('password.email') }}">
                        @csrf
                    <!-- email -->
                    <div class="form-group row">
                        <div class="col-12">
                                <input id="emailreset" type="email" class="form-control form-control-lg{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required>

                                @if ($errors->has('email'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                        </div>
                    </div>
                    <!-- pwd -->
                    <div class="row mt-5">
                        <div class="col-12">
                            <button class="btn btn-block btn-lg btn-danger btn-rounded" type="submit" name="action">Reset</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>

    </div>
</div>
@endsection

@section('script')
<script>
    $('[data-toggle="tooltip"]').tooltip();
    $(".preloader").fadeOut();
    // ==============================================================
    // Login and Recover Password
    // ==============================================================
    $('#to-recover').on("click", function() {
        $("#loginform").slideUp();
        $("#recoverform").fadeIn();
    });
</script>
@endsection

