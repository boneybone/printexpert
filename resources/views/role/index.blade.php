@extends('layouts.navbar')
@section('breadcrumb', 'Role List')
@section('content')
<div class="col-sm-12">
    <div class="card">
    <div class="d-flex no-block align-items-center col-lg-12 py-3" style="border-bottom: 1px solid #eaeaea;">
            <h4 class="card-title mb-0 ml-4">ROLE LIST</h4>
            <div class="ml-auto mr-4">
                <div class="btn-group">
                    <button class="btn btn-info btn-rounded" data-toggle="modal" data-target="#createrole">
                        <i class="mdi mdi-playlist-plus mr-2"></i>Add New Role
                    </button>
                </div>
            </div>
        </div>
        <div class="card-body">
            <div class="table-responsive col-lg-12">
                <table id="roleTable" class="table no-wrap user-table mb-3">
                    <thead>
                        <tr>
                            <th>ROLE NAME</th>
                            <th>TOTAL USER</th>
                            <th>ACTION</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($role as $r)
                        <tr>
                            <td>{{$r->name}}</td>
                            <td>{{$r->users_count}}</td>
                            <td>
                                <button class="btn btn-outline-info btn-circle btn-sm btn-circle ml-2" data-toggle="modal" data-target="#editrole{{$r->id}}"><i class="ti-pencil-alt"></i></button>
                            </td>
                        </tr>

                        <!-- Manage Role Modal -->
                        <div class="modal fade" id="editrole{{$r->id}}" tabindex="-1" role="dialog"
                            aria-labelledby="exampleModalLabel" aria-hidden="true">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <form class="form-horizontal" method="POST"
                                        action="{{ route('role.update', $r->id) }}">
                                        {{ csrf_field() }}
                                        {{ method_field('PUT') }}
                                        <div class="modal-header">
                                            <h4 class="modal-title" id="myModalLabel"><i class="mdi mdi-playlist-plus ml-2 mr-2"></i>Manage Role</h4>
                                            <button type="button" class="close" data-dismiss="modal"
                                                aria-hidden="true">×</button>
                                        </div>
                                        <div class="modal-body">
                                            <div class="form-group">
                                                <label class="col-md-12">Name of Role</label>
                                                <div class="col-md-12">
                                                    <input type="text" class="form-control" value="{{$r->name}}"
                                                        name="name" required />
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-md-12">Select Permissions</label>
                                                <div class="col-md-12">
                                                    @foreach($permission as $p)
                                                    <fieldset class="checkbox">
                                                        <label>
                                                            <input name="permission[]" type="checkbox"
                                                                value="{{$p->id}}"
                                                                @if($r->permissions->contains($p->id)) checked=checked
                                                            @endif>
                                                            {{$p->name}}
                                                        </label>
                                                    </fieldset>
                                                    @endforeach
                                                </div>
                                            </div>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-secondary"
                                                data-dismiss="modal">Cancel</button>
                                            <button type="submit" class="btn btn-success"><i class="ti-save mr-2"></i>
                                                Save</button>
                                    </form>
                                    @if($r->name != "Super Admin")
                                    <form action="{{ route('role.delete', $r->id ) }}" method="POST">
                                        {{ csrf_field() }}
                                        {{ method_field('DELETE') }}
                                        <button type="submit" class="btn btn-danger"><i class="ti-trash mr-2"></i>
                                            Delete</button>
                                    </form>
                                    @endif
                                </div>
                            </div>
                        </div>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

<!-- Create Role Modal -->
<div class="modal fade" id="createrole" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
    aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <form class="form-horizontal" method="POST" action="{{ route('role.store') }}">
                @csrf
                <div class="modal-header">
                    <h4 class="modal-title" id="myModalLabel"><i class="mdi mdi-playlist-plus ml-2 mr-2"></i>Add New Role</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label class="col-md-12">Name of Role</label>
                        <div class="col-md-12">
                            <input type="text" class="form-control" placeholder="Role Name" name="name" required />
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-12">Select Permissions</label>
                        <div class="col-md-12">
                            @foreach($permission as $p)
                            <fieldset class="checkbox">
                                <label>
                                    <input name="permission[]" type="checkbox" value="{{$p->id}}">
                                    {{$p->name}}
                                </label>
                            </fieldset>
                            @endforeach
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                    <button type="submit" class="btn btn-success"><i class="ti-save mr-2"></i> Save</button>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection

@section('script')
<script>
    $(document).ready(function () {
        $('#roleTable').DataTable();
    });
</script>
@endsection
