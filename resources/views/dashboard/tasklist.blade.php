@extends('layouts.navbar')
@section('breadcrumb', 'Task List')
@section('content')
<div class="card">
    <div class="card-body">
        <div class="card-group">

            @foreach($staff as $s)
            <div class="card col-sm-3">
                <div class="card-body text-center mb-0 pb-0">
                    <h4 class="text-center text-info">{{$s->name}}</h4>
                    <div class="row py-2">
                        <div class="col text-center align-self-center">
                            <img src="{!! $s->photo() !!}" class="rounded-circle" width="60">
                        </div>
                    </div>
                    <hr class="mb-0">
                </div>
                <div class="comment-widgets scrollable ps-container ps-theme-default ps-active-y" style="height:300px;">
                    @foreach($order->where('staff_id', $s->id) as $o)
                    <div class="flex-row comment-row mt-0 mb-0">
                        <div class="comment-text w-100" style="text-align: center;">
                            <span class="badge badge-pill badge-warning">{{$o->status}}</span>
                            <span class="badge badge-pill badge-danger ml-2">{{$o->urgency}}</span>
                            <div class="mb-2 d-block font-14 text-muted font-light mt-3">
                                <span class="font-light">{{$o->form['name']}}</span><span class="font-medium">#{{$o->id}}</span>
                            </div>
                            <div><span class="text-muted" data-toggle="tooltip" data-placement="bottom"
                                    data-original-title="{{$o->created_at}}">{{$o->created_at->diffForHumans()}}</span>
                            </div>
                        </div>
                    </div>
                    @endforeach
                </div>
            </div>
            @endforeach

        </div>
    </div>
</div>
<div class="row">
    <div class="col-sm-12">
        <div class="float-right">{{ $staff->links() }}</div>
    </div>
</div>
@endsection
