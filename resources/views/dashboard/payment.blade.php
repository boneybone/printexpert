@extends('layouts.navbar')
@section('breadcrumb', 'Dashboard')
@section('content')
<div class="row">
    <div class="col-sm-9">
        <div class="card">
            <div class="card-body">
                <div class="d-flex no-block align-items-center mb-4">
                    <h4 class="card-title">PAYMENT LIST</h4>
                </div>
                <div class="table-responsive">
                    <table id="orderTable" class="table table-bordered nowrap display">
                        <thead>
                            <tr>
                                <th>TITLE</th>
                                <th>ORDER ID</th>
                                <th>CUSTOMER</th>
                                <th>JOB STATUS</th>
                                <th>PAYMENT</th>
                                <th>METHOD</th>
                                <th>ACTION</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($order as $o)
                            <tr>
                                <td>{{$o->form['name']}}</td>
                                <td id="id{{$o->id}}">{{$o->id}}</td>
                                <td>{{$o->customer['name']}}<br>
                                    <span class="text-muted">{{$o->customer['email']}}</span>
                                </td>
                                <td id="status{{$o->id}}" align="center">
                                        @if($o->status == 'Received')
                                        <span class="badge badge-pill badge-primary">Received</span>
                                        @elseif($o->status == 'Delivered')
                                        <span class="badge badge-pill badge-success">Delivered</span>
                                        @else
                                        <span class="badge badge-pill badge-warning">Production</span>
                                        @endif
                                    </td>
                                <td align="center">
                                        @if($o->paid == null)
                                            <span class="badge badge-pill badge-danger">Unpaid</span>
                                        @elseif($o->full_paid == true)
                                            <span class="badge badge-pill badge-success px-3">Paid</span>
                                        @else
                                            <span class="badge badge-pill badge-primary">Deposit</span>
                                        @endif
                                </td>
                                <td align="center">
                                    {{$o->paymentmethod}}<br/>
                                    <span class="text-muted">{{$o->paymentreference}}</span>
                                </td>
                                <td align="center"><button class="btn btn-info btn-circle" data-toggle="modal" id="view-order"
                                        data-target="#viewOrder" data-id="{{$o->id}}"><i class="fas fa-edit"></i></button></td>
                            </tr>
                                <div id="name{{$o->id}}" style="display: none;" data-value="{{$o->customer['name']}}">
                                <div id="email{{$o->id}}" style="display: none;" data-value="{{$o->customer['email']}}">
                                <div id="contact{{$o->id}}" style="display: none;" data-value="{{$o->customer['contact']}}">
                                <div id="price{{$o->id}}" style="display: none;" data-value="{{$o->price}}">
                                <div id="discount{{$o->id}}" style="display: none;" data-value="{{$o->discount}}">
                                <div id="paid{{$o->id}}" style="display: none;" data-value="{{$o->paid}}">
                             @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <div class="col-sm-3">
        {{-- <div class="card">
            <div class="card-body">
                <h5 class="card-title text-uppercase">QUICK PRINT</h5>
                <div class="d-flex align-items-center">
                    <h2 class="mb-0 display-5"><i class="fas fa-print"></i></h2>
                    <div class="ml-auto">
                        <h2 class="mb-0 display-6"><span class="font-normal"></span></h2>
                    </div>
                    <div class="ml-auto">
                        <button type="button" class="btn btn-info btn-rounded"> CREATE ORDER</button>
                    </div>
                </div>
            </div>
        </div> --}}
        <div class="card">
            <div class="card-body">
                <h5 class="card-title text-uppercase">UNPAID PAYMENT</h5>
                <div class="d-flex align-items-center">
                    <h2 class="mb-0 display-5"><i class=" far fa-money-bill-alt text-danger"></i></h2>
                    <div class="ml-auto">
                        <h2 class="mb-0 display-6"><span class="font-normal">{{$order->where('full_paid', null)->count()}}</span></h2>
                    </div>
                </div>
            </div>
        </div>
        {{-- <div class="card">
            <div class="card-body">
                <h4 class="card-title">RECENT ACTIVITY</h4>
                <div class="feed-widget scrollable ps-container ps-theme-default" style="height:450px;">
                    <ul class="list-style-none feed-body m-0 pb-3">
                    </ul>
                    <div class="ps-scrollbar-x-rail" style="left: 0px; bottom: 0px;">
                        <div class="ps-scrollbar-x" tabindex="0" style="left: 0px; width: 0px;"></div>
                    </div>
                    <div class="ps-scrollbar-y-rail" style="top: 0px; right: 3px;">
                        <div class="ps-scrollbar-y" tabindex="0" style="top: 0px; height: 0px;"></div>
                    </div>
                </div>
            </div>
        </div> --}}
    </div>
</div>

<!-- View Order Modal -->
<div class="modal fade" id="viewOrder" tabindex="-1" role="dialog" aria-labelledby="createModalLabel"
    aria-hidden="true">
    <div class="modal-dialog modal-sm" role="document">
        <div class="modal-content" id="form-content">
            <div class="modal-header">
                <h5 class="modal-title">Payment</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <h3 class="modal-title" id="m-orderid"></h3>
                <strong>CUSTOMER INFORMATION</strong>
                <div class="row">
                    <div class="col-4">Name</div>
                    <div id="m-name" class="col-8"></div>
                </div>
                <div class="row">
                    <div class="col-4">Email</div>
                    <div id="m-email" class="col-8"></div>
                </div>
                <div class="row">
                    <div class="col-4">Mobile No</div>
                    <div id="m-contact" class="col-8"></div>
                </div>
                <strong>PAYMENT DETAILS</strong>
                <div class="form-group">
                    <label>Price</label>
                    <input id="m-price" class="form-control" disabled>
                </div>
                <div class="form-group" id="divdiscount">
                    <label id="m-pricediscountlabel">Price After Discount</label>
                    <input id="m-pricediscount" class="form-control" disabled>
                </div>
                <div class="form-group">
                        <label>Paid</label>
                        <input id="m-paid" class="form-control" disabled>
                </div>
                <div class="form-group">
                    <label>Balance</label>
                    <input id="m-balance" class="form-control" disabled>
                </div>
                <form method="POST" action="{{ route('order.updatePayment') }}">
                        {{ csrf_field() }}
                        {{ method_field('PUT') }}
                <div class="form-group">
                    <label>Payment</label>
                    <input id="m-payment" type="number" min="0" step="0.01" class="form-control" name="paid">
                </div>
                <input type="hidden" name="id" id='m-formid'>
            </div>
            <div class="modal-footer">
                    <button class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                    <button type="submit" class="btn btn-danger">Pay</button>
            </div>
            </form>
        </div>
    </div>
</div>
@endsection

@section('script')
<script>
    $(document).ready(function () {
        $('#orderTable').DataTable();

        $(document).on("click", "#view-order", function () {
            var idClicked = $(this).attr("data-id");

            $('#m-orderid').html('ORDER ID: ' + idClicked);
            $('#m-name').html(': ' + $('#name' + idClicked).attr("data-value"));
            $('#m-email').html(': ' + $('#email' + idClicked).attr("data-value"));
            $('#m-contact').html(': ' + $('#contact' + idClicked).attr("data-value"));

            var price = parseFloat($('#price' + idClicked).attr("data-value"));
            var discount = parseFloat($('#discount' + idClicked).attr("data-value"));
            var paid = parseFloat($('#paid' + idClicked).attr("data-value"));
            if(isNaN(paid))
                paid = 0;
            if(isNaN(discount))
                discount = 0;

            var pricediscount = price*(100-discount)/100;
            var balance = pricediscount - paid ;
            $('#m-price').attr('value', 'RM' + price.toFixed(2));
            if(discount == 0)
                $('#divdiscount').hide();
            else
            {
                $('#m-pricediscountlabel').html('Price After Discount (' + discount + '%)');
                $('#m-pricediscount').attr('value', 'RM' + pricediscount.toFixed(2));
                $('#divdiscount').show();
            }
            $('#m-paid').attr('value', 'RM' + paid.toFixed(2));
            $('#m-balance').attr('value', 'RM' + balance.toFixed(2));
            $('#m-formid').attr('value', idClicked);
            $('#m-payment').attr('max', balance.toFixed(2));
        });

    });
</script>
@endsection
