@extends('layouts.navbar')
@section('breadcrumb', 'Dashboard')
@section('content')
<div class="row">
    <div class="col-sm-9">
        <div class="card">
            <div class="card-body">
                <div class="d-flex no-block align-items-center mb-4">
                    <h4 class="card-title">ORDER LIST</h4>
                </div>
                <div class="table-responsive">
                    <table id="orderTable" class="table table-bordered nowrap display">
                        <thead>
                            <tr>
                                <th>TITLE</th>
                                <th>ORDER ID</th>
                                <th>CUSTOMER</th>
                                <th>PAYMENT</th>
                                <th>STORAGE</th>
                                <th>STATUS</th>
                                <th>ACTION</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($order as $o)
                            <tr>
                                <td>{{$o->form['name']}}</td>
                                <td id="id{{$o->id}}">{{$o->id}}</td>
                                <td>{{$o->customer['name']}}<br>
                                    <span class="text-muted">{{$o->customer['email']}}</span>
                                </td>
                                <td align="center">
                                    @if($o->paid == null)
                                        <span class="badge badge-pill badge-danger">Unpaid</span>
                                    @elseif($o->full_paid == true)
                                        <span class="badge badge-pill badge-success px-3">Paid</span>
                                    @else
                                        <span class="badge badge-pill badge-primary">Deposit</span>
                                    @endif
                                    @if($o->unpaid_collection == true)
                                    <br><span class="text-muted">Approved for unpaid collection</span>
                                    @endif
                                </td>
                                <td align="center">
                                    @if($o->storage == null)
                                    <span class="text-muted">n/a</span>
                                    @else
                                    {{$o->storage}}
                                    <a href="" data-toggle="modal" id="storage-order" data-target="#editStorage" data-id="{{$o->id}}"><i class="fas fa-edit"></i></a>
                                    @endif
                                </td>
                                <td id="status{{$o->id}}" align="center">
                                    @if($o->status == 'Received')
                                    <span class="badge badge-pill badge-primary">Received</span>
                                    @elseif($o->status == 'Delivered')
                                    <span class="badge badge-pill badge-success">Delivered</span>
                                    @else
                                    <span class="badge badge-pill badge-warning">Finished</span>
                                    @endif
                                    </td>
                                <td align="center">
                                    @if($o->status == 'Received')
                                    @if($o->full_paid == true || $o->unpaid_collection == true )
                                    <button class="btn btn-outline-primary btn-circle" id="deliver-order" data-id="{{$o->id}}"><i class="fas fa-truck"></i></button>
                                    @else
                                    <button class="btn btn-outline-primary btn-circle" id="unpaid-order"><i class="fas fa-truck"></i></button>
                                    @endif
                                    @elseif($o->status == 'Delivered')
                                    <button class="btn btn-outline-success btn-circle" data-toggle="modal" id="collector-order" data-target="#collectorOrder" data-id="{{$o->id}}"><i class="fas fa-eye"></i></button>
                                    @else
                                    <button class="btn btn-outline-warning btn-circle" data-toggle="modal" id="receive-order" data-target="#receiveOrder" data-id="{{$o->id}}"><i class="fas fa-box-open"></i></button>
                                    @endif
                                </td>
                            </tr>
                                <div id="department{{$o->id}}" style="display: none;" data-value="{{$o->department['name']}}">
                                <div id="name{{$o->id}}" style="display: none;" data-value="{{$o->customer['name']}}">
                                <div id="email{{$o->id}}" style="display: none;" data-value="{{$o->customer['email']}}">
                                <div id="contact{{$o->id}}" style="display: none;" data-value="{{$o->customer['contact']}}">
                                <div id="collecttime{{$o->id}}" style="display: none;" data-value="{{date('g:i A', strtotime($o->collection))}}">
                                <div id="collectdate{{$o->id}}" style="display: none;" data-value="{{date('d/m/Y', strtotime($o->collection))}}">
                                <div id="collectorname{{$o->id}}" style="display: none;" data-value="{{$o->collectorname}}">
                                <div id="collectorcontact{{$o->id}}" style="display: none;" data-value="{{$o->collectorcontact}}">
                                <div id="collectordate{{$o->id}}" style="display: none;" data-value="{{date('d/m/Y', strtotime($o->collectortime))}}">
                                <div id="collectortime{{$o->id}}" style="display: none;" data-value="{{date('g:i A', strtotime($o->collectortime))}}">
                                <div id="storage{{$o->id}}" style="display: none;" data-value="{{$o->storage}}">
                             @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <div class="col-sm-3">
        <div class="card">
            <div class="card-body">
                <h5 class="card-title text-uppercase">PAYMENT COUNTER</h5>
                <div class="d-flex align-items-center">
                    <h2 class="mb-0 display-5"><i class="mdi mdi-credit-card"></i></h2>
                    <div class="ml-auto">
                        <h2 class="mb-0 display-6"><span class="font-normal"></span></h2>
                    </div>
                    <div class="ml-auto">
                        <a href="{{route('dashboard.payment')}}" class="btn btn-success btn-rounded btn-info"> GO TO PAYMENT</a>
                    </div>
                </div>
            </div>
        </div>
        <div class="card">
            <div class="card-body">
                <h5 class="card-title text-uppercase">ORDER DELIVERED</h5>
                <div class="d-flex align-items-center">
                    <h2 class="mb-0 display-5"><i class="mdi mdi-truck-delivery text-success"></i></h2>
                    <div class="ml-auto">
                        <h2 class="mb-0 display-6"><span class="font-normal">{{$order->where('status', 'Delivered')->count()}}</span></h2>
                    </div>
                </div>
            </div>
        </div>
        {{-- <div class="card">
            <div class="card-body">
                <h4 class="card-title">RECENT ACTIVITY</h4>
                <div class="feed-widget scrollable ps-container ps-theme-default" style="height:450px;">
                    <div class="ps-scrollbar-x-rail" style="left: 0px; bottom: 0px;">
                        <div class="ps-scrollbar-x" tabindex="0" style="left: 0px; width: 0px;"></div>
                    </div>
                    <div class="ps-scrollbar-y-rail" style="top: 0px; right: 3px;">
                        <div class="ps-scrollbar-y" tabindex="0" style="top: 0px; height: 0px;"></div>
                    </div>
                </div>
            </div>
        </div> --}}
    </div>
</div>

<!-- Order Receive Modal -->
<div class="modal fade" id="receiveOrder" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-sm" role="document">
        <div class="modal-content" id="form-content">
            <div class="modal-header">
                <h5 class="modal-title">COLLECTION ACCEPTANCE</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <h3 id="m-orderid"></h3>
                <div class="text-center my-2">
                    <img id="m-qr" class="img-fluid"/>
                </div>
                <form method="POST" action="{{ route('order.updateStorage') }}">
                    {{ csrf_field() }}
                    {{ method_field('PUT') }}
                <strong>ORDER STATUS</strong>
                <div class="custom-control custom-checkbox mb-2">
                    <input type="checkbox" class="custom-control-input" id="confirm" required>
                    <label class="custom-control-label" for="confirm"> Item has been received and verified</label>
                </div>
                <strong>STORAGE NUMBER</strong>
                <div class="form-group">
                    <input type="text" class="form-control" name="storage" placeholder="Storage Number" required>
                </div>
                <input type="hidden" name="id" id='m-formid'>
            </div>
            <div class="modal-footer">
                    <button class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                    <button type="submit" class="btn btn-danger">Save Changes</button>
            </div>
            </form>
        </div>
    </div>
</div>

<!-- Order Delivery Modal -->
<div class="modal fade" id="deliverOrder" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-sm" role="document">
        <div class="modal-content" id="form-content">
            <div class="modal-header">
                <h5 class="modal-title">COLLECTION DELIVERY</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <h3 id="m2-orderid"></h3>
                <div class="text-center my-2">
                    <img id="m2-qr" class="img-fluid"/>
                </div>
                <div class="mt-2"><strong>CUSTOMER INFORMATION</strong></div>
                <div class="row">
                    <div class="col-5">Name</div>
                    <div id="m2-name" class="col-7"></div>
                </div>
                <div class="row">
                    <div class="col-5">Email</div>
                    <div id="m2-email" class="col-7"></div>
                </div>
                <div class="row">
                    <div class="col-5">Mobile No</div>
                    <div id="m2-contact" class="col-7"></div>
                </div>
                <div class="row">
                    <div class="col-5">Collect Time</div>
                    <div id="m2-collecttime" class="col-7"></div>
                </div>
                <div class="row">
                    <div class="col-5">Collect Date</div>
                    <div id="m2-collectdate" class="col-7"></div>
                </div>
                <form method="POST" action="{{ route('order.updateDelivery') }}">
                    {{ csrf_field() }}
                    {{ method_field('PUT') }}
                    <strong>ORDER STATUS</strong>
                    <div class="custom-control custom-checkbox mb-2">
                        <input type="checkbox" class="custom-control-input" id="confirm2" required>
                        <label class="custom-control-label" for="confirm2"> Item has been verified and delivered</label>
                    </div>
                <strong>COLLECTED BY</strong>
                <div class="form-group mt-2">
                    <input type="text" class="form-control mt-2" name="collectorname" placeholder="Name" required>
                    <input type="text" class="form-control mt-2" name="collectorcontact" placeholder="Mobile Number" required>
                </div>
                <input type="hidden" name="id" id='m2-formid'>
            </div>
            <div class="modal-footer">
                    <button class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                    <button type="submit" class="btn btn-danger">Save Changes</button>
            </div>
            </form>
        </div>
    </div>
</div>

<!-- Order Collector Info Modal -->
<div class="modal fade" id="collectorOrder" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-sm" role="document">
        <div class="modal-content" id="form-content">
            <div class="modal-header">
                <h5 class="modal-title">ORDER INFORMATION</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <h3 id="m3-orderid"></h3>
                <div class="text-center my-2">
                    <img id="m3-qr" class="img-fluid"/>
                </div>
                <div class="mt-2"><strong>CUSTOMER INFORMATION</strong></div>
                <div class="row">
                    <div class="col-5">Name</div>
                    <div id="m3-name" class="col-7"></div>
                </div>
                <div class="row">
                    <div class="col-5">Email</div>
                    <div id="m3-email" class="col-7"></div>
                </div>
                <div class="row">
                    <div class="col-5">Mobile No</div>
                    <div id="m3-contact" class="col-7"></div>
                </div>
                <div class="row">
                    <div class="col-5">Collect Time</div>
                    <div id="m3-collecttime" class="col-7"></div>
                </div>
                <div class="row">
                    <div class="col-5">Collect Date</div>
                    <div id="m3-collectdate" class="col-7"></div>
                </div>
                <strong>COLLECTED BY</strong>
                <div class="row">
                    <div class="col-5">Name</div>
                    <div id="m3-collectorname" class="col-7"></div>
                </div>
                <div class="row">
                    <div class="col-5">Mobile Number</div>
                    <div id="m3-collectorcontact" class="col-7"></div>
                </div>
                <div class="row">
                    <div class="col-5">Time</div>
                    <div id="m3-collectortime" class="col-7"></div>
                </div>
                <div class="row">
                    <div class="col-5">Date</div>
                    <div id="m3-collectordate" class="col-7"></div>
                </div>
            </div>
            <div class="modal-footer">
                    <button class="btn btn-secondary" data-dismiss="modal">OK</button>
            </div>
        </div>
    </div>
</div>


<!-- Edit Storage Modal -->
<div class="modal fade" id="editStorage" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-sm" role="document">
        <div class="modal-content" id="form-content">
            <div class="modal-header">
                <h5 class="modal-title">UPDATE STORAGE</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form method="POST" action="{{ route('order.updateStorage') }}">
                    {{ csrf_field() }}
                    {{ method_field('PUT') }}
                <strong>STORAGE NUMBER</strong>
                <div class="form-group">
                    <input id="m4-storage" type="text" class="form-control" name="storage" placeholder="Storage Number" required>
                </div>
                <input type="hidden" name="id" id='m4-formid'>
            </div>
            <div class="modal-footer">
                    <button class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                    <button type="submit" class="btn btn-danger">Save Changes</button>
            </div>
            </form>
        </div>
    </div>
</div>
@endsection

@section('script')
<script>
    $(document).ready(function () {
        $('#orderTable').DataTable();

        $(document).on("click", "#receive-order", function () {
            var idClicked = $(this).attr("data-id");
            $('#m-qr').attr("src", "https://api.qrserver.com/v1/create-qr-code/?data={{Config::get('app.url')}}/qr/order/" + idClicked);
            $('#m-orderid').html('ORDER ID: #' + idClicked);
            $('#m-formid').attr('value', idClicked);
        });

        $(document).on("click", "#deliver-order", function () {
            var idClicked = $(this).attr("data-id");

            swal({
            title: 'Invoice and Delivery Order',
            text: 'Please prepare the Invoice and DO, signed copy of the stated documents need to be escalate to Account Department',
            type: 'info',
            confirmButtonColor: '#3085d6',
            confirmButtonText: 'Okay',
            }).then((result) => {
                if (result.value){
                    $('#m2-qr').attr("src", "https://api.qrserver.com/v1/create-qr-code/?data={{Config::get('app.url')}}/qr/order/" + idClicked);
                    $('#m2-orderid').html('ORDER ID: #' + idClicked);
                    $('#m2-formid').attr('value', idClicked);

                    //Customer Information
                    $('#m2-name').html(': ' + $('#name' + idClicked).attr("data-value"));
                    $('#m2-email').html(': ' + $('#email' + idClicked).attr("data-value"));
                    $('#m2-contact').html(': ' + $('#contact' + idClicked).attr("data-value"));
                    $('#m2-collecttime').html(': ' + $('#collecttime' + idClicked).attr("data-value"));
                    $('#m2-collectdate').html(': ' + $('#collectdate' + idClicked).attr("data-value"));

                    $('#deliverOrder').modal('show');
                }
            });

        });

        $(document).on("click", "#collector-order", function () {
            var idClicked = $(this).attr("data-id");

            $('#m3-qr').attr("src", "https://api.qrserver.com/v1/create-qr-code/?data={{Config::get('app.url')}}/qr/order/" + idClicked);
            $('#m3-orderid').html('ORDER ID: #' + idClicked);
            $('#m3-formid').attr('value', idClicked);
            $('#m3-storage').attr('value', $('#storage' + idClicked).attr("data-value"));

            //Customer Information
            $('#m3-name').html(': ' + $('#name' + idClicked).attr("data-value"));
            $('#m3-email').html(': ' + $('#email' + idClicked).attr("data-value"));
            $('#m3-contact').html(': ' + $('#contact' + idClicked).attr("data-value"));
            $('#m3-collecttime').html(': ' + $('#collecttime' + idClicked).attr("data-value"));
            $('#m3-collectdate').html(': ' + $('#collectdate' + idClicked).attr("data-value"));
            $('#m3-collectorname').html(': ' + $('#collectorname' + idClicked).attr("data-value"));
            $('#m3-collectorcontact').html(': ' + $('#collectorcontact' + idClicked).attr("data-value"));
            $('#m3-collectortime').html(': ' + $('#collectortime' + idClicked).attr("data-value"));
            $('#m3-collectordate').html(': ' + $('#collectordate' + idClicked).attr("data-value"));


        });

        $(document).on("click", "#storage-order", function () {
            var idClicked = $(this).attr("data-id");

            $('#m4-formid').attr('value', idClicked);
            $('#m4-storage').attr('value', $('#storage' + idClicked).attr("data-value"));

        });

        $(document).on("click", "#unpaid-order", function () {
            swal({
            title: 'Payment Not Complete!',
            text: 'Please complete the payment or get approval from Marketing Manager to proceed',
            type: 'error',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Go to Payment',
            cancelButtonText: 'Cancel'
            }).then((result) => {
                if (result.value)
                    window.location = '/dashboard/payment';
            })
        });
    });
</script>
<!--Custom JavaScript -->
<script src="{{ asset('assets/libs/sweetalert2/dist/sweetalert2.all.min.js') }}"></script>
@endsection
