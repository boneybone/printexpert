@extends('layouts.navbar')
@section('breadcrumb', 'Dashboard')
@section('content')
<div class="row">
    <div class="col-sm-4">
        <div class="card">
            <div class="card-body">
                <h5 class="card-title text-uppercase">NEW ORDER</h5>
                <div class="d-flex align-items-center">
                    <h2 class="mb-0 display-5"><i class="fas fa-clipboard text-info"></i></h2>
                    <div class="ml-3">
                        <h2 class="mb-0 display-6"><span class="font-normal">{{$order->where('status', 'Pending')->count()}}</span></h2>
                    </div>
                    <div class="ml-auto">
                            <a href="{{route('order.selectForm')}}" class="btn btn-success btn-rounded"><i class="fa fa-plus"></i> CREATE ORDER</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-sm-4">
        <div class="card">
            <div class="card-body">
                <h5 class="card-title text-uppercase">NEW CUSTOMER</h5>
                <div class="d-flex align-items-center">
                    <h2 class="mb-0 display-5"><i class="fas fa-users text-primary"></i></h2>
                    <div class="ml-3">
                        <h2 class="mb-0 display-6"><span class="font-normal">{{$customerCount}}</span></h2>
                    </div>
                    <div class="ml-auto">
                            <button type="button" class="btn btn-success btn-rounded" data-toggle="modal" data-target="#createuser"><i class="fa fa-plus"></i> ADD CUSTOMER</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-sm-4">
        <div class="card">
            <div class="card-body">
                <h5 class="card-title text-uppercase">PAYMENT COUNTER</h5>
                <div class="d-flex align-items-center">
                    <h2 class="mb-0 display-5"><i class="fas fa-credit-card"></i></h2>
                    <div class="ml-auto">
                        <h2 class="mb-0 display-6"><span class="font-normal"></span></h2>
                    </div>
                    <div class="ml-auto">
                            <a href="{{route('dashboard.payment')}}" class="btn btn-success btn-rounded btn-info"> GO TO PAYMENT</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="card">
        <div class="d-flex no-block align-items-center col-lg-12 py-3" style="border-bottom: 1px solid #eaeaea;">
            <h4 class="card-title mb-0 ml-2">TRACK ORDER</h4>
        </div>
        <div class="card-body">
            <div class="form-group">
                <input type="search" class="form-control" placeholder="Search order by Order ID, Created By, Phone Number"
                    id="searchOrder">
            </div>
            <div class="table-responsive" id="searchOrderTable">
            </div>
        </div>
    </div>

    <div class="card">
        <div class="d-flex no-block align-items-center col-lg-12 py-3" style="border-bottom: 1px solid #eaeaea;">
            <h4 class="card-title mb-0 ml-2">ORDER LIST</h4>
        </div>
        <div class="card-body">
            <div class="table-responsive">
                    @include('order.indexdatamarketingmanager')
            </div>
        </div>
    </div>

<!-- Create UserModal -->
<div class="modal fade" id="createuser" tabindex="-1" role="dialog" aria-labelledby="createModalLabel"
    aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <form method="POST" action="{{ route('user.store') }}">
                @csrf
                <div class="modal-header">
                    <h4 class="modal-title" id="createModalLabel"><i class="ti-user mr-2"></i> Add New Customer</h4>
                    <button class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="input-group mb-3">
                            <select class="form-control" name="title" required>
                                <option value="" disabled selected>Select Title</option>
                                <option>Mr</option>
                                <option>Mrs</option>
                                <option>Miss</option>
                                <option>Sir</option>
                                <option>Madam</option>
                            </select>
                    </div>
                    <div class="input-group mb-3">
                        <input type="text" class="form-control" placeholder="Enter Name Here" name="name" required>
                    </div>
                    <div class="input-group mb-3">
                        <input type="text" class="form-control" placeholder="Enter IC Number Here" name="nric" required>
                    </div>
                    <div class="input-group mb-3">
                        <input type="text" class="form-control" pattern="[0-9]+" placeholder="Enter mobile number here (Number only)" name="contact"
                            required>
                    </div>
                    <div class="input-group mb-3">
                        <input type="Email" class="form-control" placeholder="Enter Email Here" name="email" required>
                    </div>
                    <input type="hidden" name="role" value="Customer">
                </div>
                <div class="modal-footer">
                    <button class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                    <button type="submit" class="btn btn-success"><i class="ti-save mr-2"></i> Save</button>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection

@section('script')
<script>
        $(document).ready(function () {

            $('#orderTable thead tr').clone(true).appendTo('#orderTable thead');
            $('#orderTable thead tr:eq(1) th').each(function (i) {

                var title = $(this).text().toLowerCase().replace(/\b[a-z]/g, function (txtVal) {
                    return txtVal.toUpperCase();
                });
                $(this).html('<input type="text" class="form-control form-control-sm" placeholder="Search ' + title + '" />');

                $('input', this).on('keyup change', function () {
                    if($('#orderTable').DataTable().column(i).search() !== this.value)
                        $('#orderTable').DataTable().column(i).search(this.value).draw();
                });
            });

            $('#orderTable').DataTable({
                orderCellsTop: true,
                fixedHeader: true
            });

        });

        $('#searchOrder').on('keyup', function () {
            $value = $(this).val();

            if($value != '')
            {
                $.ajax({
                    type: 'get',
                    url: '/order/search',
                    data: {
                        'search': $value
                    },
                    success: function (searchResult) {
                        $('#searchOrderTable').html(searchResult);
                        $('#idSearchTable').DataTable();
                        $('#contactSearchTable').DataTable();
                        $('#creatorSearchTable').DataTable();
                    }
                });
            }
            else
                $('#searchOrderTable').html(null);

        });

        $(document).on("click", "button.holdOrder", function () {
            orderID = $(this).attr("data-id");
            swal({
                title: 'Enter your reason',
                input: 'text',
                showCancelButton: true,
                inputValidator: (value) => {
                    if (value == '') {
                        return 'You need to write something!'
                    }
                }
            }).then((result) => {
                if (result.value) {
                    $.ajax({
                        type: 'PUT',
                        url: '/order/updateStatus',
                        data: {
                            onholdreason: result.value,
                            id: orderID
                        },
                        success: function (data) {
                            swal('Task On Hold!', data.message, 'success').then((result) => {location.reload();});
                        }
                    });
                }
            })
        });

        $(document).on("click", "input.unpaidCollection", function () {
            orderID = $(this).attr("data-id");

            $.ajax({
                type: 'PUT',
                url: '/order/updateUnpaid',
                data: {
                    id: orderID
                },
                success: function (data) {
                    swal(data.type, data.message, 'success').then((result) => {location.reload();});
                }
            });

        });

        $(document).on("click", "button.resumeOrder", function () {
            orderID = $(this).attr("data-id");
            swal({
                title: 'Are you sure?',
                text: 'Resume this task?',
                type: 'question',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes',
                cancelButtonText: 'No'
            }).then((result) => {
                if (result.value) {
                    $.ajax({
                        type: 'PUT',
                        url: '/order/updateStatus',
                        data: {
                            onholdreason: null,
                            id: orderID
                        },
                        success: function (data) {
                            swal('Task Resumed!', data.message, 'success').then((result) => {location.reload();});
                            $('#orderTable').load('/order/refreshList');
                        }
                    });
                }
            })
        });

        $(document).on("click", ".takeorder", function () {
            orderID = $(this).attr("data-id");
            swal({
              title: 'Are you sure?',
              text: 'Assign task to yourself?',
              type: 'question',
              showCancelButton: true,
              confirmButtonColor: '#3085d6',
              cancelButtonColor: '#d33',
              confirmButtonText: 'Yes',
              cancelButtonText: 'No'
            }).then((result) => {
              if (result.value) {
                $.ajax({
                type: 'PUT',
                url: '/order/assign',
                data: {
                    id: orderID
                },
                success: function (data) {
                    swal("Task Assigned!", data.message, "success").then((result) => {location.reload();});
                }
                });
              }
            })
        });
    </script>
    <!--Custom JavaScript -->
    <script src="{{ asset('assets/libs/sweetalert2/dist/sweetalert2.all.min.js') }}"></script>
@endsection
