@extends('layouts.navbar')
@section('head')
<link rel="stylesheet" type="text/css" href="{{ asset('css/stylecustom.css')}}">
@endsection
@section('breadcrumb', 'Dashboard')
@section('content')
<div id="data">
    @include('dashboard.managerdata')
</div>
@endsection

@section('script')
<script>
$(document).ready(function () {

    hide();

    function hide()
    {
        $('#superurgent').hide();
        $('#urgent').hide();
        $('#normal').hide();
        $('.resume').hide();
        $('#assignStaff').hide();
        $('#orderDetail').hide();
    }

    $(document).on("click", "a.orderurgency", function () {
        var urgency = $(this).attr("data-value");
        hide();
        clearActiveState(urgency);
    });

    function clearActiveState(id)
    {
        $('#orderDetail').show();
        $("#order-list").find(".list-group-item").removeClass("active show");
        $("#" + id).find(".list-group-item:first").addClass("active show");
        $("#" + id).show();
        orderDetail($('#first' + id).attr("data-id"));
    }

    $(document).on("click", "#view-order", function () {
        var idClicked = $(this).attr("data-id");
        orderDetail(idClicked);
    });

    function orderDetail(orderID) {
        //var esttime = dhm($('#d-estimated_time' + orderID).attr("data-value"));
        $('#f-orderid').attr('value', orderID);
        $('#timecreated').html($('#d-timecreated' + orderID).attr("data-value"));
        $('#estimated_time').html('ESTIMATED TIME: ' + $('#d-estimated_time' + orderID).attr("data-value"));

        $('#h-orderid').html('#' + $('#d-orderid' + orderID).attr("data-value"));
        $('#h-ordertype').html($('#d-type' + orderID).attr("data-value"));
        $('#status').html($('#d-status' + orderID).attr("data-value"));
        $('#urgency').html($('#d-urgency' + orderID).attr("data-value"));
        $('#createdbranch').html($('#d-createdbranch' + orderID).attr("data-value"));
        $('#collectbranch').html($('#d-collectbranch' + orderID).attr("data-value"));
        if($('#d-remark' + orderID).attr("data-value") != '')
            $('#remark').html($('#d-remark' + orderID).attr("data-value"));
        else
            $('#remark').html('None');

        $('#orderid').html($('#d-orderid' + orderID).attr("data-value"));
        $('#ordertype').html($('#d-type' + orderID).attr("data-value"));

        $('#name').html($('#d-name' + orderID).attr("data-value"));
        $('#email').html($('#d-email' + orderID).attr("data-value"));
        $('#contact').html($('#d-contact' + orderID).attr("data-value"));
        $('#collecttime').html($('#d-collecttime' + orderID).attr("data-value"));
        $('#collectdate').html($('#d-collectdate' + orderID).attr("data-value"));

        $('#m-orderid').html('Order ID: #' + $('#d-orderid' + orderID).attr("data-value"));
        $('#m-qr').attr("src", "https://api.qrserver.com/v1/create-qr-code/?data={{Config::get('app.url')}}/qr/order/" + orderID);
        $('#m-name').html(": " + $('#d-name' + orderID).attr("data-value"));
        $('#m-email').html(": " + $('#d-email' + orderID).attr("data-value"));
        $('#m-contact').html(": " + $('#d-contact' + orderID).attr("data-value"));
        $('#m-collecttime').html(": " + $('#d-collecttime' + orderID).attr("data-value"));
        $('#m-collectdate').html(": " + $('#d-collectdate' + orderID).attr("data-value"));

        $('#orderdetails').html($('#d-orderdetails' + orderID).html());
        $('#downloadlink').attr('href', $('#d-downloadlink' + orderID).attr("data-value"));

        if($('#d-sketch' + orderID).attr("data-value") != '#')
        {
            $('#sketch-img').attr("src", $('#d-sketch' + orderID).attr("data-value"));
            $('#sketch').show();
        }
        else
        {
            $('#sketch').hide();
        }

        if($('#d-downloadlink' + orderID).attr("data-value") == '#')
            $('#downloadlink').hide();
        else
            $('#downloadlink').show();

        if($('#d-status' + orderID).attr("data-value") == 'On Hold ')
        {
            $('.resume').show();
            $('#assignStaff').hide();
        }
        else
        {
            $('.resume').hide();
            $('#assignStaff').show();
        }

    }

    $(document).on("click", "button.resume", function () {
        swal({
          title: 'Resume this job?',
          type: 'question',
          showCancelButton: true,
          confirmButtonColor: '#3085d6',
          cancelButtonColor: '#d33',
          confirmButtonText: 'Yes',
          cancelButtonText: 'No'
        }).then((result) => {
          if (result.value) {
            $.ajax({
            type: 'PUT',
            url: '/order/updateStatus',
            data: {
                onholdreason: null,
                id: $("input[name=id]").val()
            },
            success: function (data) {
                swal('Task Resumed!', data.message,'success')
                $('#data').load('/dashboard/data/manager', function() {
                    hide();
                });
            }
            });
          }
        })
    });

    $(document).on("click", "a.dropdown-item.staff", function () {
        swal({
          title: 'Are you sure?',
          text: 'Assign task to' + this.text + '?',
          type: 'question',
          showCancelButton: true,
          confirmButtonColor: '#3085d6',
          cancelButtonColor: '#d33',
          confirmButtonText: 'Yes',
          cancelButtonText: 'No'
        }).then((result) => {
          if (result.value) {
            $.ajax({
            type: 'PUT',
            url: '/order/assign',
            data: {
                staff_id: $("input[name=staff_id]").val(),
                id: $("input[name=id]").val()
            },
            success: function (data) {
                swal("Task Assigned!", data.message, "success")
                $('#data').load('/dashboard/data/manager', function() {
                    hide();
                });
            }
            });
          }
        })
    });

    //Convert milisecond to time format
    function dhm(ms){
        days = Math.floor(ms / (24*60*60*1000));
        daysms=ms % (24*60*60*1000);
        hours = Math.floor((daysms)/(60*60*1000));
        hoursms=ms % (60*60*1000);
        minutes = Math.floor((hoursms)/(60*1000));
        minutesms=ms % (60*1000);
        sec = Math.floor((minutesms)/(1000));

        function check(t, prefix){
            if(t == 0)
                return '';
            return t + prefix;
        }

        return check(days, ' DAY ') + check(hours, ' HOUR ') + check(minutes, ' MINUTE ');
    }


});
</script>
<!--Custom JavaScript -->
<script src="{{ asset('assets/libs/sweetalert2/dist/sweetalert2.all.min.js') }}"></script>
@endsection
