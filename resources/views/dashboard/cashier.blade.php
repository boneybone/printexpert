@extends('layouts.navbar')
@section('breadcrumb', 'Dashboard')
@section('content')
<div class="row">
    <div class="col-sm-9">
        <div class="card">
            <div class="card-body">
                <div class="d-flex no-block align-items-center mb-4">
                    <h4 class="card-title">ORDER LIST</h4>
                </div>
                <div class="table-responsive">
                    <table id="orderTable" class="table table-bordered nowrap display">
                        <thead>
                            <tr>
                                <th>TITLE</th>
                                <th>ORDER ID</th>
                                <th>CUSTOMER</th>
                                <th>JOB STATUS</th>
                                <th>PAYMENT</th>
                                <th>ACTION</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($order as $o)
                            <tr>
                                <td>{{$o->form['name']}}</td>
                                <td id="id{{$o->id}}">{{$o->id}}</td>
                                <td>{{$o->customer['name']}}<br>
                                    <span class="text-muted">{{$o->customer['email']}}</span>
                                </td>
                                <td id="status{{$o->id}}">{{$o->status}}</td>
                                <td>Unpaid</td>
                                <td><button class="btn btn-info btn-circle" data-toggle="modal" id="view-order"
                                        data-target="#viewOrder" data-id="{{$o->id}}"><i class="fas fa-edit"></i></button></td>
                            </tr>
                                <div id="department{{$o->id}}" style="display: none;" data-value="{{$o->department['name']}}">
                                <div id="name{{$o->id}}" style="display: none;" data-value="{{$o->customer['name']}}">
                                <div id="email{{$o->id}}" style="display: none;" data-value="{{$o->customer['email']}}">
                                <div id="contact{{$o->id}}" style="display: none;" data-value="{{$o->customer['contact']}}">
                                <div id="collecttime{{$o->id}}" style="display: none;" data-value="{{date('g:i A', strtotime($o->collection))}}">
                                <div id="collectdate{{$o->id}}" style="display: none;" data-value="{{date('d/m/Y', strtotime($o->collection))}}">
                             @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <div class="col-sm-3">
        <div class="card">
            <div class="card-body">
                <h5 class="card-title text-uppercase">QUICK PRINT</h5>
                <div class="d-flex align-items-center">
                    <h2 class="mb-0 display-5"><i class="fas fa-print"></i></h2>
                    <div class="ml-auto">
                        <h2 class="mb-0 display-6"><span class="font-normal"></span></h2>
                    </div>
                    <div class="ml-auto">
                        <button type="button" class="btn btn-info btn-rounded"> CREATE ORDER</button>
                    </div>
                </div>
            </div>
        </div>
        <div class="card">
            <div class="card-body">
                <h5 class="card-title text-uppercase">UNPAID PAYMENT</h5>
                <div class="d-flex align-items-center">
                    <h2 class="mb-0 display-5"><i class=" far fa-money-bill-alt text-warning"></i></h2>
                    <div class="ml-auto">
                        <h2 class="mb-0 display-6"><span class="font-normal">{{$order->count()}}</span></h2>
                    </div>
                </div>
            </div>
        </div>
        {{-- <div class="card">
            <div class="card-body">
                <h4 class="card-title">RECENT ACTIVITY</h4>
                <div class="feed-widget scrollable ps-container ps-theme-default" style="height:450px;"
                    data-ps-id="1f9bb473-2fae-f2ac-77a7-0a556d1a73b7">
                    <ul class="list-style-none feed-body m-0 pb-3">
                        <li class="feed-item">
                            <div class="feed-icon bg-info"><i class="far fa-bell"></i></div> You have 4 pending tasks.
                            <span class="ml-auto font-12 text-muted">Just Now</span>
                        </li>
                    </ul>
                    <div class="ps-scrollbar-x-rail" style="left: 0px; bottom: 0px;">
                        <div class="ps-scrollbar-x" tabindex="0" style="left: 0px; width: 0px;"></div>
                    </div>
                    <div class="ps-scrollbar-y-rail" style="top: 0px; right: 3px;">
                        <div class="ps-scrollbar-y" tabindex="0" style="top: 0px; height: 0px;"></div>
                    </div>
                </div>
            </div>
        </div> --}}
    </div>
</div>

<!-- View Order Modal -->
<div class="modal fade" id="viewOrder" tabindex="-1" role="dialog" aria-labelledby="createModalLabel"
    aria-hidden="true">
    <div class="modal-dialog modal-sm" role="document">
        <div class="modal-content" id="form-content">
            <div class="modal-header">
                <h5 class="modal-title" id="m-orderid"></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <strong>ORDER STATUS</strong>
                <div class="row">
                    <div class="col-4">Status</div>
                    <div id="m-status" class="col-8"></div>
                </div>
                <div class="row">
                    <div class="col-4">Current Queue</div>
                    <div id="m-department" class="col-8"></div>
                </div>
                <strong>CUSTOMER INFORMATION</strong>
                <div class="row">
                    <div class="col-4">Name</div>
                    <div id="m-name" class="col-8"></div>
                </div>
                <div class="row">
                    <div class="col-4">Email</div>
                    <div id="m-email" class="col-8"></div>
                </div>
                <div class="row">
                    <div class="col-4">Mobile Number</div>
                    <div id="m-contact" class="col-8"></div>
                </div>
                <div class="row">
                    <div class="col-4">Collect Time</div>
                    <div id="m-collecttime" class="col-8"></div>
                </div>
                <div class="row">
                    <div class="col-4">Collect Date</div>
                    <div id="m-collectdate" class="col-8"></div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-info mx-auto" data-dismiss="modal">OK</button>
            </div>
        </div>
    </div>
</div>
@endsection

@section('script')
<script>
    $(document).ready(function () {
        $('#orderTable').DataTable();

        $(document).on("click", "#view-order", function () {
            var idClicked = $(this).attr("data-id");

            $('#m-orderid').html('<i class="mdi mdi-printer mr-2"></i>ORDER ID: ' + idClicked);
            $('#m-status').html(': ' + $('#status' + idClicked).html());
            $('#m-department').html(': ' + $('#department' + idClicked).attr("data-value") + ' Department');
            $('#m-name').html(': ' + $('#name' + idClicked).attr("data-value"));
            $('#m-email').html(': ' + $('#email' + idClicked).attr("data-value"));
            $('#m-contact').html(': ' + $('#contact' + idClicked).attr("data-value"));
            $('#m-collecttime').html(': ' + $('#collecttime' + idClicked).attr("data-value"));
            $('#m-collectdate').html(': ' + $('#collectdate' + idClicked).attr("data-value"));
        });

    });
</script>
@endsection
