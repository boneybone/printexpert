@extends('layouts.navbar')
@section('head')
<link rel="stylesheet" type="text/css" href="{{ asset('css/fileUploader.css')}}">
<link rel="stylesheet" type="text/css" href="{{ asset('css/stylecustom.css')}}">
@endsection
@section('breadcrumb', 'Dashboard')
@section('content')
<div id="data">
    @include('dashboard.staffdata')
</div>
@endsection

@section('script')
<script>
$(document).ready(function () {

    hide();

    function hide()
    {
        $('#pending').hide();
        $('#inprocess').hide();
        $('#completed').hide();
        $('#onhold').hide();
        $('#orderDetail').hide();
        $('#assignStaff').hide();
    }

    $(document).on("click", "a.orderstatus", function () {
        var status = $(this).attr("data-value");
        hide();
        clearActiveState(status);
    });

    function clearActiveState(status)
    {
        $('#orderDetail').show();
        $("#order-list").find(".list-group-item").removeClass("active show");
        $("#" + status).find(".list-group-item:first").addClass("active show");
        $("#" + status).show();
        orderDetail($('#first' + status).attr("data-id"));
    }

    $(document).on("click", "#view-order", function () {
        var idClicked = $(this).attr("data-id");
        orderDetail(idClicked);
    });

    function orderDetail(orderID) {
        $('#f-orderid').attr('value', orderID);
        $('#timecreated').html($('#d-timecreated' + orderID).attr("data-value"));
        $('#nextdept').attr("data-value", ($('#d-nextdept' + orderID).attr("data-value")));
        //var esttime = dhm($('#d-estimated_time' + orderID).attr("data-value"));
        // $('#estimated_time').html('ESTIMATED TIME: ' + $('#d-estimated_time' + orderID).attr("data-value"));

        $('#h-orderid').html('#' + $('#d-orderid' + orderID).attr("data-value"));
        $('#h-ordertype').html($('#d-type' + orderID).attr("data-value"));
        $('#status').html($('#d-status' + orderID).attr("data-value"));
        $('#urgency').html($('#d-urgency' + orderID).attr("data-value"));
        $('#createdbranch').html($('#d-createdbranch' + orderID).attr("data-value"));
        $('#collectbranch').html($('#d-collectbranch' + orderID).attr("data-value"));
        if($('#d-remark' + orderID).attr("data-value") != '')
            $('#remark').html($('#d-remark' + orderID).attr("data-value"));
        else
            $('#remark').html('None');

        $('#orderid').html($('#d-orderid' + orderID).attr("data-value"));
        $('#ordertype').html($('#d-type' + orderID).attr("data-value"));

        $('#name').html($('#d-name' + orderID).attr("data-value"));
        $('#email').html($('#d-email' + orderID).attr("data-value"));
        $('#contact').html($('#d-contact' + orderID).attr("data-value"));
        $('#collecttime').html($('#d-collecttime' + orderID).attr("data-value"));
        $('#collectdate').html($('#d-collectdate' + orderID).attr("data-value"));

        $('#m-orderid').html('Order ID: #' + $('#d-orderid' + orderID).attr("data-value"));
        $('#m-qr').attr("src", "https://api.qrserver.com/v1/create-qr-code/?data={{Config::get('app.url')}}/qr/order/" + orderID);
        $('#m-name').html(": " + $('#d-name' + orderID).attr("data-value"));
        $('#m-email').html(": " + $('#d-email' + orderID).attr("data-value"));
        $('#m-contact').html(": " + $('#d-contact' + orderID).attr("data-value"));
        $('#m-collecttime').html(": " + $('#d-collecttime' + orderID).attr("data-value"));
        $('#m-collectdate').html(": " + $('#d-collectdate' + orderID).attr("data-value"));

        if( $('#d-roundcorner' + orderID).length )
        {
            if($('#d-roundcorner' + orderID).attr("data-value").indexOf('1') != -1)
                $('#carddesign').css('border-top-left-radius', '40px');
            else
                $('#carddesign').css('border-top-left-radius', '2px');

            if($('#d-roundcorner' + orderID).attr("data-value").indexOf('2') != -1)
                $('#carddesign').css('border-top-right-radius', '40px');
            else
                $('#carddesign').css('border-top-right-radius', '2px');

            if($('#d-roundcorner' + orderID).attr("data-value").indexOf('3') != -1)
                $('#carddesign').css('border-bottom-left-radius', '40px');
            else
                $('#carddesign').css('border-bottom-left-radius', '2px');

            if($('#d-roundcorner' + orderID).attr("data-value").indexOf('4') != -1)
                $('#carddesign').css('border-bottom-right-radius', '40px');
            else
                $('#carddesign').css('border-bottom-right-radius', '2px');

            $('#carddesigndiv').show();
        }
        else
        {
            $('#carddesigndiv').hide();
        }

        $('#orderdetails').html($('#d-orderdetails' + orderID).html());

        if($('#d-sketch' + orderID).attr("data-value") != '#')
        {
            $('#sketch-img').attr("src", $('#d-sketch' + orderID).attr("data-value"));
            $('#sketch').show();
        }
        else
        {
            $('#sketch').hide();
        }

        if($('#d-downloadlink' + orderID).attr("data-value") == '#')
        {
            $('#dfilebutton').hide();
            $('#filebutton').show();
            $('#filebutton').html(' <form method="POST" class="container" enctype="multipart/form-data" action="{{route('order.updateDesign')}}"> {{ csrf_field() }} {{ method_field('PUT') }} <div class="form-group"> <span class="btn btn-light btn-block fileinput-button" id="uploadFile"> <h1><span class="mdi mdi-cloud-upload"></span></h1> <h6>Drag and drop design here <br> MAX SIZE: {{ Config::get('medialibrary.max_file_size')/1024/1024 }} MB</h6> <input type="file" name="design" onchange="this.form.submit()" id="ufile"> </span> </div> <input type="hidden" name="id" value="' + orderID + '"> </div> </form>');
        }
        else
        {
            $('#dfilebutton').show();
            $('#filebutton').hide();
            $('#downloadlink').attr('href', $('#d-downloadlink' + orderID).attr("data-value"));
        }
        $('.inprocess').show();
        $('.completed').show();
        $('.resume').hide();
        $('#assignStaff').hide();

        if($('#d-status' + orderID).attr("data-value") == 'In Process')
        {
            $('.inprocess').hide();
            $('.resume').hide();
            $('#assignStaff').hide();
        }
        if($('#d-status' + orderID).attr("data-value") == 'Completed')
        {
            $('.inprocess').hide();
            $('.completed').hide();
            $('.resume').hide();
            $('#assignStaff').show();
        }
        if($('#d-status' + orderID).attr("data-value") == 'On Hold ')
        {
            $('.inprocess').hide();
            $('.completed').hide();
            $('.resume').show();
            $('#assignStaff').hide();
        }

    }

    $(document).on("click", "button.inprocess", function () {
        swal({
          title: 'Are you sure?',
          type: 'question',
          showCancelButton: true,
          confirmButtonColor: '#3085d6',
          cancelButtonColor: '#d33',
          confirmButtonText: 'Yes',
          cancelButtonText: 'No'
        }).then((result) => {
          if (result.value) {
            $.ajax({
            type: 'PUT',
            url: '/order/updateStatus',
            data: {
                status: 'In Process',
                id: $("input[name=id]").val()
            },
            success: function (data) {
                swal('Task Started!','','success')
                $('#data').load('/dashboard/data/staff', function() {
                    hide();
                });
            }
            });
          }
        })
    });

    $(document).on("click", "button.completed", function () {
        swal({
          title: 'Are you sure?',
          type: 'question',
          showCancelButton: true,
          confirmButtonColor: '#3085d6',
          cancelButtonColor: '#d33',
          confirmButtonText: 'Yes',
          cancelButtonText: 'No'
        }).then((result) => {
          if (result.value) {
            $.ajax({
            type: 'PUT',
            url: '/order/updateStatus',
            data: {
                status: 'Completed',
                id: $("input[name=id]").val()
            },
            success: function (data) {
                swal('Task Completed!', data.message,'success')
                $('#data').load('/dashboard/data/staff', function() {
                    hide();
                });
            }
            });
          }
        })
    });

    $(document).on("click", "a.dropdown-item.staff", function () {
        swal({
          title: 'Are you sure?',
          text: 'Assign task to' + this.text + '?',
          type: 'question',
          showCancelButton: true,
          confirmButtonColor: '#3085d6',
          cancelButtonColor: '#d33',
          confirmButtonText: 'Yes',
          cancelButtonText: 'No'
        }).then((result) => {
          if (result.value) {
            $.ajax({
            type: 'PUT',
            url: '/order/assign',
            data: {
                staff_id: $("input[name=staff_id]").val(),
                id: $("input[name=id]").val()
            },
            success: function (data) {
                if(data.error)
                    swal("Task Not Assigned!", data.error, "error")
                else
                    swal("Task Assigned!", data.message, "success")

                $('#data').load('/dashboard/data/staff', function() {
                    hide();
                });
            }
            });
          }
        })
    });

    $(document).on("click", "button.resume", function () {
        swal({
          title: 'Resume this job?',
          type: 'question',
          showCancelButton: true,
          confirmButtonColor: '#3085d6',
          cancelButtonColor: '#d33',
          confirmButtonText: 'Yes',
          cancelButtonText: 'No'
        }).then((result) => {
          if (result.value) {
            $.ajax({
            type: 'PUT',
            url: '/order/updateStatus',
            data: {
                onholdreason: null,
                id: $("input[name=id]").val()
            },
            success: function (data) {
                swal('Task Resumed!', data.message,'success')
                $('#data').load('/dashboard/data/staff', function() {
                    hide();
                });
            }
            });
          }
        })
    });

    $(document).on("click", "#btnAssign", function () {
        // var orderID =  $("input[name=id]").val();
        $(".dropdown-item.staff").hide();
        var deptid = $('#nextdept').attr("data-value");
        $(".dropdown-item.staff[data-dept=" + deptid + "]").show();
        console.log(deptid);
        $(this).attr("data-toggle", "dropdown");

    });

    //Convert milisecond to time format
    function dhm(ms){
        days = Math.floor(ms / (24*60*60*1000));
        daysms=ms % (24*60*60*1000);
        hours = Math.floor((daysms)/(60*60*1000));
        hoursms=ms % (60*60*1000);
        minutes = Math.floor((hoursms)/(60*1000));
        minutesms=ms % (60*1000);
        sec = Math.floor((minutesms)/(1000));

        function check(t, prefix){
            if(t == 0)
                return '';
            return t + prefix;
        }

        return check(days, ' DAY ') + check(hours, ' HOUR ') + check(minutes, ' MINUTE ');
    }

});
</script>
<!--Custom JavaScript -->
<script src="{{ asset('assets/libs/sweetalert2/dist/sweetalert2.all.min.js') }}"></script>
@endsection
