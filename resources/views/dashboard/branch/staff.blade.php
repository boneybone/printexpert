@extends('layouts.navbar')
@section('head')
<link rel="stylesheet" type="text/css" href="{{ asset('css/stylecustom.css')}}">
@endsection
@section('breadcrumb', 'Dashboard')
@section('content')
<div id="data">
    @include('dashboard.branch.staffdata')
</div>
@endsection

@section('script')
<script>
$(document).ready(function () {

    hide();

    function hide()
    {
        $('#pending').hide();
        $('#logisticshq').hide();
        $('#arrivedhq').hide();
        $('#logisticsbranch').hide();
        $('#orderDetail').hide();

        //button
        $('.itemarrived').hide();
        $('.distributebranch').hide();
        $('.completed').hide();

    }

    $(document).on("click", "a.orderstatus", function () {
        var status = $(this).attr("data-value");
        hide();
        clearActiveState(status);
    });

    function clearActiveState(id)
    {
        $('#orderDetail').show();
        $("#order-list").find(".list-group-item").removeClass("active show");
        $("#" + id).find(".list-group-item:first").addClass("active show");
        $("#" + id).show();
        orderDetail($('#first' + id).attr("data-id"));
    }

    $(document).on("click", "#view-order", function () {
        var idClicked = $(this).attr("data-id");
        orderDetail(idClicked);
    });

    function orderDetail(orderID) {
        $('#f-orderid').attr('value', orderID);
        $('#timecreated').html($('#d-timecreated' + orderID).attr("data-value"));

        $('#h-orderid').html('#' + $('#d-orderid' + orderID).attr("data-value"));
        $('#h-ordertype').html($('#d-type' + orderID).attr("data-value"));
        $('#h-collectbranch').html(' Branch Collection: ' + $('#d-collectbranch' + orderID).attr("data-value"));

        $('#status').html($('#d-status' + orderID).attr("data-value"));
        $('#urgency').html($('#d-urgency' + orderID).attr("data-value"));
        $('#createdbranch').html($('#d-createdbranch' + orderID).attr("data-value"));
        $('#collectbranch').html($('#d-collectbranch' + orderID).attr("data-value"));
        if($('#d-remark' + orderID).attr("data-value") != '')
            $('#remark').html($('#d-remark' + orderID).attr("data-value"));
        else
            $('#remark').html('None');

        $('#orderid').html($('#d-orderid' + orderID).attr("data-value"));
        $('#ordertype').html($('#d-type' + orderID).attr("data-value"));

        $('#name').html($('#d-name' + orderID).attr("data-value"));
        $('#email').html($('#d-email' + orderID).attr("data-value"));
        $('#contact').html($('#d-contact' + orderID).attr("data-value"));
        $('#collecttime').html($('#d-collecttime' + orderID).attr("data-value"));
        $('#collectdate').html($('#d-collectdate' + orderID).attr("data-value"));

        $('#m-orderid').html('Order ID: #' + $('#d-orderid' + orderID).attr("data-value"));
        $('#m-qr').attr("src", "https://api.qrserver.com/v1/create-qr-code/?data={{Config::get('app.url')}}/qr/order/" + orderID);
        $('#m-name').html(": " + $('#d-name' + orderID).attr("data-value"));
        $('#m-email').html(": " + $('#d-email' + orderID).attr("data-value"));
        $('#m-contact').html(": " + $('#d-contact' + orderID).attr("data-value"));
        $('#m-collecttime').html(": " + $('#d-collecttime' + orderID).attr("data-value"));
        $('#m-collectdate').html(": " + $('#d-collectdate' + orderID).attr("data-value"));

        $('#orderdetails').html($('#d-orderdetails' + orderID).html());
        $('#downloadlink').attr('href', $('#d-downloadlink' + orderID).attr("data-value"));

        $('.inprocess').show();

        if($('#d-status' + orderID).attr("data-value") == 'Logistics to HQ')
            $('.itemarrived').show();
        else if($('#d-status' + orderID).attr("data-value") == 'Arrived at HQ')
            $('.distributebranch').show();



    }

    $(document).on("click", "button.itemarrived", function () {
        swal({
          title: 'Order has arrived?',
          text: 'This will mark order item from offset has arrived at HQ.',
          type: 'question',
          showCancelButton: true,
          confirmButtonColor: '#3085d6',
          cancelButtonColor: '#d33',
          confirmButtonText: 'Yes',
          cancelButtonText: 'No'
        }).then((result) => {
          if (result.value) {
            $.ajax({
            type: 'PUT',
            url: '/order/updateStatus',
            data: {
                status: 'Arrived at HQ',
                id: $("input[name=id]").val()
            },
            success: function (data) {
                swal('Order Arrived!','','success')
                $('#data').load('/dashboard/data/staff', function() {
                    hide();
                });
            }
            });
          }
        })
    });


    $(document).on("click", "button.distributebranch", function () {
        swal({
          title: 'Distribute to Branch?',
          text: 'This will mark this order has been collected and distributed to collection branch.',
          type: 'question',
          showCancelButton: true,
          confirmButtonColor: '#3085d6',
          cancelButtonColor: '#d33',
          confirmButtonText: 'Yes',
          cancelButtonText: 'No'
        }).then((result) => {
          if (result.value) {
            $.ajax({
            type: 'PUT',
            url: '/order/updateStatus',
            data: {
                status: 'Logistics to Branch',
                id: $("input[name=id]").val()
            },
            success: function (data) {
                swal('Order distributed to Branch!','','success')
                $('#data').load('/dashboard/data/staff', function() {
                    hide();
                });
            }
            });
          }
        })
    });

    $(document).on("click", "button.completed", function () {
        swal({
          title: 'Are you sure?',
          type: 'question',
          showCancelButton: true,
          confirmButtonColor: '#3085d6',
          cancelButtonColor: '#d33',
          confirmButtonText: 'Yes',
          cancelButtonText: 'No'
        }).then((result) => {
          if (result.value) {
            $.ajax({
            type: 'PUT',
            url: '/order/updateStatus',
            data: {
                status: 'Completed',
                id: $("input[name=id]").val()
            },
            success: function (data) {
                swal('Task Completed!', data.message,'success')
                $('#data').load('/dashboard/data/staff', function() {
                    hide();
                });
            }
            });
          }
        })
    });

});
</script>
<!--Custom JavaScript -->
<script src="{{ asset('assets/libs/sweetalert2/dist/sweetalert2.all.min.js') }}"></script>
@endsection
