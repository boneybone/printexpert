<div class="col-sm-12">
    <div class="card-group">
    <div class="card col-sm-3">
        <div class="card-body px-0">
            <button class="btn btn-info btn-block" disabled>Task</button>
            <hr>
            <div class="list-group">
                <a class="orderstatus list-group-item list-group-item-action {{$order->where('status', 'Pending')->count() == 0  ? 'disabled' : ''}}"
                    data-value="pending" data-toggle="list" href="">Task Completed
                    <span class="badge badge-pill badge-danger float-right">{{$order->where('status', 'Pending')->count()}}</span>
                </a>
                <a class="orderstatus list-group-item list-group-item-action {{$order->where('status', 'Logistics to HQ')->count() == 0  ? 'disabled' : ''}}"
                    data-value="logisticshq" data-toggle="list" href="">Logistics to HQ
                    <span class="badge badge-pill badge-danger float-right">{{$order->where('status', 'Logistics to HQ')->count()}}</span>
                </a>
                <a class="orderstatus list-group-item list-group-item-action {{$order->where('status', 'Arrived at HQ')->count() == 0  ? 'disabled' : ''}}"
                    data-value="arrivedhq" data-toggle="list" href="">Arrived at HQ
                    <span class="badge badge-pill badge-danger float-right">{{$order->where('status', 'Arrived at HQ')->count()}}</span>
                </a>
                <a class="orderstatus list-group-item list-group-item-action {{$order->where('status', 'Logistics to Branch')->count() == 0  ? 'disabled' : ''}}"
                    data-value="logisticsbranch" data-toggle="list" href="">Logistics to Branch
                    <span class="badge badge-pill badge-danger float-right">{{$order->where('status', 'Logistics to Branch')->count()}}</span>
                </a>
            </div>
        </div>
    </div>
    <div id="order-list" class="card col-sm-2 p-0">
        <div id="pending" class="list-group">
            @foreach($order->where('status', 'Pending') as $o)
            @if($loop->first)
            <div id="firstpending" data-id="{{$o->id}}"></div>
            @endif
            <a class="list-group-item list-group-item-action" id="view-order" data-id="{{$o->id}}" data-toggle="list"
                href="">
                <div class="font-medium">{{$o->form['name']}}</div>
                <div class="col-12 px-0">
                    <div class="font-light">
                        Order ID #{{$o->id}}<div class="pull-right">{{$o->created_at->diffForHumans()}}</div>
                    </div>
                </div>
            </a>
            @endforeach
        </div>

            <div id="logisticshq" class="list-group">
                @foreach($order->where('status', 'Logistics to HQ') as $o)
                @if($loop->first)
                <div id="firstlogisticshq" data-id="{{$o->id}}"></div>
                @endif
                <a class="list-group-item list-group-item-action" id="view-order" data-id="{{$o->id}}" data-toggle="list"
                    href="">
                    <div class="font-medium">{{$o->form['name']}}</div>
                    <div class="col-12 px-0">
                        <div class="font-light">
                            Order ID #{{$o->id}}<div class="pull-right">{{$o->created_at->diffForHumans()}}</div>
                        </div>
                    </div>
                </a>
                @endforeach
            </div>

        <div id="arrivedhq" class="list-group">
            @foreach($order->where('status', 'Arrived at HQ') as $o)
            @if($loop->first)
            <div id="firstarrivedhq" data-id="{{$o->id}}"></div>
            @endif
            <a class="list-group-item list-group-item-action" id="view-order" data-id="{{$o->id}}" data-toggle="list"
                href="">
                <div class="font-medium">{{$o->form['name']}}</div>
                <div class="col-12 px-0">
                    <div class="font-light">
                        Order ID #{{$o->id}}<div class="pull-right">{{$o->created_at->diffForHumans()}}</div>
                    </div>
                </div>
            </a>
            @endforeach
        </div>

        <div id="logisticsbranch" class="list-group">
            @foreach($order->where('status', 'Logistics to Branch') as $o)
            @if($loop->first)
            <div id="firstlogisticsbranch" data-id="{{$o->id}}"></div>
            @endif
            <a class="list-group-item list-group-item-action" id="view-order" data-id="{{$o->id}}" data-toggle="list"
                href="">
                <div class="font-medium">{{$o->form['name']}}</div>
                <div class="col-12 px-0">
                    <div class="font-light">
                        Order ID #{{$o->id}}<div class="pull-right">{{$o->created_at->diffForHumans()}}</div>
                    </div>
                </div>
            </a>
            @endforeach
        </div>

    </div>
    <div class="card col-sm-7">
        <div id="orderDetail" class="card-body">
            <div class="row pb-2">
                <h4 id="h-ordertype" class="font-light m-0"></h4>
                <h4 id="h-orderid" class="font-medium ml-2 mb-0"></h4>
                <span id="status" class="badge badge-pill badge-warning ml-2"></span>
                <span id="urgency" class="badge badge-pill badge-danger ml-2"></span>
                <h5 id="timecreated" class="card-subtitle ml-auto m-1"></h5>
            </div>
            <div class="row">
                <h4 class="pt-2"><span class="mdi mdi-map-marker" id="h-collectbranch"></span></h4>
                <div class="row ml-auto">
                    <button type="button" class="itemarrived btn btn-outline-success ml-2"><i class="mdi mdi-truck-delivery"></i> Item Arrived</button>
                    <button type="button" class="distributebranch btn btn-outline-info ml-2"><i class="mdi mdi-truck-delivery"></i> Distribute to Branch</button>
                    <button type="button" class="completed btn btn-outline-success ml-2"><i class="far fa-check-circle"></i> Mark as Completed</button>
                    <button class="btn btn-outline-secondary ml-2" data-toggle="modal" id="view-qr" data-target="#viewQR"><i class="fas fa-qrcode"></i> Order ID</button> <input id="f-orderid" type="hidden" name="id">
                </div>
            </div>
            <hr>
            <h5 class="font-medium mt-4">ATTACHED ORDER DETAILS</h5>
            <div class="row">
                <div class="col-sm-4">
                    <div class="font-medium">Order ID</div>
                    <div id="orderid" class="font-light"></div>
                </div>
                <div class="col-sm-4">
                        <div class="font-medium">Collect Date</div>
                        <div id="collectdate" class="font-light"></div>
                </div>
                <div class="col-sm-4">
                        <div class="font-medium">Created Branch</div>
                        <div id="createdbranch" class="font-light"></div>
                    </div>
            </div>
            <div class="row">
                    <div class="col-sm-4">
                            <div class="font-medium col-">Order Type</div>
                            <div id="ordertype" class="font-light"></div>
                        </div>
                    <div class="col-sm-4">
                        <div class="font-medium">Collect Time</div>
                        <div id="collecttime" class="font-light"></div>
                    </div>
                    <div class="col-sm-4">
                            <div class="font-medium">Collection Branch</div>
                            <div id="collectbranch" class="font-light"></div>
                        </div>
                </div>
            <h5 class="font-medium mt-4">CUSTOMER INFORMATION</h5>
            <div class="row">
                <div class="col-sm-4">
                    <div class="font-medium">Name</div>
                    <div id="name" class="font-light"></div>
                </div>
                <div class="col-sm-4">
                    <div class="font-medium">Mobile No</div>
                    <div id="contact" class="font-light"></div>
                </div>
                <div class="col-sm-4">
                    <div class="font-medium">Email</div>
                    <div id="email" class="font-light"></div>
                </div>
            </div>
            <h5 class="font-medium mt-4">ORDER INFORMATION</h5>
            <div id="orderdetails"></div>
            <div class="row mt-4">
                    <div class="col-sm-12">
                        <div class="font-medium">Remark</div>
                        <div id="remark" class="font-light"></div>
                    </div>
            </div>
            <div id="dfilebutton" class="row pt-4">
                    <a id="downloadlink" href="" target="_blank" class="btn btn-rounded btn-block btn-dark"><i class="fas fa-download"></i> DOWNLOAD</a>
            </div>
        </div>
    </div>

    @foreach($order as $o)
    <div id="d-timecreated{{$o->id}}" style="display: none;" data-value="{{$o->created_at->diffForHumans()}}"></div>
    @if($o->sequence_turn != -1)
    <div id="d-estimated_time{{$o->id}}" style="display: none;"
        data-value="{{$o->form['department_sequence'][$o->sequence_turn]['day'] != 0  ? $o->form['department_sequence'][$o->sequence_turn]['day'].' DAY' : ''}}
        {{$o->form['department_sequence'][$o->sequence_turn]['hour'] != 0  ? $o->form['department_sequence'][$o->sequence_turn]['hour'].' HOUR' : ''}}
        {{$o->form['department_sequence'][$o->sequence_turn]['minute'] != 0  ? $o->form['department_sequence'][$o->sequence_turn]['minute'].' MINUTE' : ''}}"></div>
    @endif

    {{-- Attached Order Details --}}
    <div id="d-orderid{{$o->id}}" style="display: none;" data-value="{{$o->id}}"></div>
    <div id="d-type{{$o->id}}" style="display: none;" data-value="{{$o->form['name']}}"></div>
    <div id="d-status{{$o->id}}" style="display: none;" data-value="{{$o->status}}"></div>
    <div id="d-urgency{{$o->id}}" style="display: none;" data-value="{{$o->urgency}}"></div>
    <div id="d-collecttime{{$o->id}}" style="display: none;" data-value="{{date('g:i A', strtotime($o->collection))}}"></div>
    <div id="d-collectdate{{$o->id}}" style="display: none;" data-value="{{date('d/m/Y', strtotime($o->collection))}}"></div>
    <div id="d-createdbranch{{$o->id}}" style="display: none;" data-value="{{$o->createdbranch['name']}}"></div>
    <div id="d-collectbranch{{$o->id}}" style="display: none;" data-value="{{$o->collectbranch['name']}}"></div>
    <div id="d-remark{{$o->id}}" style="display: none;" data-value="{{$o->remark}}"></div>

    {{-- Customer Information --}}
    <div id="d-name{{$o->id}}" style="display: none;" data-value="{{$o->customer['name']}}"></div>
    <div id="d-email{{$o->id}}" style="display: none;" data-value="{{$o->customer['email']}}"></div>
    <div id="d-contact{{$o->id}}" style="display: none;" data-value="{{$o->customer['contact']}}"></div>

    <div id="d-orderdetails{{$o->id}}" style="display: none;">
        {{-- Order Information --}}
        @foreach($o->details as $d)
        @if($loop->iteration % 3 === 1)
        <div class="row">
            @endif

            @foreach($d as $key=>$val)
            <div class="col-sm-4">
                <div class="font-medium">{{$key}}</div>
                <div class="font-light">{{$val}}</div>
            </div>
            @endforeach

            @if($loop->iteration % 3 === 0 || $loop->last)
        </div>
        @endif
        @endforeach
    </div>

    <div id="d-downloadlink{{$o->id}}" style="display: none;" data-value="{!! $o->file() !!}"></div>

    @endforeach

<!-- View QR Code Modal -->
<div class="modal fade" id="viewQR" tabindex="-1" role="dialog">
        <div class="modal-dialog modal-sm" role="document">
            <div class="modal-content" id="form-content">
                <div class="modal-header">
                    <h5 class="modal-title font-medium" id="m-orderid"></h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="text-center my-2">
                        <img id="m-qr" class="img-fluid" />
                    </div>
                    <strong>CUSTOMER INFORMATION</strong>
                    <div class="row">
                        <div class="col-5">Name</div>
                        <div id="m-name" class="col-7"></div>
                    </div>
                    <div class="row">
                        <div class="col-5">Email</div>
                        <div id="m-email" class="col-7"></div>
                    </div>
                    <div class="row">
                        <div class="col-5">Mobile Number</div>
                        <div id="m-contact" class="col-7"></div>
                    </div>
                    <div class="row">
                        <div class="col-5">Collect Time</div>
                        <div id="m-collecttime" class="col-7"></div>
                    </div>
                    <div class="row">
                        <div class="col-5">Collect Date</div>
                        <div id="m-collectdate" class="col-7"></div>
                    </div>
                </div>
                <div class="modal-footer">
                        <button class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                        <button class="btn btn-danger" onclick="window.print();">Print</button>
                </div>
            </div>
        </div>
    </div>

    </div>
    </div>
