@extends('layouts.navbar')
@section('breadcrumb', 'Department / '.$department)
@section('content')
<div class="col-sm-12">
    <div class="card">
        <div class="card-body">
            <div class="d-flex no-block align-items-center mb-4">
                <h4 class="card-title">{{$department}} Department Staff</h4>
            </div>
            <div class="table-responsive col-lg-12">
                <table id="userTable" class="table no-wrap user-table mb-3">
                    <thead>
                        <tr>
                            <th>NAME</th>
                            <th>IC NUMBER</th>
                            <th>EMAIL</th>
                            <th>MOBILE NO</th>
                            <th>JOINING DATE</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($staff as $s)
                        <tr>
                            <td>{{$s->name}}</td>
                            <td>{{$s->nric}}</td>
                            <td>{{$s->email}}</td>
                            <td>{{$s->contact}}</td>
                            <td>{{date('d-m-Y', strtotime($s->created_at))}}</td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
@endsection
