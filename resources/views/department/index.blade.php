@extends('layouts.navbar')
@section('breadcrumb', 'Department List')
@section('content')
<div class="col-sm-12">
    <div class="card">
            <div class="d-flex no-block align-items-center col-lg-12 py-3" style="border-bottom: 1px solid #eaeaea;">
                    <h4 class="card-title mb-0 ml-4">DEPARTMENT LIST</h4>
                    <div class="ml-auto mr-4">
                        @can('Manage Department')
                        <div class="btn-group">
                            <button class="btn btn-info btn-rounded" data-toggle="modal" data-target="#createdepartment">
                                <i class="mdi mdi-briefcase mr-2"></i>Add New Department
                            </button>
                        </div>
                        @endcan
                    </div>
                </div>
        <div class="card-body">
            <div class="table-responsive col-lg-12">
                <table id="departmentTable" class="table no-wrap user-table mb-3">
                    <thead>
                        <tr>
                            <th>DEPARTMENT NAME</th>
                            <th>TOTAL STAFF</th>
                            <th>ACTION</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($department as $d)
                        <tr>
                            <td>{{$d->name}}</td>
                            <td>{{$d->users_count}}</td>
                            <td>
                                <a href="{{route('department.stafflist', $d->id)}}" class="btn btn-outline-info btn-circle btn-sm btn-circle ml-2"><i class="ti-eye"></i></a>
                                @can('Manage Department')
                                <button class="btn btn-outline-info btn-circle btn-sm btn-circle ml-2" data-toggle="modal" data-target="#managedepartment{{$d->id}}"><i class="ti-pencil-alt"></i></button>
                                @endcan
                            </td>
                        </tr>

                        <!-- Manage Department Modal -->
                        <div class="modal fade" id="managedepartment{{$d->id}}" tabindex="-1" role="dialog"
                            aria-labelledby="exampleModalLabel" aria-hidden="true">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <form class="form-horizontal" method="POST"
                                        action="{{ route('department.update', $d->id) }}">
                                        {{ csrf_field() }}
                                        {{ method_field('PUT') }}
                                        <div class="modal-header">
                                            <h4 class="modal-title" id="myModalLabel">Manage Department</h4>
                                            <button type="button" class="close" data-dismiss="modal"
                                                aria-hidden="true">×</button>
                                        </div>
                                        <div class="modal-body">
                                            <div class="form-group">
                                                <label class="col-md-12">Name of Department</label>
                                                <div class="col-md-12">
                                                    <input type="text" class="form-control" value="{{$d->name}}"
                                                        name="name" required />
                                                </div>
                                            </div>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-secondary"
                                                data-dismiss="modal">Cancel</button>
                                            <button type="submit" class="btn btn-success"><i class="ti-save mr-2"></i>
                                                Save</button>
                                    </form>
                                    <form action="{{ route('department.delete', $d->id ) }}" method="POST">
                                        {{ csrf_field() }}
                                        {{ method_field('DELETE') }}
                                        <button type="submit" class="btn btn-danger"><i class="ti-trash mr-2"></i>
                                            Delete</button>
                                    </form>
                                </div>
                            </div>
                        </div>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

<!-- Create Department Modal -->
<div class="modal fade" id="createdepartment" tabindex="-1" role="dialog" aria-labelledby="createModalLabel"
    aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <form method="POST" action="{{ route('department.store') }}">
                @csrf
                <div class="modal-header">
                    <h4 class="modal-title" id="createModalLabel"><i class="mdi mdi-briefcase mr-2"></i> Add New
                        Department</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="input-group mb-3">
                        <input type="text" class="form-control" placeholder="Enter Department Name Here" name="name"
                            required>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                    <button type="submit" class="btn btn-success"><i class="ti-save mr-2"></i> Save</button>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection

@section('script')
<script>
    $(document).ready(function () {
        $('#departmentTable').DataTable();
    });
</script>
@endsection
