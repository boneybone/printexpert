@extends('layouts.navbar')
@section('breadcrumb', 'Status List')
@section('content')
<div class="col-sm-12">
    <div class="card">
            <div class="d-flex no-block align-items-center col-lg-12 py-3" style="border-bottom: 1px solid #eaeaea;">
                    <h4 class="card-title mb-0 ml-4">STATUS LIST</h4>
                    <div class="ml-auto mr-4">
                        
                        <div class="btn-group">
                            <button class="btn btn-info btn-rounded" data-toggle="modal" data-target="#createstatus">Add New Status</button>                        </div>

                    </div>
                </div>
        <div class="card-body">
            <div class="table-responsive col-lg-12">
                <table id="statusTable" class="table no-wrap user-table mb-3">
                    <thead>
                        <tr>
                            <th>STATUS NAME</th>
                            <th>CREATED AT</th>
                            <th>ACTION</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($status as $s)
                        <tr>
                            <td>{{$s->name}}</td>
                            <td>{{$s->created_at}}</td>
                            <td>
                                <form action="{{route('status.delete', $s->id)}}" method="POST">
                                        {{ csrf_field() }}
                                        {{ method_field('DELETE') }}
                                        <button type="submit" class="btn btn-outline-danger btn-circle btn-sm btn-circle ml-2"><i class="ti-trash"></i></button>
                                </form>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

<!-- Create Status Modal -->
<div class="modal fade" id="createstatus" tabindex="-1" role="dialog" aria-labelledby="createModalLabel"
    aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <form method="POST" action="{{ route('status.store') }}">
                @csrf
                <div class="modal-header">
                    <h5 class="modal-title" id="createModalLabel"><i class="ti-check-box mr-2"></i> Add New
                        Status</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="input-group mb-3">
                        <input type="text" class="form-control" placeholder="Enter Status Name Here" name="name" required>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                    <button type="submit" class="btn btn-success"><i class="ti-save mr-2"></i> Save</button>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection

@section('script')
<script>
    $(document).ready(function () {
        $('#statusTable').DataTable();
    });
</script>
@endsection
