@extends('layouts.navbar')
@section('breadcrumb', 'User List')
@section('content')
<div class="col-sm-12">
    <div class="card">
        <div class="d-flex no-block align-items-center col-lg-12 py-3" style="border-bottom: 1px solid #eaeaea;">
            <h4 class="card-title mb-0 ml-4">USER LIST</h4>
            <div class="ml-auto mr-4">
                @can('Manage User')
                <div class="btn-group">
                    <button class="btn btn-info btn-rounded" data-toggle="modal" data-target="#createuser">
                        <i class="mdi mdi-account-plus mr-2"></i>Add User
                    </button>
                </div>
                @endcan
            </div>
        </div>
        <div class="card-body">
            <div class="table-responsive col-lg-12">
                <table id="userTable" class="table no-wrap user-table mb-3">
                    <thead>
                        <tr>
                            <th>NAME</th>
                            <th>IC NUMBER</th>
                            <th>EMAIL</th>
                            <th>MOBILE NO</th>
                            <th>DEPARTMENT</th>
                            <th>ROLE</th>
                            <th>JOINING DATE</th>
                            <th>ACTION</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($user as $u)
                        <tr>
                            <td><img src="{!! $u->photo() !!}" alt="user" class="rounded-circle" width="30" /> {{$u->name}}
                            </td>
                            <td>{{$u->nric}}</td>
                            <td>{{$u->email}}</td>
                            <td>{{$u->contact}}</td>
                            <td>{{$u->department['name']}}</td>
                            <td><span class="label label-danger">{{$u->getRoleNames()->first()}}</span></td>
                            <td>{{date('d-m-Y', strtotime($u->created_at))}}</td>
                            <td>
                                @can('Manage User')<a href="{{route('user.edit', $u->id)}}" class="btn btn-outline-info btn-circle btn-sm btn-circle ml-2"><i class="ti-pencil-alt"></i> </a>@endcan

                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

<!-- Create UserModal -->
<div class="modal fade" id="createuser" tabindex="-1" role="dialog" aria-labelledby="createModalLabel"
    aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <form method="POST" action="{{ route('user.store') }}">
                @csrf
                <div class="modal-header">
                    <h4 class="modal-title" id="createModalLabel"><i class="ti-user ml-2 mr-2"></i> Add New
                        User</h4>
                    <button class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="input-group mb-3">
                        <select class="form-control" name="title" required>
                            <option value="" disabled selected>Select Title</option>
                            <option>Mr</option>
                            <option>Mrs</option>
                            <option>Miss</option>
                            <option>Sir</option>
                            <option>Madam</option>
                        </select>
                    </div>
                    <div class="input-group mb-3">
                        <input type="text" class="form-control" placeholder="Enter Name Here" name="name" required>
                    </div>
                    <div class="input-group mb-3">
                        <input type="text" class="form-control" placeholder="Enter IC Number Here" name="nric" required>
                    </div>
                    <div class="input-group mb-3">
                        <input type="text" class="form-control" pattern="[0-9]+" placeholder="Enter mobile number here (Number only)" name="contact"
                            required>
                    </div>
                    <div class="input-group mb-3">
                        <input type="Email" class="form-control" placeholder="Enter Email Here" name="email" required>
                    </div>
                    <div class="input-group mb-3">
                        <input type="password" class="form-control" placeholder="Enter Password Here" name="password"
                            required>
                    </div>
                    <div class="input-group mb-3">
                        <select class="form-control" name="role" required>
                            <option value="" disabled selected>Select Role</option>
                            @foreach($role as $r)
                            <option>{{$r->name}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="input-group mb-3">
                        <select class="form-control" name="department_id">
                            <option value="">Select Department</option>
                            @foreach($department as $d)
                            <option value="{{$d->id}}">{{$d->name}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="modal-footer">
                    <button class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                    <button type="submit" class="btn btn-success"><i class="ti-save mr-2"></i> Save</button>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection

@section('script')
<script>
    $(document).ready(function () {
        $('#userTable').DataTable();
    });
</script>
@endsection
