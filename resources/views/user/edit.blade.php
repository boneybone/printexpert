@extends('layouts.navbar')
@section('breadcrumb', 'User Profile')
@section('content')
<div class="row d-flex no-block justify-content-center">
    <div class="col-6">
        <div class="card">
            <div class="d-flex no-block align-items-center col-lg-12 py-3" style="border-bottom: 1px solid #eaeaea;">
                <h4 class="card-title mb-0">USER PROFILE</h4>
            </div>
            <form class="form-horizontal" method="POST" enctype="multipart/form-data" action="{{route('user.update', $user->id)}}">
                {{ csrf_field() }}
                {{ method_field('PUT') }}
                <div class="card-body">

                    <div class="profile-pic text-center mb-4">
                            <div class="profile-pic mb-3 mt-3">
                                <img src="{!!$user->photo() !!}" width="150" class="rounded-circle" alt="user">
                            </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-sm-3 text-left control-label col-form-label">Title</label>
                        <div class="col-sm-9">
                            <select class="form-control" name="title">
                                <option {{$user->title == 'Mr.'  ? 'selected' : ''}}>Mr.</option>
                                <option {{$user->title == 'Mrs.'  ? 'selected' : ''}}>Mrs.</option>
                                <option {{$user->title == 'Miss'  ? 'selected' : ''}}>Miss</option>
                                <option {{$user->title == 'Sir'  ? 'selected' : ''}}>Sir</option>
                                <option {{$user->title == 'Madam'  ? 'selected' : ''}}>Madam</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="fname" class="col-sm-3 text-left control-label col-form-label">Name</label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control" value="{{$user->name}}" name="name" required>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-sm-3 text-left control-label col-form-label">IC Number</label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control" value="{{$user->nric}}" name="nric" required>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-sm-3 text-left control-label col-form-label">Mobile No</label>
                        <div class="col-sm-9">
                            <input type="text" pattern="[0-9]+" class="form-control" value="{{$user->contact}}" name="contact" required>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-sm-3 text-left control-label col-form-label">Email</label>
                        <div class="col-sm-9">
                            <input type="email" class="form-control" value="{{$user->email}}" name="email" required>
                        </div>
                    </div>
                    <div class="form-group row">
                            <label class="col-sm-3 text-left control-label col-form-label">Department</label>
                            <div class="col-sm-9">
                            @role('Super Admin')
                                <select class="form-control" name="department_id">
                                    <option value="">Unassigned</option>
                                    @foreach($department as $d)
                                    <option value="{{$d->id}}" {{$user->department_id == $d->id  ? 'selected' : ''}}>{{$d->name}}</option>
                                    @endforeach
                                </select>
                            @else
                            <label class="text-left control-label col-form-label">{{$user->department['name']}}</label>
                            @endrole
                            </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-sm-3 text-left control-label col-form-label">Role</label>
                        <div class="col-sm-9">
                            @role('Super Admin')
                            <select class="form-control" name="role">
                                @foreach($role as $r)
                                <option {{$user->getRoleNames()->first() == $r->name  ? 'selected' : ''}}>{{$r->name}}</option>
                                @endforeach
                            </select>
                            @else
                            <label class="text-left control-label col-form-label">{{$user->getRoleNames()->first()}}</label>
                            @endrole
                        </div>
                    </div>
                    <div class="form-group row">
                            <label class="col-sm-3 text-left control-label col-form-label">Profile Picture</label>
                            <div class="input-group mb-3 col-sm-9">
                                <div class="input-group-prepend">
                                    <span class="input-group-text">Upload</span>
                                </div>
                                <div class="custom-file">
                                    <input type="file" class="custom-file-input" name="profilepicture" onchange="ValidateSize(this)">
                                    <label id="img" class="custom-file-label">Choose file</label>
                                </div>
                            </div>
                    </div>
                </div>
                <hr class="mb-0">
                <div class="card-body">
                    <div class="row ml-2">
                        <div class="form-group mb-0 text-left">
                            <button type="submit" class="btn btn-info waves-effect waves-light">Save</button>
                        </div>
                    </form>
                        <form action="{{route('user.delete', $user->id)}}" method="POST">
                            {{ csrf_field() }}
                            {{ method_field('DELETE') }}
                            <button type="submit" class="btn btn-danger waves-effect waves-light ml-2">Delete</button>
                        </form>
                    </div>
                </div>
        </div>
    </div>
</div>
</div>
</div>
@endsection

@section('script')
<script>
    function ValidateSize(file) {
        var FileSize = file.files[0].size / 1024 / 1024; // in MB
        if (FileSize > 1) {
            swal("Warning", "File size exceeds 1 MB!", "warning");
            $(file).val(''); //for clearing with Jquery
            $('#img').html('Choose file');
        }
        else{
            $('#img').html(file.files[0].name);
        }
    }
</script>
<!--Custom JavaScript -->
<script src="{{ asset('assets/libs/sweetalert2/dist/sweetalert2.all.min.js') }}"></script>
@endsection
