@extends('layouts.navbar')
@section('breadcrumb', 'Branch List')
@section('content')
<div class="col-sm-12">
    <div class="card">
        <div class="d-flex no-block align-items-center col-lg-12 py-3" style="border-bottom: 1px solid #eaeaea;">
                <h4 class="card-title mb-0 ml-4">BRANCH LIST</h4>
                <div class="ml-auto mr-4">

                    <div class="btn-group">
                        <button class="btn btn-info btn-rounded" data-toggle="modal" data-target="#createbranch">
                            <i class="mdi mdi-map-marker mr-2"></i>Add New Branch
                        </button>
                    </div>

                </div>
            </div>
        <div class="card-body">
            <div class="table-responsive col-lg-12">
                <table id="branchTable" class="table no-wrap user-table mb-3">
                    <thead>
                        <tr>
                            <th>BRANCH NAME</th>
                            <th>BRANCH ADDRESS</th>
                            <th>ACTION</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($branch as $b)
                        <tr>
                            <td>{{$b->name}}</td>
                            <td>{{$b->address1}}<br>{{$b->address2}}<br>{{$b->postcode}} {{$b->city}}<br>{{$b->state}}</td>
                            <td>
                                <button class="btn btn-outline-info btn-circle btn-sm btn-circle ml-2" data-toggle="modal" data-target="#managebranch{{$b->id}}"><i class="ti-pencil-alt"></i></button>
                            </td>
                        </tr>

                        <!-- Manage Department Modal -->
                        <div class="modal fade" id="managebranch{{$b->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <form class="form-horizontal" method="POST"
                                        action="{{ route('branch.update', $b->id) }}">
                                        {{ csrf_field() }}
                                        {{ method_field('PUT') }}
                                        <div class="modal-header">
                                            <h4 class="modal-title" id="myModalLabel">Manage Branch</h4>
                                            <button type="button" class="close" data-dismiss="modal"
                                                aria-hidden="true">×</button>
                                        </div>
                                        <div class="modal-body">
                                            <div class="form-group">
                                                <label >Name of Branch</label>
                                                <div>
                                                    <input type="text" class="form-control" value="{{$b->name}}" name="name" required />
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                    <label >Address 1</label>
                                                    <div >
                                                        <input type="text" class="form-control" value="{{$b->address1}}" name="address1" required />
                                                    </div>
                                            </div>
                                            <div class="form-group">
                                                    <label >Address 2</label>
                                                    <div>
                                                        <input type="text" class="form-control" value="{{$b->address2}}" name="address2" required />
                                                    </div>
                                            </div>
                                            <div class="form-group">
                                                    <label >Postcode</label>
                                                    <div >
                                                        <input type="text" class="form-control" value="{{$b->postcode}}" name="postcode" required />
                                                    </div>
                                            </div>
                                            <div class="form-group">
                                                    <label >City</label>
                                                    <div>
                                                        <input type="text" class="form-control" value="{{$b->city}}" name="city" required />
                                                    </div>
                                            </div>
                                            <div class="form-group">
                                                    <label>State</label>
                                                    <div >
                                                        <input type="text" class="form-control" value="{{$b->state}}" name="state" required />
                                                    </div>
                                            </div>
                                        </div>

                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-secondary"
                                                data-dismiss="modal">Close</button>
                                            <button type="submit" class="btn btn-success"><i class="ti-save"></i>
                                                Save</button>
                                    </form>
                                    <form action="{{ route('branch.delete', $b->id ) }}" method="POST">
                                        {{ csrf_field() }}
                                        {{ method_field('DELETE') }}
                                        <button type="submit" class="btn btn-danger"><i class="ti-trash"></i>Delete</button>
                                    </form>
                                </div>
                            </div>
                        </div>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

<!-- Create Branch Modal -->
<div class="modal fade" id="createbranch" tabindex="-1" role="dialog" aria-labelledby="createModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <form method="POST" action="{{ route('branch.store') }}">
                @csrf
                <div class="modal-header">
                    <h4 class="modal-title" id="createModalLabel"><i class="mdi mdi-map-marker mr-2"></i> Add New Branch</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="input-group mb-3">
                        <input type="text" class="form-control" placeholder="Enter Branch Name Here" name="name" required>
                    </div>
                    <div class="input-group mb-3">
                            <input type="text" class="form-control" placeholder="Enter Address 1 Here" name="address1" required>
                    </div>
                    <div class="input-group mb-3">
                            <input type="text" class="form-control" placeholder="Enter Address 2 Here" name="address2" required>
                    </div>
                    <div class="input-group mb-3">
                            <input type="text" class="form-control" placeholder="Enter Postcode Here" name="postcode" required>
                    </div>
                    <div class="input-group mb-3">
                            <input type="text" class="form-control" placeholder="Enter City Here" name="city" required>
                    </div>
                    <div class="input-group mb-3">
                            <input type="text" class="form-control" placeholder="Enter State Here" name="state" required>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                    <button type="submit" class="btn btn-success"><i class="ti-save mr-2"></i> Save</button>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection

@section('script')
<script>
    $(document).ready(function () {
        $('#branchTable').DataTable();
    });
</script>
@endsection
