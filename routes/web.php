<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('auth.login');
});

Auth::routes(['register' => false]);

Route::get('/home', 'HomeController@index')->name('home');

Route::group(['prefix'=>'dashboard','as'=>'dashboard.','name'=>'dashboard'],function(){
    Route::get('/', 'DashboardController@index')->name('index');
    Route::get('/tasklist', 'DashboardController@taskList')->name('tasklist');
    Route::get('/manager', 'DashboardController@manager')->name('manager');
    Route::get('/staff', 'DashboardController@staff')->name('staff');
    Route::get('/data/manager', 'DashboardController@datamanagerRefresh');
    Route::get('/data/staff', 'DashboardController@datastaffRefresh');
    Route::get('/payment', 'DashboardController@payment')->name('payment');
});

Route::group(['prefix'=>'role','as'=>'role.','name'=>'role', 'middleware' => 'role:Super Admin'],function(){
    Route::get('/', 'RoleController@index')->name('index');
    Route::post('/', 'RoleController@store')->name('store');
    Route::put('/{id}', 'RoleController@update')->name('update');
    Route::delete('/{id}','RoleController@delete')->name('delete');
});

Route::group(['prefix'=>'user','as'=>'user.','name'=>'user'],function(){
    Route::get('/', 'UserController@index')->name('index')->middleware(['permission:View User']);
    Route::get('/{id}', 'UserController@edit')->name('edit');
    Route::post('/', 'UserController@store')->name('store')->middleware(['role:Super Admin|Manager|Staff']);
    Route::put('/{id}', 'UserController@update')->name('update');
    Route::delete('/{id}','UserController@delete')->name('delete')->middleware(['permission:Manage User']);
});

Route::group(['prefix'=>'department','as'=>'department.','name'=>'department'],function(){
    Route::get('/', 'DepartmentController@index')->name('index');
    Route::get('/{id}', 'DepartmentController@stafflist')->name('stafflist');
    Route::post('/', 'DepartmentController@store')->name('store')->middleware(['permission:Manage Department']);
    Route::put('/{id}', 'DepartmentController@update')->name('update')->middleware(['permission:Manage Department']);
    Route::delete('/{id}','DepartmentController@delete')->name('delete')->middleware(['permission:Manage Department']);
});

Route::group(['prefix'=>'form','as'=>'form.','name'=>'form'],function(){
    Route::get('/', 'FormController@index')->name('index')->middleware(['permission:View Form']);
    Route::get('/create', 'FormController@create')->name('create')->middleware(['permission:Manage Form']);
    Route::get('/{id}', 'FormController@show')->name('show')->middleware(['permission:View Form']);
    Route::get('/{id}/edit', 'FormController@edit')->name('edit')->middleware(['permission:Manage Form']);
    Route::post('/', 'FormController@store')->name('store')->middleware(['permission:Manage Form']);
    Route::put('/{id}', 'FormController@update')->name('update')->middleware(['permission:Manage Form']);
    Route::delete('/{id}','FormController@delete')->name('delete')->middleware(['permission:Manage Form']);

    Route::get('/{id}/sequence', 'FormController@sequence')->name('sequence')->middleware(['permission:View Form']);
    Route::put('/{id}/sequence', 'FormController@storeSequence')->name('storeSequence')->middleware(['permission:Manage Form']);
});

Route::group(['prefix'=>'status','as'=>'status.','name'=>'status'],function(){
    Route::get('/', 'StatusController@index')->name('index')->middleware(['permission:View Status']);
    Route::post('/', 'StatusController@store')->name('store')->middleware(['permission:Manage Status']);
    Route::delete('/{id}','StatusController@delete')->name('delete')->middleware(['permission:Manage Status']);
});

Route::group(['prefix'=>'branch','as'=>'branch.','name'=>'branch'],function(){
    Route::get('/', 'BranchController@index')->name('index')->middleware(['permission:View Branch']);
    Route::post('/', 'BranchController@store')->name('store')->middleware(['permission:Manage Branch']);
    Route::put('/{id}', 'BranchController@update')->name('update')->middleware(['permission:Manage Branch']);
    Route::delete('/{id}','BranchController@delete')->name('delete')->middleware(['permission:Manage Branch']);
});

Route::group(['prefix'=>'order','as'=>'order.','name'=>'order', 'middleware' => 'role:Super Admin|Manager|Staff'],function(){
    Route::get('/', 'OrderController@index')->name('index');
    Route::get('/edit/{id}', 'OrderController@edit')->name('edit');
    Route::get('/refreshList', 'OrderController@refreshList')->name('refreshList');
    Route::get('/detail/{id}', 'OrderController@detail')->name('detail');
    Route::post('/{id}', 'OrderController@store')->name('store');
    Route::delete('/{id}','OrderController@delete')->name('delete');

    Route::get('/form', 'OrderController@selectForm')->name('selectForm');
    Route::get('/form/{id}', 'OrderController@form')->name('form');

    Route::put('/assign', 'OrderController@assignStaff')->name('assignStaff');

    Route::get('/getCustomer/{id}', 'OrderController@getCustomer')->name('getCustomer');
    Route::get('/search', 'OrderController@search')->name('search');

    Route::put('/updateStatus', 'OrderController@updateStatus')->name('updateStatus');
    Route::put('/updateStorage', 'OrderController@updateStorage')->name('updateStorage');
    Route::put('/updatePayment', 'OrderController@updatePayment')->name('updatePayment');
    Route::put('/updateDesign', 'OrderController@updateDesign')->name('updateDesign');
    Route::put('/updateDelivery', 'OrderController@updateDelivery')->name('updateDelivery');
    Route::put('/updateUnpaid', 'OrderController@updateUnpaid')->name('updateUnpaid');

    Route::put('/{id}', 'OrderController@update')->name('update');

});

Route::get('/qr/order/{id}', 'OrderController@qrDetail')->name('qrDetail');
Route::put('/qr/order/{id}', 'OrderController@qrDetailUpdate')->name('qrDetailUpdate');

