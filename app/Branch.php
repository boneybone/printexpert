<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\Activitylog\Traits\LogsActivity;


class Branch extends Model
{
    use SoftDeletes;
    use LogsActivity;

    protected static $logAttributes = ['*'];
    protected static $logOnlyDirty = true;

    protected $fillable = [
        'name', 'address1', 'address2', 'postcode', 'city', 'state',
    ];

    public function createdbranches()
    {
        return $this->hasMany('App\Order');
    }

    public function collectbranches()
    {
        return $this->hasMany('App\Order');
    }
}
