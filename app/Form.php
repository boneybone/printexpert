<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\MediaLibrary\HasMedia\HasMedia;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;
use Spatie\Activitylog\Traits\LogsActivity;

class Form extends Model implements HasMedia
{
    use SoftDeletes;
    use HasMediaTrait;
    use LogsActivity;

    protected static $logAttributes = ['*'];
    protected static $logOnlyDirty = true;

    protected $casts = [
        'field' => 'array',
        'department_sequence' => 'array',
    ];

    protected $fillable = [
        'name', 'description', 'field', 'department_sequence'
    ];

    public function orders()
    {
        return $this->hasMany('App\Order', 'form_id');
    }

    public function photo()
    {
        if ($this->getMedia('form')->last() != null)
        {
            return $this->getMedia('form')->last()->getUrl();
        }
        else {
            return '../../assets/images/big/auth-bg.jpg';
        }
    }

    public function registerMediaCollections()
    {
        $this->addMediaCollection('form')->singleFile();
    }
}
