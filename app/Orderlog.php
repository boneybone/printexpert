<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Orderlog extends Model
{
    protected $fillable = [
        'order_id', 'logger_id'
    ];

    public function logger()
    {
        return $this->belongsTo('App\User');
    }

    public function order()
    {
        return $this->belongsTo('App\Order');
    }
}
