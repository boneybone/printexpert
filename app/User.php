<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Spatie\Permission\Traits\HasRoles;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;
use Spatie\MediaLibrary\HasMedia\HasMedia;
use Spatie\Activitylog\Traits\LogsActivity;

class User extends Authenticatable implements HasMedia
{
    use Notifiable;
    use HasRoles;
    use SoftDeletes;
    use HasMediaTrait;
    use LogsActivity;

    protected static $logAttributes = ['*'];
    protected static $logOnlyDirty = true;

    protected $guard_name = 'web';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'title', 'name', 'email', 'password', 'nric', 'contact', 'department_id', 'free_at'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    protected $dates = [
        'free_at',
    ];

    public function department()
    {
        return $this->belongsTo('App\Department');
    }

    public function creators()
    {
        return $this->hasMany('App\Order');
    }

    public function staffs()
    {
        return $this->hasMany('App\Order');
    }

    public function unpaid_approvers()
    {
        return $this->hasMany('App\Order');
    }

    public function customers()
    {
        return $this->hasMany('App\Order');
    }

    public function loggers()
    {
        return $this->hasMany('App\Orderlog');
    }

    public function photo()
    {
        if ($this->getMedia('profilepictures')->last() != null)
        {
            return $this->getMedia('profilepictures')->last()->getUrl();
        }
        else {
            return asset('assets/images/users/no-profile-pic.jpg');
        }
    }

    public function registerMediaCollections()
    {
        $this->addMediaCollection('profilepictures')->singleFile();
    }

}
