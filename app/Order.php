<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\MediaLibrary\HasMedia\HasMedia;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;
use Spatie\Activitylog\Traits\LogsActivity;

class Order extends Model implements HasMedia
{
    use SoftDeletes;
    use HasMediaTrait;
    use LogsActivity;

    protected $casts = [
        'details' => 'array',
    ];

    protected static $logAttributes = ['*'];
    protected static $logOnlyDirty = true;

    protected $fillable = [
        'details', 'form_id', 'customer_id', 'staff_id', 'department_id', 'urgency', 'status', 'collection', 'createdbranch_id', 'collectbranch_id', 'sequence_turn', 'remark', 'storage', 'price', 'paid', 'collectorname', 'collectorcontact', 'collectortime', 'onholdreason', 'discount', 'creator_id', 'full_paid', 'unpaid_collection', 'unpaid_approver_id', 'paymentmethod', 'paymentreference'
    ];

    public function department()
    {
        return $this->belongsTo('App\Department');
    }

    public function creator()
    {
        return $this->belongsTo('App\User');
    }

    public function staff()
    {
        return $this->belongsTo('App\User');
    }

    public function unpaid_approver()
    {
        return $this->belongsTo('App\User');
    }

    public function customer()
    {
        return $this->belongsTo('App\User');
    }

    public function form()
    {
        return $this->belongsTo('App\Form');
    }

    public function createdbranch()
    {
        return $this->belongsTo('App\Branch');
    }

    public function collectbranch()
    {
        return $this->belongsTo('App\Branch');
    }

    public function loggers()
    {
        return $this->hasMany('App\Orderlog');
    }

    public function file()
    {
        if ($this->getMedia('order')->last() != null)
        {
            return $this->getMedia('order')->last()->getUrl();
        }
        else {
            return '#';
        }
    }

    public function sketch()
    {
        if ($this->getMedia('sketch')->last() != null)
        {
            return $this->getMedia('sketch')->last()->getUrl();
        }
        else {
            return '#';
        }
    }

    public function signature()
    {
        if ($this->getMedia('signature')->last() != null)
        {
            return $this->getMedia('signature')->last()->getUrl();
        }
        else {
            return '#';
        }
    }

    public function registerMediaCollections()
    {
        $this->addMediaCollection('design')->singleFile();
        $this->addMediaCollection('sketch')->singleFile();
        $this->addMediaCollection('signature')->singleFile();
    }
}
