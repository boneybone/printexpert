<?php

namespace App;

class Permission  extends \Spatie\Permission\Models\Permission
{
    public static function DefaultPermissions()
    {
        return [

            'View User',
            'Manage User',

            'View Department',
            'Manage Department',

            'View Branch',
            'Manage Branch',

            'View Form',
            'Manage Form',

            'View Status',
            'Manage Status',

            'View Order',
            'Manage Order',
            'Assign Order',

        ];
    }
}
