<?php

namespace App\Http\Controllers;

use App\Order;
use App\Form;
use App\Branch;
use App\Status;
use App\Department;
use App\User;
use App\Orderlog;
use App\Media;
use Carbon\Carbon;

use Illuminate\Http\Request;

class OrderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $order = Order::get();

        return view('order.index', compact('order'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $id)
    {
        $form = Form::find($id);
        $details = [];

        foreach($form->field as $f)
        {
            if(strpos($f['name'], " ") !== false)//Check for whitespace named input
            {
                if($f['type'] == 'checkbox')
                {
                    if($request[str_replace(" ","_", $f['name'])] == 'Yes')
                        $details[] = [$f['name'] => $request[str_replace(" ","_", $f['name'])]];
                    else
                        $details[] = [$f['name'] => 'No'];
                }
                else
                    $details[] = [$f['name'] => $request[str_replace(" ","_", $f['name'])]];
            }
            else
            {
                if($f['type'] == 'checkbox')
                {
                    if($request[$f['name']] == 'Yes')
                        $details[] = [$f['name'] => $request[$f['name']]];
                    else
                        $details[] = [$f['name'] => 'No'];
                }
                else
                    $details[] = [$f['name'] => $request[$f['name']]];
            }
        }

        if($form->name == 'Business Card')
            $details[] = ['Roundcorner' => $request['roundcorner']];

        $request['details'] =  $details;
        $request['form_id'] =  $id;

        if($request['design'] == 'requestDesign')
        {
            $request['department_id'] = Department::where('name','Design')->first()->id;
            $request['sequence_turn'] = -1;
        }
        else
        {
            $request['department_id'] = $form->department_sequence[0]['department'];
            $request['sequence_turn'] = 0;
        }

        $request['creator_id'] = auth()->user()->id;

        if($request['paid'] >= (100-$request['discount'])/100*$request['price'])
            $request['full_paid'] = true;

        $order = Order::create($request->all());

        if ($request->hasFile('file'))
            $order->addMediaFromRequest('file')->toMediaCollection('order');

        if($request['sketchdesign'] == 'sketchTrue')
            $order->addMediaFromBase64($request['sketch'])->usingFileName(Carbon::now()->format('Y-m-d-H-s').'.png')->toMediaCollection('sketch');

        $order->addMediaFromBase64($request['signature'])->usingFileName(Carbon::now()->format('Y-m-d-H-s').'.png')->toMediaCollection('signature');

        return redirect()->route('order.selectForm')->with('message', 'Order has been created.');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Order  $order
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Order  $order
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $order = Order::find($id);
        $branch = Branch::get();

        if($order->form->name == 'Business Card')
            return view('order.customformedit.businesscard', compact('order', 'branch'));
        else
            return view('order.edit', compact('order', 'branch'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Order  $order
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $form = Order::find($id)->form;
        $details = [];

        foreach($form->field as $f)
        {
            if(strpos($f['name'], " ") !== false)//Check for whitespace named input
            {
                if($f['type'] == 'checkbox')
                {
                    if($request[str_replace(" ","_", $f['name'])] == 'Yes')
                        $details[] = [$f['name'] => $request[str_replace(" ","_", $f['name'])]];
                    else
                        $details[] = [$f['name'] => 'No'];
                }
                else
                    $details[] = [$f['name'] => $request[str_replace(" ","_", $f['name'])]];
            }
            else
            {
                if($f['type'] == 'checkbox')
                {
                    if($request[$f['name']] == 'Yes')
                        $details[] = [$f['name'] => $request[$f['name']]];
                    else
                        $details[] = [$f['name'] => 'No'];
                }
                else
                    $details[] = [$f['name'] => $request[$f['name']]];
            }
        }

        if($form->name == 'Business Card')
            $details[] = ['Roundcorner' => $request['roundcorner']];

        $request['details'] =  $details;

        if($request['paid'] >= (100-$request['discount'])/100*$request['price'])
            $request['full_paid'] = true;

        $order = Order::find($id);
        $order->update($request->all());

        if ($request->hasFile('file'))
            $order->addMediaFromRequest('file')->toMediaCollection('order');

        if ($request['sketchdesign'] == 'sketchTrue')
            $order->addMediaFromBase64($request['sketch'])->usingFileName(Carbon::now()->format('Y-m-d-H-s').'.png')->toMediaCollection('sketch');

        return redirect()->back()->with('message', 'Order has been updated.');

        // $todel =  $order->getMedia('sketch');
        // $todel[0]->delete();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Order  $order
     * @return \Illuminate\Http\Response
     */
    public function delete($id)
    {
        //
    }

    public function selectForm()
    {
        $form = Form::where('department_sequence', '!=', null)->orderBy('name')->get();

        return view('order.selectform', compact('form'));
    }

    public function form($id)
    {
        $form = Form::find($id);
        $customer = User::whereHas("roles", function($q){ $q->where("name", "Customer"); })->get();
        $branch = Branch::get();

        if($form->name == 'Business Card')
            return view('order.customform.businesscard', compact('form', 'customer', 'branch'));
        else
            return view('order.form', compact('form', 'customer', 'branch'));
    }

    public function assignStaff(Request $request)
    {
        $order = Order::find($request->id);
        if($request->has('staff_id'))
            $staff = User::find($request->staff_id);
        else
            $staff = auth()->user();

        if($order->department_id == $staff->department_id)
        {
            $order->staff_id = $staff->id;
            $order->save();
            $message = "Order #".$order->id." has been assigned to ".$order->staff->name;
            return compact('message');
        }
        else
        {
            $error = "Order already completed by ".$staff->department->name." department!";
            return compact('error');
        }

    }

    public function updateStatus(Request $request)
    {
        $order = Order::find($request->id);
        if($request->has('onholdreason'))
            if($request->onholdreason != null)
                $order->onholdreason = auth()->user()->name.': '.$request->onholdreason;
            else
                $order->onholdreason = null;
        if($request->has('status'))
            $order->status = $request->status;
        $order->save();

        //Update Task Time
        // if($order->sequence_turn != -1)
        // {
        //     if($request->status == 'In Process')
        //     {
        //         $user = User::find($order->staff_id);

        //         $day = $order->form['department_sequence'][$order->sequence_turn]['day'];
        //         $hour = $order->form['department_sequence'][$order->sequence_turn]['hour'];
        //         $minute = $order->form['department_sequence'][$order->sequence_turn]['minute'];

        //         if($user->free_at != null)
        //             $timefree = $user->free_at;
        //         else
        //             $timefree = now();

        //         $user->free_at = date('Y-m-d H:i:s',strtotime('+'.$day.' day +'.'+'.$hour.' hour +'.$minute.' minutes',strtotime($timefree)));
        //         $user->save();
        //     }
        // }

        if($request->status == 'Completed')
        {
            $order->staff_id = null;
            $turn = (int)$order->sequence_turn;
            ++$turn;
            $order->sequence_turn = $turn;

            $orderlog = Orderlog::create([
                'order_id' =>  $order->id,
                'logger_id' => auth()->user()->id,
            ]);

            if($turn >= count($order->form['department_sequence']) )
            {
                $order->status = 'Finished';
                $order->department_id = Department::where('name','Collection')->first()->id;
                $order->save();

                $message = 'Order #'.$order->id.' has been completed and queued in collection';
                return compact('message');
            }
            else
            {   //Pass to next department
                $order->status = "Pending";
                $order->department_id = $order->form['department_sequence'][$turn]['department'];
                $order->save();

                $message = 'Order #'.$order->id.' has been passed to '.$order->department->name.' department';
                return compact('message');
            }

        }

    }

    public function getCustomer($id)
    {
        $customer = User::find($id);

        return compact('customer');
    }

    public function updateStorage(Request $request)
    {
        $order = Order::find($request->id);
        $order->storage = $request->storage;
        $order->status = "Received";
        $order->save();

        return redirect()->back()->with('message', 'Item Order #'.$order->id.' storage has been updated.');
    }

    public function updatePayment(Request $request)
    {
        $order = Order::find($request->id);
        $order->paid += $request->paid;
        if($order->paid >= (100-$order->discount)/100*$order->price)
            $order->full_paid = true;
        $order->save();

        return redirect()->back()->with('message', 'Item Order #'.$order->id.' payment has been updated.');
    }

    public function updateDesign(Request $request)
    {
        $order = Order::find($request->id);
        if ($request->hasFile('design')) {
            $order->addMediaFromRequest('design')->toMediaCollection('order');
            return redirect()->back()->with('message', 'Order #'.$order->id.' design has been uploaded.');
        }
    }

    public function updateDelivery(Request $request)
    {
        $order = Order::find($request->id);
        $order->update($request->all());
        $order->status = 'Delivered';
        $order->collectortime = Carbon::now();
        $order->save();

        return redirect()->back()->with('message', 'Item Order #'.$order->id.' status has been updated.');
    }

    public function updateUnpaid(Request $request)
    {
        $order = Order::find($request->id);
        $order->unpaid_collection = !$order->unpaid_collection;
        if($order->unpaid_collection == true)
        {
            $order->unpaid_approver_id = auth()->user()->id;
            $type = 'Approved';
            $message = 'Order #'.$order->id.'  approved by '.auth()->user()->name.' to be picked up witout full payment.';
        }
        else
        {
            $order->unpaid_approver_id = null;
            $type = 'Disapproved';
            $message = 'Order #'.$order->id.'  disapproved by '.auth()->user()->name.' to be picked up without full payment.';
        }
        $order->save();

        return compact('type', 'message');
    }
    public function refreshList()
    {
        $order = Order::get();
        if(auth()->user()->getRoleNames()->first() == 'Manager' && (auth()->user()->department->name == 'Marketing'))
            return view('order.indexdatamarketingmanager', compact('order'));
        else
            return view('order.indexdata', compact('order'));

    }

    public function search(Request $request)
    {
        $searchValue = $request->search;
        $idsearch = Order::where('id', 'like', $searchValue.'%')->get();
        $contactsearch = Order::whereHas('customer', function($query) use ($searchValue) {$query->where('contact', 'like', $searchValue.'%');})->get();
        $creatorsearch = Order::whereHas('creator', function($query) use ($searchValue) {$query->where('name', 'like', $searchValue.'%');})->get();

        if($idsearch->count() == 0 && $contactsearch->count() == 0 && $creatorsearch->count() == 0)
            return '<h5 style="text-align: center;">No Results</h5>';

        if(auth()->user()->getRoleNames()->first() == 'Manager' && (auth()->user()->department->name == 'Marketing'))
            return view('order.searchdatawithapproval', compact('contactsearch', 'idsearch', 'creatorsearch'));
        else
            return view('order.searchdata', compact('contactsearch', 'idsearch', 'creatorsearch'));
    }

    public function qrDetail($id)
    {
        $order = Order::find($id);

        return view('order.qrdetail', compact('order'));
    }

    public function detail($id)
    {
        $order = Order::find($id);

        return view('order.detail', compact('order'));
    }

    public function qrDetailUpdate($id)
    {
        if(auth()->user()->department['name'] == 'Collection')
            $order = Order::find($id);
            $order->staff_id = null;
            $turn = (int)$order->sequence_turn;
            ++$turn;
            $order->sequence_turn = $turn;
            $order->status = 'Finished';
            $order->department_id = Department::where('name','Collection')->first()->id;
            $order->save();

            return redirect()->back()->with('message', 'Order #'.$order->id.' has been completed and queued in collection');

    }
}
