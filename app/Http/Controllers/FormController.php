<?php

namespace App\Http\Controllers;

use App\Form;
use App\Department;
use App\Branch;
use App\Status;
use Illuminate\Http\Request;
use Carbon\Carbon;

class FormController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $form = Form::get();

        return view('form.index', compact('form'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('form.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $field=[];
        for($i=1; $i<=$request['fieldcount']; $i++)
        {
            if($request['optioncount'.$i] > 0)
            {
                $option=[];
                $directory=[];

                for($j=1; $j<=$request['optioncount'.$i]; $j++)
                {
                    if($request->has('optionimage'.$i.'index'.$j))
                    {
                        $file = $request['optionimage'.$i.'index'.$j];
                        $filename = Carbon::now()->format('Y-m-d-H-s').$file->getClientOriginalName();
                        $file->move(public_path().'/formimages/', $filename);
                        $directory[] = $filename;
                    }

                    $option[] = $request['option'.$i.'index'.$j];

                }
                if($request['type'.$i] == 'radioImage')
                    $field[]=['name' => $request['name'.$i], 'type' => $request['type'.$i], 'validation' => $request['validation'.$i], 'option' =>  $option, 'directory' => $directory, 'custom' => $request['custom'.$i]];
                else
                    $field[]=['name' => $request['name'.$i], 'type' => $request['type'.$i], 'validation' => $request['validation'.$i], 'option' =>  $option, 'custom' => $request['custom'.$i]];
            }
            else
                $field[]=['name' => $request['name'.$i], 'type' => $request['type'.$i], 'validation' => $request['validation'.$i]];
        }
        $request['field'] = $field;
        $form = Form::create($request->all());

        //Save Form Thumbnail
        if (isset($request['file'])) {
            $form->addMediaFromRequest('file')->toMediaCollection('form');
        }

        // //Save Option Image
        // for($i=1; $i<=$request['fieldcount']; $i++)
        // {
        //     if($request['optioncount'.$i] > 0)
        //     {
        //         for($j=1; $j<=$request['optioncount'.$i]; $j++)
        //         {

        //         }
        //     }
        // }

        return redirect()->route('form.sequence', $form->id)->with('message', $form->name.' form has been created. Please set department sequence.');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Form  $form
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $form = Form::find($id);
        $branch = Branch::get();
        $status = Status::get();

        return view('form.show', compact('form', 'branch', 'status'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Form  $form
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $form = Form::find($id);

        return view('form.edit', compact('form'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Form  $form
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $field = [];
        for($i=1; $i<=$request->fieldcount; $i++)
        {
            if($request['option'.$i] != null)
            {
                $option_arr = explode (";", $request['option'.$i]);
                $option_arr = array_filter(array_map('trim', $option_arr));
                $field[] = ['name' => $request['name'.$i], 'type' => $request['type'.$i], 'validation' => $request['validation'.$i], 'option' =>  $option_arr, 'custom' => $request['custom'.$i]];
            }
            else
                $field[] = ['name' => $request['name'.$i], 'type' => $request['type'.$i], 'validation' => $request['validation'.$i]];
        }
        $request['field'] =  $field;
        $form = Form::find($id);
        $form->update($request->all());

        if (isset($request['file'])) {
            $form->addMediaFromRequest('file')->toMediaCollection('form');
        }

        return redirect()->back()->with('message', $form->name.' form has been updated.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Form  $form
     * @return \Illuminate\Http\Response
     */
    public function delete($id)
    {
        $form = Form::find($id);
        $form->delete();

        return redirect()->back()->with('message', $form->name.' form has been deleted.');
    }

    public function sequence($id)
    {
        $form = Form::find($id);
        $department = Department::get();
        //Remove unavailable department
        $department = $department->where('name','!=','Marketing')->where('name','!=','Collection');

        return view('form.sequence', compact('form', 'department'));
    }

    public function storeSequence(Request $request, $id)
    {
        $sequence = [];
        for($i=1; $i<=$request->fieldcount; $i++)
        {
            //Convert time to ms
            // $day = (int)$request['day'.$i]*86400000;
            // $hour = (int)$request['hour'.$i]*3600000;
            // $minute = (int)$request['min'.$i]*1000;
            // $totaltime = $day + $hour + $minute;

            $sequence[] = ['department' => (int)$request['department'.$i], 'day' => (int)$request['day'.$i], 'hour' => (int)$request['hour'.$i], 'minute' => (int)$request['minute'.$i] ];
        }
        $form = Form::find($id);
        $form->department_sequence = $sequence;
        $form->save();

        return redirect()->back()->with('message', $form->name.' sequence has been updated.');
    }
}
