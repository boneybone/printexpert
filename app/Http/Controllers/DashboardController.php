<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Order;
use App\Orderlog;
use App\Status;
use Spatie\Permission\Models\Role;
use App\Department;
use App\User;
use Auth;
use Spatie\Activitylog\Models\Activity;

class DashboardController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //super admin
        if(auth()->user()->getRoleNames()->first() == 'Super Admin')
        {
            $order = Order::with('customer', 'form', 'department')->get();

            return view('order.index', compact('order'));
        }

        //marketing
        $marketingid = Department::where('name','Marketing')->first()->id;
        if(auth()->user()->department->id == $marketingid)
        {
            $order = Order::get();
            $role = Role::get();
            $department = Department::get();
            $customerCount = User::whereHas("roles", function($q){ $q->where("name", "Customer"); })->count();
            $activitylog = Activity::orderBy('updated_at', 'DESC')->get();
            return view('dashboard.marketing', compact('order', 'role', 'department', 'customerCount', 'activitylog', 'marketingid'));
        }

        //collection
        $collectionid = Department::where('name','Collection')->first()->id;
        if(auth()->user()->department->id == $collectionid)
        {
            $status = Status::get();
            $order = Order::where('status', '!=', 'Pending')->where('status', '!=', 'In Process')->where('status', '!=', 'On Hold')->get();
            return view('dashboard.collection', compact('order', 'status'));
        }

        //manager
        if(auth()->user()->getRoleNames()->first() == 'Manager')
        {
            return redirect()->route('dashboard.manager');
        }

        //staff
        if(auth()->user()->getRoleNames()->first() == 'Staff')
        {
            return redirect()->route('dashboard.staff');
        }

        //customer
        if(auth()->user()->getRoleNames()->first() == 'Customer')
        {
            $order = Order::where('customer_id', auth()->user()->id)->get();
            return view('dashboard.customer', compact('order'));
        }

        //cashier
        //$order = Order::get();
        //return view('dashboard.cashier', compact('order'));

        return view('home');

    }

    public function manager()
    {
        $order = Order::where('status', 'Pending')->where('department_id', auth()->user()->department_id)->where('staff_id', null)->get();
        $taskcount = Order::where('status', '!=', 'Completed')->get();
        $staff = User::where('department_id', auth()->user()->department_id)->get();
        return view('dashboard.manager', compact('order', 'staff', 'taskcount'));
    }

    public function staff()
    {
        $order = Order::where('staff_id', auth()->user()->id)->get();
        $ordercompletedid = Orderlog::where('logger_id', auth()->user()->id)->pluck('order_id');
        $ordercompleted = Order::latest()->limit(30)->find($ordercompletedid)->where('staff_id', null)->where('status', 'Pending');
        $staff = User::whereHas("roles", function($q){ $q->where("name", "Staff"); })->get();

        $branchid = Department::where('name','Branch')->first()->id;
        if(auth()->user()->department->id == $branchid)
            return view('dashboard.branch.staff', compact('order'));
        else
            return view('dashboard.staff', compact('order','ordercompleted', 'staff'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

    }

    public function taskList()
    {
        $staff = User::where('department_id', auth()->user()->department_id)->paginate(4);
        $order = Order::get();
        return view('dashboard.tasklist', compact('staff', 'order'));
    }

    public function datamanagerRefresh()
    {
        $order = Order::where('status', 'Pending')->where('department_id', auth()->user()->department_id)->where('staff_id', null)->get();
        $taskcount = Order::where('status', '!=', 'Completed')->get();
        $staff = User::where('department_id', auth()->user()->department_id)->get();
        return view('dashboard.managerdata', compact('order', 'staff', 'taskcount'));
    }

    public function datastaffRefresh()
    {
        $order = Order::where('staff_id', auth()->user()->id)->get();
        $ordercompletedid = Orderlog::where('logger_id', auth()->user()->id)->pluck('order_id');
        $ordercompleted = Order::latest()->limit(30)->find($ordercompletedid)->where('staff_id', null)->where('status', 'Pending');
        $staff = User::whereHas("roles", function($q){ $q->where("name", "Staff"); })->get();

        $branchid = Department::where('name','Branch')->first()->id;

        if(auth()->user()->department->id == $branchid)
            return view('dashboard.branch.staffdata', compact('order'));
        else
            return view('dashboard.staffdata', compact('order', 'ordercompleted', 'staff'));
    }

    public function payment()
    {
        //Middleware
        if(Auth::user()->getRoleNames()->first() == 'Super Admin' || (Auth::user()->getRoleNames()->first() == 'Manager' && Auth::user()->department->name == 'Marketing'))
        {
            $order = Order::get();
            return view('dashboard.payment', compact('order'));
        }
        else
        {
            return redirect()->route('dashboard.index');
        }

    }
}
